<?php include('header.php');?>

<script type="text/javascript">
  function showImage(src, target) {
            var fr = new FileReader();

             fr.onload = function(){
      target.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putImage() {
            var src = document.getElementById("select_image");
            var target = document.getElementById("target");
            showImage(src, target);
        }
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Adding User Info</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Add User</h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>user/storeuser" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($cat)) {echo $cat->ID;}?>" name="id">
									<input type="hidden" value="<?php if(isset($cat)) {echo $cat->Image;}?>" name="imageupdate">
									<div class="col-sm-6">
									<div class="form-group">
										<label class="col-md-3 control-label">Employee Image</label>
										<div class="col-md-6">
											<span class="pull-left btn btn-default btn-file">
											Browse... 
<input  type="file" id="select_image" name="image" onchange="putImage()" />
 <img id="target" height="100" width="100" src="<?php if(isset($cat)) {echo base_url().$cat->Image;}?>" />
											</span>
										</div>
									</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" >Employee Name</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Employee Name" value="<?php if(isset($cat)) {echo $cat->Name;}?>" name="name" required id="name" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Phone Number</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Phone Number" value="<?php if(isset($cat)) {echo $cat->Phone;}?>" name="phone" required id="phone" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Address</label>
											<div class="col-sm-6">
												<textarea type="text" placeholder="Address" name="address" required id="address" class="form-control"><?php if(isset($cat)) {echo $cat->Address;}?></textarea>
											</div>
										</div>
										
									</div>
									<div class="col-sm-6">
									<div class="form-group">
											<label class="col-sm-3 control-label">Employee ID</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Employee ID" name="empid" value="<?php if(isset($cat)){ echo $cat->Emp_ID;} ?>" <?php if(!($this->session->userdata('Emp_ID') == "1001")){ echo 'readonly';}  ?> required id="empid" class="form-control">
											
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Employee Email</label>
											<div class="col-sm-6">
												<input type="email" placeholder="Employee Email" value="<?php if(isset($cat)) {echo $cat->Email;}?>" <?php if(!($this->session->userdata('Emp_ID') == "1001")){ echo 'readonly';}  ?> name="email" required id="email" class="form-control">
											</div>
										</div>
										<?php if(!isset($cat)) { ?>
										<div class="form-group">
											<label class="col-sm-3 control-label">Password</label>
											<div class="col-sm-6">
												<input type="password" placeholder="Password" value="" name="password" required id="Passwordnm" class="form-control">
											</div>
										</div>
										<?php } ?>
										<div class="form-group">
											<label class="col-sm-3 control-label">Role</label>
											<div class="col-sm-6">
											<select class="selectpicker"  name="role" required id="role"<?php if(!($this->session->userdata('Emp_ID') == "1001")){ echo 'disabled';}  ?>>
												 <option value="">Select Role </option>
												 <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_role`")->result_array();
                
                        foreach ($query  as $role)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $role['ID']; ?>" <?php if(isset($cat)) { if($cat->Role == $role['Role']){ echo "selected";}}?> ><?php echo $role['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
                                   
												</select>
												</div>
												</div>
												<div class="form-group">
											<label class="col-sm-3 control-label">Designation</label>
											<div class="col-sm-6">
											<select class="selectpicker"  name="position" required id="position"<?php if(!($this->session->userdata('Emp_ID') == "1001")){ echo 'disabled';}  ?>>
												 <option value="">Select Designation </option>
												 <option value="Manager" <?php if(isset($cat)) {if($cat->Position=="Manager"){ echo "selected";}}?>>Manager</option>
												 <option value="TeamLead" <?php if(isset($cat)) {if($cat->Position=="TeamLead"){ echo "selected";}}?>>TeamLead</option>
												 <option value="Agent" <?php if(isset($cat)) {if($cat->Position=="Agent"){ echo "selected";}}?>>Agent</option>
												
												</select>
												</div>
												</div>
												<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Change Password</label>
											<div class="col-sm-6">
											 <a href="<?php echo base_url();?>user/changepassword" class="btn btn-primary">Change Password</a>
												</div>
												</div>
									
									</div>
									</div>
									<div class="panel-footer text-center">
									<?php if(($this->session->userdata('Emp_ID') == "1001")){ ?> <a href="<?php echo base_url();?>user/userdetail" class="btn btn-purple btn-labeled fa fa-eye">View List</a><?php } ?>
									
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
