<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Facilities & Amenities</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Facilities & Amenities List</h3>
						</div>
						<div class="panel-body">
						<a href="<?php  echo base_url();?>report/addfacilities"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Facilities & Amenities</button></a>
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="slno"
								   data-page-list="[5, 10, 20]"
								   data-page-size="20"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										<th data-field="slno" data-sortable="true">SL No.</th>
										<th data-field="companyname" data-sortable="true">Facilities Name </th>
										<th data-field="contactperson" data-sortable="true" >Category</th>
										<th data-field="phone" data-sortable="true" >Created</th>
									
										<th data-field="setting" data-sortable="true" >Setting</th>
										
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($facilities  as $facilitieslist) { 
										?>	
                    <tr>
					   <td><?php echo $i;?></td>
								<td><?php echo $facilitieslist->Name;?></td>
								<td><?php $catg= $this->db->get_where('tbl_category' , array('ID' => $facilitieslist->Category))->row_array(); echo $catg['Name'];?></td>
                        		<td><?php echo $facilitieslist->Created;?></td>
                        		
								
								
                        		
                        <td class="form-group"> <a  href="<?php echo base_url();?>report/editfacilities/<?php echo $facilitieslist->ID;?>" class="label label-table label-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deletefacilities/<?php echo $facilitieslist->ID;?>" class="label label-table label-success btn-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>