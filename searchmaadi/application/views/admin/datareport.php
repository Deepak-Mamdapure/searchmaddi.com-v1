<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Data Reports</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
					<div class="panel-body">
					<div class="row">
					<form action="<?php echo base_url();?>admin/datareport" method="post">
														<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>Company Name/ContractID</b></label>
													<input type="text" name="name"  value="" placeholder="Company Name/ContractID" id="txt1"  class="form-control">
												</div>
											</div>
											<!--<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>ContractID</b></label>
													<input type="text" name="contractid" id="txt2" value=""   class="form-control">
												</div>
												
											</div>-->
											<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>From</b></label>
													<input type="date" name="from" value="" id="txt1"  class="form-control">
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>TO</b></label>
													<input type="date" name="to" id="txt2" value=""  class="form-control">
												</div>
												
											</div>
											
										</div>
										<div class="panel-footer text-center">
										<button class="btn btn-info" type="submit" name="to_web">Search </b></button>
										
									</div>
									</form>
										</div>
						<div class="panel-heading">
							<h3 class="panel-title"><?php //echo $mess;?> Details</h3>
						</div>
						<div class="panel-body">
							<!--<a href="<?php // echo base_url();?>admin/"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Contrct</button></a>-->
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   >
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Agent Name</th>
										<th data-field="track" data-sortable="true" >ContractID</th>
										<th data-field="name" data-sortable="true">CompanyName</th>
										<th data-field="date" data-sortable="true" >Data Submit</th>
										<th data-field="da" data-sortable="true" >Quality </th>
										<th data-field="trac" data-sortable="true" >Data Active</th>
										<th data-field="status" data-sortable="true" >Data Status</th>
										
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
				   if(isset($tme)) {
										$i=1;
										foreach ($tme  as $tmeinfo) { 
										?>	
                    <tr >
					<td><?php $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $tmeinfo->EmpID))->row_array(); 
echo $emp['Name'];?></td>
                        <!--<td><img src="<?php //echo base_url().$tmeinfo->Image;?>" width="100" alt="" /></td>-->
						<td><?php echo $tmeinfo->ContractID;?></td>
						<td><?php echo $tmeinfo->CompanyName;?></td>
						
                        		<td><?php echo $tmeinfo->Created;?></br> <i class="fa fa-check fa-2x label-success"></i>
								
                       </td>
					   <td class="form-group"> <a   class="label label-table label-success">
						<?php
						
						if($tmeinfo->flag==2)
						{
							
							echo 'Under Review';
							echo '<i class="fa fa-check fa-2x"></i>';
						}
						
						else 
						{
							echo 'Pending<br/>';
							echo '<i class="fa fa-ban fa-2x"></i>';
						}
						?>
                               
                            </a>
                            
                          
                            </td>
							 <td class="form-group"> <a   class="label label-table label-success">
						<?php
						
						if($tmeinfo->flag==2)
						{
							
							echo 'Under Review';
							echo '<i class="fa fa-check fa-2x"></i>';
						}
						else if($tmeinfo->flag==3)
						{
							echo 'Reject';
							echo '<i class="fa fa-ban fa-2x"></i>';
						}
						else 
						{
							echo 'Pending<br/>';
							echo '<i class="fa fa-tasks fa-2x"></i>';
						}
						?>
                               
                            </a>
                            
                          
                            </td>
							 <td class="form-group"> <a   class="label label-table label-success">
						<?php
						if($tmeinfo->flag==0)
						{
							echo 'On Process';
							echo '<i class="fa fa-check fa-2x"></i>';
						}
						if($tmeinfo->flag==1)
						{
							echo 'Active';
							echo '<i class="fa fa-check fa-2x"></i>';
						}
						if($tmeinfo->flag==2)
						{
							echo 'Under Review';
							echo '<i class="fa fa-check fa-2x"></i>';
						}
						if($tmeinfo->flag==3)
						{
							echo 'Rejected';
							echo '<i class="fa fa-ban fa-2x"></i>';
						}
						?>
                               
                            </a>
                            
                          
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
				   }
										
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>