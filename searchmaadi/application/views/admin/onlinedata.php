<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Online Details</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php if(isset($onlinedata)) {echo "Update Online Details"; } else { echo "Add Online Details"; } ?></h3>
						</div>
						<div class="panel-body">
						<form action="<?php echo base_url();?>report/saveonlinedata" method="post" enctype="multipart/form-data">
							<div class="row col-lg-12">
							<?php if(isset($onlinedata)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($onlinedata)) { echo $onlinedata->ID;}?>">
								<?php 
								}
									?>	
							<div class="col-lg-4">
								<input type="text" name="companyname" class="form-control" placeholder="Company Name" value="<?php if(isset($onlinedata)) { echo $onlinedata->CompanyName;}?>" required >
							
						<br />
								<input type="text" name="contactperson" class="form-control" placeholder="Contact Person" value="<?php if(isset($onlinedata)) { echo $onlinedata->ContactPerson;}?>" required >
							<br />
							<input type="text" name="phonenumber" class="form-control" placeholder="Phone Number" pattern="/\[0-9]/g" value="<?php if(isset($onlinedata)) { echo $onlinedata->PhoneNumber;}?>"   maxlength="10" required >
							
						</div>
						<div class="col-lg-4">
								<input type="email" name="email" class="form-control" placeholder="Email Id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php if(isset($onlinedata)) { echo $onlinedata->Email;}?>" required >
							
						<br />
									<select class="form-control" name="category" required >
								  <option value="">Select Category</option>
										<?php  foreach($categorylist as $catg) 
												  { ?>
													<option value="<?php echo $catg->ID; ?>" <?php if(isset($onlinedata)) { if($catg->ID== $onlinedata->CategoryId){ echo "selected";}}?>><?php echo $catg->Name; ?></option>
												  <?php } ?>
								</select>
						<br />
							<textarea name="address" rows="2" cols="50"  placeholder="Address" required ><?php if(isset($onlinedata)) { echo $onlinedata->Address;}?></textarea>
							
						</div>
						<div class="col-lg-4">
						<div class="form-group city">
											<label class="control-label"><b>City</b></label>
											<select class="selectpicker"   name="city" required id="txt7">
												 <option value="">Select City </option>
                                   <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_location` where flag= '0'")->result_array();
                
                        foreach ($query  as $lac1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $lac1['ID']; ?>" <?php if(isset($onlinedata)) { if($onlinedata->City== $lac1['ID']) {echo "selected";} }?>><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												<div class="form-group area">
											<label class="control-label"><b>Area</b></label>
											<select class="selectpic"  name="area" required id="txt8" data-live-search="true" style="width: 200px;height: 30px;">
												 <option value="">Select area </option>
												 <?php
 if(isset($onlinedata)) {
            $query=$this->db->query("SELECT * FROM `tbl_location` where Parent='".$onlinedata->City."'")->result_array();
                
                        foreach ($query  as $area)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $area['ID']; ?>" <?php if(isset($onlinedata)) { if($onlinedata->Area== $area['ID']) {echo "selected";}}?>><?php echo $area['Name']; ?></option>
                                        
                                        <?php
                                        }
 }
                                        ?>
                                   
												</select>
												</div>
												<div class="form-group">
													<label class="control-label" id="editpin"><b>Pincode</b> <i class="fa fa-edit fa-2x" onclick="ToggleReadOnlyState ();"></i></label>
													<input type="text" name="pincode" id="txt9"  rows="3" readonly="readonly"  value="<?php if(isset($onlinedata)) { echo $onlinedata->PinCode;}?>" required class="form-control" >
												
												</div>
							
						</div>
						</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Save</button>
								<a href="<?php echo base_url();?>report/onlinedatalist" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

	</div>
<?php include('footer.php');?>