<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Location</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Location Details</h3>
						</div>
						<div class="panel-body">
							<a href="<?php echo base_url();?>admin/addlocation"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Location and Sub-Location</button></a>
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true" >Image</th>
										<th data-field="name" data-sortable="true">Name</th>
										<th data-field="date" data-sortable="true" >Kannada</th>
										<th data-field="track" data-sortable="true" >Pin Code</th>
										<th data-field="track1" data-sortable="true" >Sub Location</th>
										<th data-field="status" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($location  as $locationinfo) { 
										?>	
                    <tr >
                        <td><img src="<?php echo base_url().$locationinfo->SmallImage;?>" width="100" alt="" /></td>
						<td><?php echo $locationinfo->Name;?></td>
						<td><?php echo $locationinfo->Kannada;?></td>
						<td><?php echo $locationinfo->Zip;?></td>
                        		
                 <td class="center"><a  href="<?php echo base_url();?>admin/areainfo/<?php echo $locationinfo->ID;?>" class="btn btn-default buttons-copy buttons-html5">
                               View
                            </a></td>
                        <td class="form-group"> <a  href="<?php echo base_url();?>admin/editlocation/<?php echo $locationinfo->ID;?>" class="label label-table label-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>admin/deletelocation/<?php echo $locationinfo->ID;?>" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>