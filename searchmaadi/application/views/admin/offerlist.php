<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Offers List</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Offers List</h3>
						</div>
						<div class="panel-body">
						<a href="<?php echo base_url();?>report/contractlist" class="btn btn-purple btn-labeled fa fa-reply pull-left">Back To Contract List</a>
							
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Company Name</th>
										<th data-field="name" data-sortable="true">Offer Title</th>
										<th data-field="date" data-sortable="true" >Offer Description</th>
										<th data-field="track" data-sortable="true" >Valid From</th>
										<th data-field="status" data-sortable="true" >Valid To</th>
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($offerlist  as $offer) { 
										?>	
                    <tr >
						<td><?php echo $offer->CompanyName;?></td>
						<td><?php echo $offer->OfferTitle;?></td>
                        		<td><?php echo $offer->OfferDescription;?></td>
                        		<td><?php echo $offer->ValidFrom;?></td>
                        		<td><?php echo $offer->ValidTo;?></td>
								
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						
						<a  href="<?php echo base_url();?>report/editoffers/<?php echo $offer->OfferId;?>" class="label label-table label-success btn-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deleteoffer/<?php echo $offer->OfferId;?>" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin//<?php echo $offer->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>