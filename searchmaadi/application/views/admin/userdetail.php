<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">User Details</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> Details</h3>
						</div>
						<div class="panel-body">
						<a href="<?php  echo base_url();?>user/adduser"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add User</button></a>
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="20"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Image</th>
										<th data-field="name" data-sortable="true">Employee Name </th>
										<th data-field="date" data-sortable="true" >Employee ID</th>
										<th data-field="Phone" data-sortable="true" >Phone</th>
										<th data-field="Address" data-sortable="true" >Address</th>
										<th data-field="Position" data-sortable="true" >Designation</th>
										<th data-field="track" data-sortable="true" >Email</th>
										<th data-field="status" data-sortable="true" >Role</th>
										<th data-field="statu" data-sortable="true" >Setting</th>
										
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($user  as $userinfo) { 
										?>	
                    <tr>
					 <td><img src="<?php echo base_url().$userinfo->Image;?>" width="100" alt="" /></td>
					
                       
						<td><?php echo $userinfo->Name;?></td>
						<td><?php echo $userinfo->Emp_ID;?></td>
						<td><?php echo $userinfo->Phone;?></td>
						<td><?php echo $userinfo->Address;?></td>
                        <td><?php echo $userinfo->Position;?></td>
                        <td><?php echo $userinfo->Email;?></td>
                        		
								
								<td><?php $emp= $this->db->get_where('tbl_role' , array('Role' => $userinfo->Role))->row_array(); 
echo $emp['Name'];?></td>
								
               
                        <td class="form-group"> <a  href="<?php echo base_url();?>user/updateuser/<?php echo $userinfo->ID;?>" class="label label-table label-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>user/deleteuser/<?php echo $userinfo->ID;?>" class="label label-table label-success btn-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>