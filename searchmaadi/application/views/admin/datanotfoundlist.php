<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Data Not Found List</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">View Data Not Found List</h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Company Name</th>
										<th data-field="a" data-sortable="true">Category Name</th>
										<th data-field="b" data-sortable="true">City Name</th>
										<th data-field="c" data-sortable="true">Area Name</th>
										<th data-field="d" data-sortable="true">Employee Name</th>
										<th data-field="e" data-sortable="true">Category Not Found</th>
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($datanotfound  as $list) { 
										?>	
                    <tr >
					<td><?php echo $list->CompanyName;?></td>
					<td><?php $category= $this->db->get_where('tbl_category' , array('ID' => $list->CategoryID))->row_array(); echo $category['Name'];?></td>
					<td><?php $City= $this->db->get_where('tbl_location' , array('ID' => $list->CityID))->row_array(); echo $City['Name'];?></td>
					<td><?php $Area= $this->db->get_where('tbl_location' , array('ID' => $list->AreaID))->row_array(); echo $Area['Name'];?></td>
					<td><?php $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $list->EmployeeId))->row_array(); echo $emp['Name'];?></td>
					<td><?php echo $list->CategoryNotFound;?></td>
					
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						<?php if(($this->session->userdata('Emp_ID') == "1020")){ ?>
									
						<a  href="<?php echo base_url();?>iro/updatedata/<?php echo $list->ID;?>" title="Edit/View" class="label label-table label-success btn-success">
                               Edit/View
                            </a>
                            <?php } 
							if(($this->session->userdata('Emp_ID') == "1020")){ 
						 ?>
                            <a  href="<?php echo base_url();?>iro/deletenotfound/<?php echo $list->ID;?>" title="Delete" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 <?php } 
							else{ 
						 ?>
						  <a  href="<?php echo base_url();?>report/deletenotfound/<?php echo $list->ID;?>" title="Delete" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
						 <?php } 
							
						 ?>
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin//<?php echo $contract->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					
					
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
