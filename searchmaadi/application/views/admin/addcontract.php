<?php include('header.php');

if($this->session->userdata('agent_city') == true) {
   $setcity = $this->session->userdata('agent_city');
   }
   else
   {
    $setcity =38;
   }

?>
 
   
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Adding Contract Info</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						<div class="col-sm-6" style="overflow-y:scroll; max-height: 700px;">
							<div class="panel">
							<form action="<?php echo base_url();?>admin/save_contract" method="post" id="form">
								<div class="panel-heading">
									<h3 class="panel-title">Add Contract Details( <?php if(isset($contractinfo)) { echo $contractinfo->ContractID;} ?>)	<a class="btn btn-info panel-footer" onclick="document.getElementById('light1').style.display='block';document.getElementById('fade').style.display='block'">Disposition</b></a> </h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<?php if(isset($contractinfo)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($contractinfo)) { echo $contractinfo->ID;}?>">
								<input type="hidden" name="contractid" value="<?php if(isset($contractinfo)) { echo $contractinfo->ContractID;} ?>">
								<?php 
								}
									?>
									<div class="panel-body">
									
										<div class="row">
														<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label"><b>Company Name</b></label>
													<input type="text" name="name" value="<?php if(isset($contractinfo)) { echo $contractinfo->CompanyName;}?>" id="txt1" required class="form-control">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label"><b>Company Tagline</b></label>
													<input type="text" name="tagline" id="txt2" value="<?php if(isset($contractinfo)) { echo $contractinfo->Tagline;}?>" required class="form-control">
												</div>
												
											</div>
										</div>
										<div class="row">
											
											<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>Contact Person</b></label>
													<input type="text" name="person" id="txt3" value="<?php if(isset($contractinfo)) { echo $contractinfo->ContactPerson;}?>" required class="form-control">
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<label class="control-label"><b>Designation</b></label>
													<input type="text" name="designation" id="txt21" value="<?php if(isset($contractinfo)) { echo $contractinfo->Designation;}?>" required class="form-control">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label"><b>Address</b></label>
													<input type="text" name="address" id="txt10" value="<?php if(isset($contractinfo)) { echo $contractinfo->Address;}?>" required class="form-control">
												</div>
												
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
											<div class="form-group city">
											<label class="control-label"><b>City</b></label>
											<select class="selectpicker"   name="city" required id="txt7" data-live-search="true">
												 <option value="">Select City </option>
                                   <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_location` where Parent='".$setcity."'")->result_array();
                
                        foreach ($query  as $lac1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $lac1['ID']; ?>" <?php if(isset($contractinfo)) { if($contractinfo->City== $lac1['ID']) {echo "selected";} else{ }}else{ if($lac1['ID'] == $setcity) {echo "selected";} else{ }}?><?php ?>><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												
											</div>
											<div class="col-sm-6">
												<div class="form-group area">
											<label class="control-label"><b>Area</b></label>
											<select class="selectpic"  name="area" required id="txt8" data-live-search="true" style="width: 200px;height: 30px;">
												 <option value="">Select area </option>
												 <?php
 if(isset($contractinfo)) {
            $query=$this->db->query("SELECT * FROM `tbl_location` where Parent='".$contractinfo->City."'")->result_array();
                
                        foreach ($query  as $area)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $area['Name']; ?>" <?php if(isset($contractinfo)) { if($contractinfo->AreaName== $area['ID']) {echo "selected";}}?>><?php echo $area['Name']; ?></option>
                                        
                                        <?php
                                        }
 }
                                        ?>
                                   
												</select>
												</div>
												
												
											</div>
											
										</div>
										
										<div class="row">
											
											<div class="col-sm-6">
											<div class="form-group">
													<label class="control-label" id="editpin"><b>Pincode</b> <i class="fa fa-edit fa-2x" onclick="ToggleReadOnlyState ();"></i></label>
													<input type="text" name="pincode" id="txt9"  rows="3" readonly="readonly"  value="<?php if(isset($contractinfo)) { echo $contractinfo->Pincode;}?>" required class="form-control" >
												
												</div>
												
											</div>
											<div class="col-sm-6">
											<div class="form-group">
													<label class="control-label" id="editpin"><b>Landmark</b> </label>
													<input type="text" name="landmark" id="txt31"  rows="3"  value="<?php if(isset($contractinfo)) { echo $contractinfo->Landmark;}?>" required class="form-control" >
												
												</div>
												
											</div>
											
											
										</div>
										<div class="row">
										<div class="col-sm-6">
												<div class="form-group" id="TextBoxContainer2">
													<label class="control-label"><b>Email</b><input id="btnAdd2" type="button" value="Add" /></label>
													<input type="email" name="email" id="txt4" value="<?php if(isset($contractinfo)) { echo $contractinfo->Email;}?>" required class="form-control">
												</div>
												
											</div>
												<?php if(isset($contractinfo)) { 
											if(!empty(json_decode($contractinfo->SecondaryEmail) & is_array(json_decode($contractinfo->SecondaryEmail))))
												{
											$i=1;												
											 foreach (json_decode($contractinfo->SecondaryEmail) as $email)
											 {
												 
												?>
												<div class="col-sm-6">

												<div class="form-group" id="TextBoxContainer">
													<label class="control-label" ><b>Email<?php echo $i ?></b> </label>
													<input type="text" name = "email2[]" placeholder="Secondary Email" class="form-control" type="email" value="<?php  echo $email;?>">
													
												</div>
												<div class="form-group" id="TextBoxContainer">
													
													
												</div>
											</div>
										<?php										
											$i++;		
											 }
                      
											}
											}
											?>
											<div class="col-sm-6">

												<div class="form-group" id="TextBoxContainer">
													<label class="control-label" ><b>Contact Number</b>  <input id="btnAdd" type="button" value="Add" /></label>
													<input type="text" name="number" maxlength="10"  pattern="[0-9]{10}" id="txt5" value="<?php if(isset($contractinfo)) { echo $contractinfo->Number;}?>" required class="form-control">
													
												</div>
												<div class="form-group" id="TextBoxContainer">
													
													
												</div>
											</div>
											<?php if(isset($contractinfo)) { 
											
												if(!empty(json_decode($contractinfo->Number2) & is_array(json_decode($contractinfo->Number2))))
												{
											$i=1;												
											 foreach (json_decode($contractinfo->Number2) as $numb)
											 {
												 
												?>
												<div class="col-sm-6">

												<div class="form-group" id="TextBoxContainer">
													<label class="control-label" ><b>Contact Number<?php echo $i ?></b> </label>
													<input type="text" name = "number2[]" placeholder="Secondary Number" class="form-control" maxlength="10"  pattern="[0-9]{10}" value="<?php  echo $numb;?>">
													
												</div>
												<div class="form-group" id="TextBoxContainer">
													
													
												</div>
											</div>
										<?php										
											$i++;		
											 }
                      
											}
											}
											?>
										<div class="col-sm-6">
												<div class="form-group" id="TextBoxContainer1">
													<label class="control-label"><b>STD Code</b></label>
													<input type="text" name="stdcode" readonly   id="stdcode" value="<?php if(isset($contractinfo)) { echo $contractinfo->STDcode;} else{ }?>" required class="form-control">
												</div>
												
											</div>
											<div class="col-sm-6">
												<div class="form-group" id="TextBoxContainer1">
													<label class="control-label"><b>Landline</b><input id="btnAdd1" type="button" value="Add" /></label>
													<input type="text" name="landline"  maxlength="10"  pattern="[0-9]{10}" id="txt6" value="<?php if(isset($contractinfo)) { echo $contractinfo->Number;} else{ }?>" required class="form-control">
												</div>
												
											</div>
											<?php if(isset($contractinfo)) { 
											if(!empty(json_decode($contractinfo->SecondaryLandline) & is_array(json_decode($contractinfo->SecondaryLandline))))
												{
											$i=1;												
											 foreach (json_decode($contractinfo->SecondaryLandline) as $land)
											 {
												 
												?>
												<div class="col-sm-6">

												<div class="form-group" id="TextBoxContainer">
													<label class="control-label" ><b>Landline<?php echo $i ?></b> </label>
													<input type="text" name = "landline2[]" placeholder="Secondary Landline" class="form-control"  maxlength="10"  pattern="[0-9]{10}" type="text" value="<?php  echo $land;?>">
													
												</div>
												<div class="form-group" id="TextBoxContainer">
													
													
												</div>
											</div>
										<?php										
											$i++;		
											 }
                      
											}
											}
											?>
											
											
										</div>
										<div class="row">
											
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label"><b>Website</b></label>
													<input type="text" name="web" id="txt11" value="<?php if(isset($contractinfo)) { echo $contractinfo->Web;}?>" required class="form-control">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label"><b>Fax</b></label>
													<input type="text" name="fax" id="txt25" value="<?php if(isset($contractinfo)) { echo $contractinfo->Fax;}?>" required class="form-control">
												</div>
											</div>
										</div>
										
										
										
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group subcat">
											<label class="control-label"><b>Category</b></label>
											<select class="selectpicker"  name="category" id="txt12" data-live-search="true">
												 <option value="">Select Category </option>
												 <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_category` where Parent=0 ")->result_array();
                
                        foreach ($query  as $cat)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $cat['ID']; ?>" <?php if(isset($contractinfo)) { if($contractinfo->Category== $cat['ID']) {echo "selected";}}?>><?php echo $cat['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
                                   
												</select>
												</div>
												
												
											</div>
											
											<div class="col-sm-6">
												<div class="form-group getsubcat">
											<label class="control-label"><b>Sub Category</b></label>
											
										<input type="text" name="subcategory1" id="txt30" value="" readonly  class="form-control" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
										<!--<select class="selectpicker"  name="subcategory[]"   multiple="multiple" id="multiple" >
											
												  <?php
												  /*
            if(isset($contractinfo)) {
            $query=$this->db->query("SELECT * FROM `tbl_category` where Parent='".$contractinfo->Category."'")->result_array();
                
                        foreach ($query  as $cat)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $cat['ID']; ?>" <?php if(isset($contractinfo)) { if(in_array($cat['ID'], json_decode($contractinfo->SubCategory))) {echo "selected";}}?>><?php echo $cat['Name']; ?></option>
                                        
                                        <?php
						}
                                        }
										*/
                                        ?>
                                   
												</select>-->
												 
												</div>
												
												
											</div>
											
										</div>
										<div class="row">
										
							<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Start Time </b></label>
											 <div style="position: relative"> 
        <input class="form-control time" name="starttime" value="<?php if(isset($contractinfo)) { echo json_decode($contractinfo->Time)[0];}?>" type="text" id="txt14"/>
    </div>
											
												</div>
												
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Close Time </b></label>
											 <div style="position: relative"> 
        <input class="form-control time1" name="endtime" type="text" value="<?php if(isset($contractinfo)) { echo json_decode($contractinfo->Time)[1];}?>" id="txt15"/>
    </div>
												</div>
												
												
											</div>
											
											
											
										</div>
											<!--<div class="row">
											
										
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Start Time </b></label>
											 <div style="position: relative"> 
        <input class="form-control time2" name="time[]" type="text" value="<?php //if(isset($contractinfo)) { echo json_decode($contractinfo->Time)[2];}?>" id="txt16"/>
    </div>
												</div>
												
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Close Time </b></label>
											 <div style="position: relative"> 
        <input class="form-control time3" type="text" name="time[]" value="<?php // if(isset($contractinfo)) { echo json_decode($contractinfo->Time)[3];}?>" id="txt17"/>
    </div>
												</div>
												
												
											</div>
											
										</div>-->
										<div class="row">
											
										
											<div class="col-sm-12">
												<div class="form-group">
											<label class="control-label"><b>Holiday days</b></label>
											 <div class="checkbox">
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="1" <?php if(isset($contractinfo)) {if (in_array("1", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Monday</label>
											<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="2" <?php if(isset($contractinfo)) {if (in_array("2", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>>Tuesday</label>
																						
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="3" <?php if(isset($contractinfo)) {if (in_array("3", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Wednesday</label>
												<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="4" <?php if(isset($contractinfo)) {if (in_array("4", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Thursday</label>
											<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="5" <?php if(isset($contractinfo)) {if (in_array("5", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Friday</label>
																						
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="6" <?php if(isset($contractinfo)) {if (in_array("6", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Saturday</label>
												<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="workday[]" value="7" <?php if(isset($contractinfo)) {if (in_array("7", json_decode($contractinfo->Workingday))) { echo "checked";}} ?>> Sunday</label>
											</div>
												
												</div>
												
												
										
												
											
											
												
												
											</div>
											
										</div>
										<div class="row">
											
										
											<div class="col-sm-12">
												<div class="form-group">
											<label class="control-label"><b>Payment Method</b></label>
											 <div class="checkbox">
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="payment[]" value="Transfer" <?php if(isset($contractinfo)) {if (in_array("Transfer", json_decode($contractinfo->PaymentType))) { echo "checked";}} ?>>Transfer </label>
											<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="payment[]" value="Card" <?php if(isset($contractinfo)) {if (in_array("Card", json_decode($contractinfo->PaymentType))) { echo "checked";}} ?>> Card</label>
																						
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="payment[]" value="Cash" <?php if(isset($contractinfo)) {if (in_array("Cash", json_decode($contractinfo->PaymentType))) { echo "checked";}} ?>> Cash</label>
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="payment[]" value="Cheque" <?php if(isset($contractinfo)) {if (in_array("Cheque", json_decode($contractinfo->PaymentType))) { echo "checked";}} ?>> Cheque </label>
												
											</div>
												
												</div>
	
											</div>
											
										</div>
										<div class="row">
											
										
											<div class="col-sm-12">
												<div class="form-group">
											<label class="control-label"><b>Facility/Amenities</b></label>
											 <div class="checkbox">
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="facility[]" value="Home Delivery" <?php if(isset($contractinfo)) {if (in_array("Home Delivery", json_decode($contractinfo->Facility))) { echo "checked";}} ?>>Home Delivery </label>
											<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="facility[]" value="0 EMI" <?php if(isset($contractinfo)) {if (in_array("0 EMI", json_decode($contractinfo->Facility))) { echo "checked";}} ?>> 0 EMI</label>
																						<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="facility[]" value="Free Parking" <?php if(isset($contractinfo)) {if (in_array("Free Parking", json_decode($contractinfo->Facility))) { echo "checked";}} ?>> Free Parking</label>
													<label class="form-checkbox form-normal form-primary form-text active"><input type="checkbox" name="facility[]" value="Free Wifi" <?php if(isset($contractinfo)) {if (in_array("Free Wifi", json_decode($contractinfo->Facility))) { echo "checked";}} ?>> Free Wifi</label>
												
											</div>
												
												</div>
								
												
											</div>
											
										</div>
										<div class="row">
											
										
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Language </b></label>
											 <select class="selectpicker"  name="language[]" id="txt20" multiple="multiple" data-live-search="true">
												 <option value="">Select Language </option>
												 <option value="Kannada">Kannada</option>
												<option value="Hindi">Hindi</option>
												<option value="English">English</option>
												<option value="Telugu">Telugu </option>
												<option value="Tamil">Tamil </option>
												<option value="Malayalam">Malayalam </option>
												<option value="Odia">Odia </option>
												</select>
      <!--  <input class="form-control" type="text" name="language" id="txt20" value="<?php if(isset($contractinfo)) { echo $contractinfo->Language;}?>"/>-->
  
												</div>
												
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Tollfree</b></label>
											
        <input class="form-control" type="text" name="tollfree" id="tollfree" value="<?php if(isset($contractinfo)) { echo $contractinfo->Tollfree;}?>"/>
  
												</div>
												
												
											</div>
											<div class="col-sm-12">
												<div class="form-group">
											<label class="control-label"><b>About Company</b></label>
											<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
       <textarea class="note-codable" name="aboutcompany" id="abtcompany"></textarea>
  
												</div>
												
												
											</div>
										</div>
									</div>
									<div class="panel-footer text-center">
										<button class="btn btn-info" type="submit" name="to_web">Submit To Website</b></button>
										<?php if(!isset($contractinfo)) { ?>
										<button class="btn btn-info" type="submit" name="to_quality">Submit To Quality</b></button>
										<a class="btn btn-info" onclick="document.getElementById('light1').style.display='block';document.getElementById('fade').style.display='block'">Disposition</b></a>
										<?php
										}
										?>
									</div>
									
								
								<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
						<!--start subcategory -->
						<div id="light" class="white_content"> 
  <!--===================================================-->
								
									<div class="form-group district">
											
												</div>
												<div class="col-sm-12">
												<div class="form-group">
											<label class="control-label"><b>Sub Category</b></label>
											 <div class="checkbox getsubcat1">
												<input type="checkbox" id="multiple" name="subcategory[]" value="258" >	
											
											</div>
												
												</div>
												
											<div class="panel-footer text-center">	
										<a href="javascript:void(0)" id="check" class="btn btn-info" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" style="color:#002780; font-size: :30px;">Submit</a>		
								 
												
											</div>
								 
												
											
											
												
												
											</div>
												
								
  </div>
  <!--end subcategory -->
  <!--start Disposition -->
  <style>
  option:disabled {
   background: red;
   width: 500px;
   padding: 5px;
   }
  </style>
							<div id="light1" class="white_content"> 
  <!--===================================================-->
								
								
												<div class="col-sm-12">
												<div class="form-group">
											
											<label class="control-label ">Disposition</label>
											<select class="selectpicker"  name="disposition" id="disposition"   data-live-search="true">
												 <option value="">Select Disposition </option>
												 <option value="1">Call Back</option>
												  <option value="2">Switch Off</option>
												   <option value="3">Not Contactable</option>
												    <option value="4">Do Not Disturb </option>
													 <option value="5">Not Reachable </option>
													  <option value="6">Follow UP </option>
													   <option value="7">Not Interested</option>
													    <option value="8">RNR </option>
														 <option value="9">Not in Business</option>
														  <option value="10">Wrong Number</option>
														   <option value="11">Government sectors </option>
														    <option value="12">Appointment </option>
														   
												 
                                   
												</select>
												
												
												</div>
												<div id="appointment" style="display:none;">
												<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Date </b></label>
											 
        <input class="form-control" type="date" name="date" id="medate" value=""/>
  
												</div>

											</div>
											<div class="col-sm-6">
												<div class="form-group me">
											
											<label class="control-label ">ME List</label>
											<select class="selectme"  name="me" id="me" style="width: 200px;height: 30px;" >
												 <option value="">Select ME </option>
                             
												</select>
												
												
												</div>
												
												
											</div>
											
										<div class="col-sm-6 metime">
												<div class="form-group">
											
											<label class="control-label ">Time</label>
											<select name="end_hour"  class="selecttime"  id="metime" data-live-search="true" style="width: 200px;height: 30px;">
			
			</select>
												
												
												</div>
												
												
											</div>
											
											
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Feedback</b></label>
											
        <input type="text"  name="mefeedback" style="resize:none;width:200px;height:60px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;"  id="mfeedback"  placeholder="Feedback" >
  
												</div>
												
												
											</div>
											
											
										</div>
												<div id="callback" style="display:none;">
											
										
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Date </b></label>
											 
        <input class="form-control" type="date" name="date" id="txt31" value=""/>
  
												</div>

											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Time</b></label>
											
        <input class="form-control" type="time" name="time" id="txt32" value=""/>
  
												</div>
												
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label"><b>Feedback</b></label>
											
        <textarea  name="feedback" style="resize:none" rows="6" cols="35" id="txt33"  >Feedback</textarea>
  
												</div>
												
												
											</div>
											
										</div>
											<div id="interest" style="display:none;">
											
										
									
											<div class="col-sm-12">
												<div class="form-group">
											
											
        <textarea  name="feedback" style="resize:none" rows="6" cols="35"  value="">Feedback</textarea>
  
												</div>
												
												
											</div>
											
										</div>
												
												
											</div>
											<div class="row col-lg-12">
							<br />
							<div class="col-lg-2"></div>
							<div class="col-lg-4"><a href="javascript:void(0)"  onclick="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none'" ><input id="dispositionsave" type="button" value="Submit" class="btn btn-purple text-uppercase"></a>
								</div>
							<div class="col-lg-4">
								<a href="javascript:void(0)" class="btn btn-danger text-uppercase" onclick="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none'" >Close</a>
							</div>
							<div class="col-lg-2"></div>
						</div>
												
								
  </div>
  <!--end Disposition -->
  </form>
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Information  <b>( <?php if(isset($contractinfo)) { echo $contractinfo->ContractID;} else{ }?> )</b></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								
									<div class="panel-body">
								<style> 


#example2 {
    border: 1px solid;
    padding: 10px;
    box-shadow: 3px 3px #888888;
}


</style>
									<address>
									<div id="example2">
<h4>Company Details</h4> 
<p id="company"><b>Company Name:  <?php if(isset($contractinfo)) { echo $contractinfo->	CompanyName;}?></b> </p>
<p id="tagline"><b>Company Tagline:  <?php if(isset($contractinfo)) { echo $contractinfo->Tagline;}?></b> </p>
<p id="person"><b>Contact Person:  <?php if(isset($contractinfo)) { echo $contractinfo->ContactPerson;}?></b> </p>
<p id="number"><b>Number:  <?php if(isset($contractinfo)) { echo $contractinfo->Number;}?></b> </p>
<p id="landline"><b>Landline: <?php if(isset($contractinfo)) { echo $contractinfo->Landline;}?></b> </p>
<p id="email"><b>Email: <?php if(isset($contractinfo)) { echo $contractinfo->Email;}?></b> </p>
<p id="web"><b>Website:  <?php if(isset($contractinfo)) { echo $contractinfo->Web;}?></b> </p>
<p id="fax"><b>Fax:  <?php if(isset($contractinfo)) { echo $contractinfo->Fax;}?></b> </p>
</div>
<div id="example2">
<h4>Address Deatils</h4> 
<p id="address"><b>Address: <?php if(isset($contractinfo)) { echo $contractinfo->Address;}?></b> </p>
<p id="city"><b>City: <?php if(isset($contractinfo)) { $City= $this->db->get_where('tbl_location' , array('ID' => $contractinfo->City))->row_array(); 
echo $City['Name'];} else{ }?></b> </p>
<p id="area"><b>Area:   <?php if(isset($contractinfo)) { echo $contractinfo->AreaName;}?></b> </p>
<p id="pincode"><b>Pincode:  <?php if(isset($contractinfo)) { echo $contractinfo->Pincode;}?></b> </p>
</div>
<div id="example2">
<h4>Ctegory Deatils</h4> 

<p id="cat"><b>Category Name: <?php if(isset($contractinfo)) { $category= $this->db->get_where('tbl_category' , array('ID' => $contractinfo->Category))->row_array(); 
echo $category['Name'];} ?></b> </p>
<b>Sub Category Name: </b><p id="sucatresult"> </p>
</div>
</address>
									
										
								
									</div>
									
								
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
		

<?php include('footer.php');?>
