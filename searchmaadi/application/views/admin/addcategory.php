<?php include('header.php');?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<script type="text/javascript">
  function showImage(src, target1) {
            var fr = new FileReader();

             fr.onload = function(){
      target1.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putsmallImage() {
            var src = document.getElementById("select_smallimage");
            var target = document.getElementById("target1");
            showImage(src, target1);
        }
		 function showImage(src, target2) {
            var fr = new FileReader();

             fr.onload = function(){
      target2.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
		function putbigImage() {
            var src = document.getElementById("select_bigimage");
            var target = document.getElementById("target2");
            showImage(src, target2);
        }
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Category Info</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($cat)) {echo "Update Category" ; } else{ echo "Add Category" ;} ?></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>admin/storecategory" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($cat)) {echo $cat->ID;}?>" name="id">
									<input type="hidden" value="<?php if(isset($cat)) {echo $cat->SmallImage;}?>" name="smallimageupdate">
									<input type="hidden" value="<?php if(isset($cat)) {echo $cat->BigImage;}?>" name="bigimageupdate">
									<div class="form-group">
											<label class="col-md-4 control-label">Category Small Image (186X186 px(max 100 kb))</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
												<input <?php if(!isset($cat)) {echo "required";}?> type="file" id="select_smallimage" name="smallimage" onchange="putsmallImage()" />
												<?php if(!isset($cat)) { ?> <img id="target1" height="100" width="100"/> <?php } else{ ?>
												 <img id="target1" height="100" width="100"  src="<?php echo base_url().$cat->SmallImage; ?>"/><?php }?>
											</span>
										</div>
									
									</div>
									<div class="form-group">
											<label class="col-md-4 control-label">Category Large Image (1045X423 px(max 500 kb))</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
											<input <?php if(!isset($cat)) {echo "required";}?> type="file" id="select_bigimage" name="bigimage" onchange="putbigImage()" />
											<?php if(!isset($cat)) { ?> <img id="target2" height="100" width="100" /><?php } else{ ?>
											 <img id="target2" height="100" width="100" src="<?php echo base_url().$cat->BigImage;?>"/><?php }?>
											</span>
										</div>
									</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Category Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Category Name" value="<?php if(isset($cat)) {echo $cat->Name;}?>" name="name" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Kannada Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Kannada Name" name="kannada" value="<?php if(isset($cat)) {echo $cat->Kannada;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Category Type</label>
											
											<select class="selectpicker"  name="cattype" id="cattype" required>
												 <option value="">Category Type</option>
												 <option value="B2B" <?php if(isset($cat)) { if($cat=="B2B"){ echo "selected";}}?>>B2B</option>
												 <option value="B2C" <?php if(isset($cat)) { if($cat=="B2C"){ echo "selected";}}?>>B2C</option>
											</select> 	
										</div>
										
											<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputdesc">Description:</label><br/><br/>
												<textarea name="description" rows="3" cols="60" placeholder="Description"><?php if(isset($cat)) {echo $cat->Description;}?></textarea>
												
											
										</div>
									
									</div>
									<div class="panel-footer text-right">
									<a href="<?php echo base_url();?>admin/categoryinfo" class="btn btn-purple btn-labeled fa fa-eye">View List</a>
										
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($subcat)) { echo "Update Sub-Category" ; } else{ echo "Add Sub-Category" ; } ?></h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<form action="<?php echo base_url();?>admin/storesubcategory" method="post">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($subcat)) {echo $subcat->ID;}?>" name="id">
										<div class="row">
											<div class="col-sm-6">
											<div class="form-group">
											<label class="control-label">Category Name</label>
											<select class="selectpicker"  name="cat" required>
												 <option value="">Select Category </option>
                                    <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_category` where parent=0")->result_array();
                
                        foreach ($query  as $cat1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $cat1['ID']; ?>" <?php if(isset($subcat)) {if($cat1['ID'] == $subcat->Parent){ echo "selected";}}?>><?php echo $cat1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">Sub Category Name</label>
													<input type="text" name="name" value="<?php if(isset($subcat)) {echo $subcat->Name;}?>" required class="form-control">
												</div>
												
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">Kannada Name</label>
													<input type="text" name="kannada" value="<?php if(isset($subcat)) {echo $subcat->Kannada;}?>" required class="form-control">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
											<label class="control-label" for="demo-hor-inputpass">Category Type</label>
											
											<select class="selectpicker"  name="subcattype" id="subcattype" required>
												 <option value="">Category Type</option>
												 <option value="B2B" <?php if(isset($subcat)) { if($subcat=="B2B"){ echo "selected";}}?>>B2B</option>
												 <option value="B2C" <?php if(isset($subcat)) { if($subcat=="B2C"){ echo "selected";}}?>>B2C</option>
											</select> 	
										</div>
											</div>
											
										</div>
									</div>
									<div class="panel-footer text-right">
										<a href="<?php echo base_url();?>admin/categoryinfo" class="btn btn-purple btn-labeled fa fa-eye">View List</a>
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
