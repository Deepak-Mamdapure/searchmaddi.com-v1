<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Allocate ME Agents</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($allocations)) {echo "Update Allocate ME Agents";}else{echo "Allocate ME Agents";} ?></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>report/saveallocatedme" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($allocations)) {echo $allocations->ID;}?>" name="id">
									<input type="hidden" value="<?php if(isset($allocations)) {echo $this->session->userdata('Emp_ID');}?>" name="dbid">
									<div class="col-lg-12">
									
									
										<div class="col-md-4 city">
									<br />
											<select class="selectpicker pull-right"  name="city"  data-live-search="true" required >
												<option value="">Select City </option>
												<?php
															 $query=$this->db->query("SELECT * FROM `tbl_location` where `flag` = '0'")->result_array();
															foreach ($query  as $lac1){ 
												?>
                                        <option value="<?php echo $lac1['ID']; ?>" <?php if($allocations->City == $lac1['ID']){ echo "selected";}?>><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>	
									</div>
										<div class="col-md-4 area">
										<br />
									<select class="selectpic"  name="area" data-live-search="true" style="width: 200px;height: 30px;">
										<option value="">Select Area </option>
																					<?php
															 $query1=$this->db->query("SELECT * FROM `tbl_location` ")->result_array();
															foreach ($query1  as $lac2){ 
												?>
                                        <option value="<?php echo $lac2['ID']; ?>" <?php if($allocations->Area == $lac2['ID']){ echo "selected";}?>><?php echo $lac2['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
									</select>
								</div>
									<div class="col-md-4">
											<br />
												<select class="selectpicker"  name="meid" required id="txt12" >
												 <option value="">Select ME Agent </option>
												 <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_admin`")->result_array();
                     foreach ($melist as $mename)
                        { 
                        
                        
                        ?>
      <option value="<?php echo $mename->Emp_ID; ?>" <?php if(isset($allocations)) { if($allocations->ME_id == $mename->Emp_ID){ echo "selected";}}?>><?php echo $mename->Name; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
                                   
												</select>
										</div>
									</div>
									</div>
									<div class="panel-footer text-center">
									<a href="<?php echo base_url();?>report/meallocationlist" class="btn btn-purple btn-labeled fa fa-eye">View List</a>
									
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
