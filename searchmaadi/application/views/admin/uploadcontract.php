<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Upload Contract Data</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Upload To Website</h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>admin/contractimport" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Contract CSV File</label>
										<input type="hidden" name="role" value="7">
										<div class="col-md-9">
											
<input  type="file"  name="file" class="form-control" onchange="putImage()" />
 
											
										</div>
									</div>
										
									
									</div>
									<div class="panel-footer text-right">
										<button class="btn btn-info" type="submit">Upload To Website</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Upload To B-Form</h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<form action="<?php echo base_url();?>admin/contractimport" method="post" enctype="multipart/form-data">
									<div class="panel-body">
									
										
										<div class="row">
												<div class="form-group">
										<label class="col-md-3 control-label">Contract CSV File</label>
										<input type="hidden" name="role" value="3">
										<div class="col-md-9">
											
<input  type="file"  name="file" class="form-control" onchange="putImage()" />
 
											
										</div>
									</div>
											
										</div>
									</div>
									<div class="panel-footer text-right">
										<button class="btn btn-info" type="submit">Upload To B-Form</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
