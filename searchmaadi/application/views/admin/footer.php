

            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            <nav id="mainnav-container">
                <div id="mainnav">

                    <!--Shortcut buttons-->
                    <!--================================-->
             <!-- 
       <div id="mainnav-shortcut">
                        <ul class="list-unstyled">
                            <li class="col-xs-4" data-content="Additional Sidebar">
                                <a id="demo-toggle-aside" class="shortcut-grid" href="#">
                                    <i class="fa fa-magic"></i>
                                </a>
                            </li>
                            <li class="col-xs-4" data-content="Notification">
                                <a id="demo-alert" class="shortcut-grid" href="#">
                                    <i class="fa fa-bullhorn"></i>
                                </a>
                            </li>
                            <li class="col-xs-4" data-content="Page Alerts">
                                <a id="demo-page-alert" class="shortcut-grid" href="#">
                                    <i class="fa fa-bell"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
 -->
                    <!--================================-->
                    <!--End shortcut buttons-->


                    <!--Menu-->
                    <!--================================-->
                    <div id="mainnav-menu-wrap">
                        <div class="nano">
                            <div class="nano-content">
                                <ul id="mainnav-menu" class="list-group">
						
						            <!--Category name-->
						           
						
						            <!--Menu list item-->
						            <li class="active-link">
						                <a href="<?php echo base_url();?>admin">
						                    <i class="fa fa-dashboard"></i>
						                    <span class="menu-title">
												<strong>Dashboard</strong>
												
											</span>
						                </a>
						            </li>
									  <li class="list-divider"></li>
									  <?php 
																
									if($this->session->userdata('Role')==1)
									{
										
									?>
						           <li>
						                <a href="#">
						                    <i class="fa fa-plus fa-2x"></i>
						                    <span class="menu-title">
												<strong>Add Important Data</strong>
											</span>
											<i class="arrow"></i>
						                </a>
										
											
						               
										 
										 <ul class="collapse">
                                                    <li><a href="<?php echo base_url();?>report/famouspersons">Famous Personalities</a></li>
                                                    <li><a href="<?php echo base_url();?>admin/categoryinfo">Category</a></li>
													<li><a href="<?php echo base_url();?>report/addcitywisecategory">Add City Wise Category</a></li>
                                                    <li><a href="<?php echo base_url();?>report/facilitiesinfo">Facilities & Aminities</a></li>
													<li><a href="<?php echo base_url();?>admin/locationinfo">Location</a></li>
                                                    <li><a href="<?php echo base_url();?>report/addeventtype">Add Event Type</a></li>
													<li><a href="<?php echo base_url();?>report/addsourcetype">Add Source Type</a></li>
											  
											 </ul>
						               
										
						            </li>
									
									<li class="list-divider"></li>
									
									  <li>
						                <a href="#">
						                    <i class="fa fa-industry fa-2x"></i>
						                    <span class="menu-title">
												<strong>Manage Contract</strong>
											</span>
											<i class="arrow"></i>
						                </a>
						
						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="<?php echo base_url();?>admin/uploadcontract">Upload Data</a></li>
											<li><a href="<?php echo base_url();?>report/contractlist/3">TME Data</a></li>
											<li><a href="<?php echo base_url();?>report/contractlist/4">ME Data</a></li>
											<li><a href="<?php echo base_url();?>report/contractlist/5">Quality Data</a></li>
											<li><a href="<?php echo base_url();?>report/contractlist/6">Database Data</a></li>
											<!--<li><a href="<?php echo base_url();?>admin/tmeinfo/6">Tech Data</a></li>
											<li><a href="<?php echo base_url();?>admin/tmeinfo/7">Direct Data</a></li>-->
											
											
						                </ul>
						            </li>
									 <li class="list-divider"></li>
						           
									<li>
						                <a href="#">
						                    <i class="fa fa-user fa-2x"></i>
						                    <span class="menu-title">
												<strong>Customer</strong>
												
											</span>
						                </a>
						            </li>
									
									
									<li class="list-divider"></li>
									<li>
						                <a href="#">
						                    <i class="fa fa-tripadvisor fa-2x"></i>
						                    <span class="menu-title">
												<strong>Visitors</strong>
												
											</span>
						                </a>
						            </li>
									
						             <li class="list-divider"></li>
						            
						            <li>
						                <a href="#">
						                    <i class="fa fa-cog fa-2x"></i>
						                    <span class="menu-title">
												<strong>Settings</strong>
											</span>
											<i class="arrow"></i>
						                </a>
						
						                <!--Submenu-->
						                <ul class="collapse">
						                    <li><a href="<?php echo base_url();?>user/userdetail">User</a></li>
											<li><a href="#">Academic Details</a></li>
											<li><a href="#">Departments</a></li>
											<li><a href="#">Add User Types</a></li>
											<li><a href="#">Designations</a></li>
											<li><a href="#">Fees Allocation Import</a></li>
											  <li>
                                                <a href="#">Location<i class="arrow"></i></a>

                                                <!--Submenu-->
                                                <ul class="collapse">
                                                    <li><a href="#">Country</a></li>
                                                    <li><a href="#">States</a></li>
                                                    <li><a href="#">Location</a></li>
                                            
                                                </ul>
                                            </li>
											
											
						                </ul>
						            </li>
						             <li class="list-divider"></li>
						<?php
									}
												
									else if($this->session->userdata('Role')==7){
										?>
												<li>
						                <a href="<?php echo base_url();?>iro/">
						                    <i class="fa fa-tags"></i>
						                    <span class="menu-title">
												<strong>Search Data</strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>iro/datanotfound">
						                    <i class="fa fa-question-circle 2x"></i>
						                    <span class="menu-title">
												<strong>Data Not Found</strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-map-marker"></i>
						                    <span class="menu-title">
												<strong>Location</strong>
												
											</span>
						                </a>
						            </li>
									
									 <li class="list-divider"></li>
						           
									<li>
						                <a href="#">
						                    <i class="fa fa-user fa-2x"></i>
						                    <span class="menu-title">
												<strong>Customers</strong>
												
											</span>
						                </a>
						            </li>
									
									
										
										<?php 
									}
												
									else if($this->session->userdata('Role')==9)
									{
										
									?>
									<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-briefcase fa-2x"></i>
						                    <span class="menu-title">
												<strong>Company Details</strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
										<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-calendar fa-2x"></i>
						                    <span class="menu-title">
												<strong>Manage Events</strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
										<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-gift fa-2x"></i>
						                    <span class="menu-title">
												<strong>Manage Offers</strong>
												
											</span>
						                </a>
						            </li>
									
						<?php 
									}						
									else
									{
									?>
									<!-- start Database-->
									<?php
									
										if	($this->session->userdata('Role')==6)
									{	
											  
						          if($this->router->fetch_method()=="addcontract" || $this->router->fetch_method()=="allocated" || $this->router->fetch_method()=="newbusiness")
									{
										
									?>
						           
											<li>
						                <a href="<?php echo base_url();?>admin/addcontract">
						                    <i class="fa fa-plus fa-2x"></i>
						                    <span class="menu-title">
												<strong>Add contract</strong>
												
											</span>
						                </a>
						            </li>
											<li class="list-divider"></li>
											<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-briefcase fa-2x"></i>
						                    <span class="menu-title">
												<strong>New Business</strong>
												
											</span>
						                </a>
						            </li>
											 
											 <li class="list-divider"></li>
											  
											  <li>
						                <a href="#">
						                    <i class="fa fa-bar-chart fa-2x "></i>
						                    <span class="menu-title">
												<strong>Allocated Data</strong>
												
											</span>
						                </a>
						            </li>
											  <?php
									}
									else
									{
									?>
									 <li>
						                <a href="#">
						                    <i class="fa fa-user fa-2x"></i>
						                    <span class="menu-title">
												<strong>Mithra Login/ Visiting Card</strong>
											</span>
											<i class="arrow"></i>
						                </a>
										
											
						               
										 
										 <ul class="collapse">
                                                    <li><a href="<?php echo base_url();?>admin/allocated">Bangalore</a></li>
											
											
											  <li>
                                                <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Smart City</a>

                                                <!--Submenu-->
                                                
                                            </li>
                                            
                                               
										 
						
											 </ul>
						               
										
						            </li>
									<li class="list-divider"></li>
									
									<li>
						                <a href="<?php echo base_url();?>report/contractlist/6">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>Contract Details</strong>
												
											</span>
						                </a>
						            </li>
								<li class="list-divider"></li>
								 <li>
						                <a href="#">
						                    <i class="fa fa-plus fa-2x"></i>
						                    <span class="menu-title">
												<strong>Add Important Data</strong>
											</span>
											<i class="arrow"></i>
						                </a>
										
											
						               
										 
										 <ul class="collapse">
                                                    <li><a href="<?php echo base_url();?>report/famouspersons">Famous Personalities</a></li>
                                                    <li><a href="<?php echo base_url();?>admin/categoryinfo">Category</a></li>
													<li><a href="<?php echo base_url();?>report/addcitywisecategory">Add City Wise Category</a></li>
                                                    <li><a href="<?php echo base_url();?>report/facilitiesinfo">Facilities & Aminities</a></li>
													<li><a href="<?php echo base_url();?>admin/locationinfo">Location</a></li>
                                                    <li><a href="<?php echo base_url();?>report/addeventtype">Add Event Type</a></li>
													<li><a href="<?php echo base_url();?>report/addsourcetype">Add Source Type</a></li>
											  
											 </ul>
						               
										
						            </li>
									<li class="list-divider"></li>
								 <li>
						                <a href="#">
						                    <i class="fa fa-eye fa-2x"></i>
						                    <span class="menu-title">
												<strong>Add & View Agents Data</strong>
											</span>
											<i class="arrow"></i>
						                </a>
										
											
						               
										 
										 <ul class="collapse">
                                                    <li><a href="<?php echo base_url();?>report/meallocationlist">ME Allocation</a></li>
                                                    <li><a href="<?php echo base_url();?>report/irodatanotfound">Data Not Found From IRO</a></li>
													
											  
											 </ul>
						               
										
						            </li>
									
									
									<li class="list-divider"></li>
										<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-tasks fa-2x"></i>
						                    <span class="menu-title">
												<strong>View Reports </strong>
												
											</span>
											<i class="arrow"></i>
						                </a>
										 <ul class="collapse">
										 
										 
						
											  <li><a href="<?php echo base_url();?>report/onlinedatalist">Online Data</a></li>
											  <li><a href="<?php echo base_url();?>report/eventlists">Contract Event List</a></li>
											 <li><a href="<?php echo base_url();?>report/offerlists">Offers</a></li>
											 </ul>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-users fa-2x"></i>
						                    <span class="menu-title">
												<strong>HRMS </strong>
												
											</span>
											<i class="arrow"></i>
						                </a>
										 <ul class="collapse">
										 
										 
						
											 <li><a href="<?php echo base_url();?>">Attendance</a></li>
											  <li><a href="<?php echo base_url();?>">Health</a></li>
											  <li><a href="<?php echo base_url();?>">Payslip</a></li>
											 </ul>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>IRO </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>Mail </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>SMD </strong>
												
											</span>
						                </a>
						            </li>
									<?php
									}
									}
									?>
									  <li class="list-divider"></li>
						            <!--Menu list item-->
									<?php
									
										if	($this->session->userdata('Role')==5)
									{	
											  ?>
						          
									<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>Contract Details</strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-users fa-2x"></i>
						                    <span class="menu-title">
												<strong>HRMS </strong>
												
											</span>
											<i class="arrow"></i>
						                </a>
										 <ul class="collapse">
										 
										 
						
											 <li><a href="<?php echo base_url();?>">Attendance</a></li>
											  <li><a href="<?php echo base_url();?>">Health</a></li>
											  <li><a href="<?php echo base_url();?>">Payslip</a></li>
											 </ul>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>IRO </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>Mail </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>SMD </strong>
												
											</span>
						                </a>
						            </li>
									<?php
									}
									?>
									  <li class="list-divider"></li>
									  <?php 
										if	($this->session->userdata('Role')==3)
									{					
									if($this->router->fetch_method()=="addcontract" || $this->router->fetch_method()=="allocated"|| $this->router->fetch_method()=="newbusiness")
									{
										
									?>
						           
											<li>
						                <a href="<?php echo base_url();?>admin/addcontract">
						                    <i class="fa fa-plus fa-2x"></i>
						                    <span class="menu-title">
												<strong>Add contract</strong>
												
											</span>
						                </a>
						            </li>
											<li class="list-divider"></li>
											<li>
						                <a href="<?php echo base_url();?>admin/newbusiness">
						                    <i class="fa fa-briefcase fa-2x"></i>
						                    <span class="menu-title">
												<strong>New Business</strong>
												
											</span>
						                </a>
						            </li>
											 
											 <li class="list-divider"></li>
											  
											  <!--<li>
						                <a href="#">
						                    <i class="fa fa-bar-chart fa-2x "></i>
						                    <span class="menu-title">
												<strong>Allocated Data</strong>
												
											</span>
						                </a>
						            </li>-->
											 <?php
									}
									elseif($this->session->userdata('Role')==7){
									
									?>
									<?php
									}
									else{
									
										
									?>
									 <li>
						                <a href="#">
						                    <i class="fa fa-user fa-2x"></i>
						                    <span class="menu-title">
												<strong>Mithra Login</strong>
											</span>
											<i class="arrow"></i>
						                </a>
										
											
						               
										 
										 <ul class="collapse">
                                                    <li><a href="<?php echo base_url();?>admin/allocated">Bangalore</a></li>
											
											
											  <li>
                                                <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Smart City</a>

                                                <!--Submenu-->
                                                
                                            </li>
                                            
                                               
										 
						
											 </ul>
						               
										
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>Report </strong>
												
											</span>
											<i class="arrow"></i>
						                </a>
										 <ul class="collapse">
										 
										 
						
											 <li><a href="<?php echo base_url();?>admin/datareport">Data Report</a></li>
											  <li><a href="<?php echo base_url();?>admin/contractreport">Contract Report</a></li>
											  <li><a href="<?php echo base_url();?>admin/salereport">Sale Report</a></li>
											  <li><a href="<?php echo base_url();?>admin/bouncereport">Bounce Report</a></li>
											 </ul>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-users fa-2x"></i>
						                    <span class="menu-title">
												<strong>HRMS </strong>
												
											</span>
											<i class="arrow"></i>
						                </a>
										 <ul class="collapse">
										 
										 
						
											 <li><a href="<?php echo base_url();?>">Attendance</a></li>
											  <li><a href="<?php echo base_url();?>">Health</a></li>
											  <li><a href="<?php echo base_url();?>">Payslip</a></li>
											 </ul>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/irototme">
						                    <i class="fa fa-bar-chart fa-2x"></i>
						                    <span class="menu-title">
												<strong>IRO </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>Mail </strong>
												
											</span>
						                </a>
						            </li>
									<li class="list-divider"></li>
									<li>
						                <a href="<?php echo base_url();?>admin/">
						                    <i class="fa fa-envelope fa-2x"></i>
						                    <span class="menu-title">
												<strong>SMD </strong>
												
											</span>
						                </a>
						            </li>
									<?php
									}
									
									}
									}
									
									?>
									<li class="list-divider"></li>
									
						            <!--Menu list item-->
						        

						
						            
						
						            <!--Category name-->
						            
						         
						             
						            
						          
						          
						               
						           
						            
						           
						           
                                </ul>



                            </div>
                        </div>
                    </div>
                    <!--================================-->
                    <!--End menu-->

                </div>
            </nav>
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
			<div id="light" class="white_content">Click here to. <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" style="color:#002780; font-size: :30px;">Close</a>
  <!--===================================================-->
								<form action="<?php echo base_url();?>admin/allocated/" method="post">
									<div class="form-group district">
											<label class="control-label ">District Name</label>
											<select class="selectpicker"  name="city" required onchange="this.form.submit()"  data-live-search="true">
												 <option value="">Select District </option>
												 
                                    <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_district` where parent=1 and ID!=38")->result_array();
                
                        foreach ($query  as $lac1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $lac1['ID']; ?>" ><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												<div class="panel-footer text-center">	
										<a href="javascript:void(0)" id="check" class="btn btn-info" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" style="color:#002780; font-size: :30px;">Close</a>		
								 
												
											</div>
								 
												<!--<div class="form-group city">
											<label class="control-label">City Name</label>
											<select class="selectpic"  name="city" required onchange="this.form.submit()" data-live-search="true" style="width: 200px;height: 30px;">
												 <option value="">Select City </option>
												 
                                    
												</select>
												</div>-->
									
								</form>
  </div>
			<!--ASIDE-->
			<!--===================================================-->
			<aside id="aside-container">
				<div id="aside">
					<div class="nano">
						<div class="nano-content">
							
							<!--Nav tabs-->
							<!--================================-->
							<ul class="nav nav-tabs nav-justified">
								<li class="active">
									<a href="#demo-asd-tab-1" data-toggle="tab">
										<i class="fa fa-comments"></i>
										<span class="badge badge-purple">7</span>
									</a>
								</li>
								<li>
									<a href="#demo-asd-tab-2" data-toggle="tab">
										<i class="fa fa-info-circle"></i>
									</a>
								</li>
								<li>
									<a href="#demo-asd-tab-3" data-toggle="tab">
										<i class="fa fa-wrench"></i>
									</a>
								</li>
							</ul>
							<!--================================-->
							<!--End nav tabs-->



							<!-- Tabs Content -->
							<!--================================-->
							<div class="tab-content">

								<!--First tab (Contact list)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<div class="tab-pane fade in active" id="demo-asd-tab-1">
									<h4 class="pad-hor text-thin">
										<span class="pull-right badge badge-warning">3</span> Family
									</h4>

									<!--Family-->
									<div class="list-group bg-trans">
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av2.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Stephen Tran</div>
												<span class="text-muted">Availabe</span>
											</div>
										</a>
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av4.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Brittany Meyer</div>
												<span class="text-muted">I think so</span>
											</div>
										</a>
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av3.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Donald Brown</div>
												<span class="text-muted">Lorem ipsum dolor sit amet.</span>
											</div>
										</a>
									</div>


									<hr>
									<h4 class="pad-hor text-thin">
										<span class="pull-right badge badge-info">4</span> Friends
									</h4>

									<!--Friends-->
									<div class="list-group bg-trans">
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av5.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Betty Murphy</div>
												<span class="text-muted">Bye</span>
											</div>
										</a>
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av6.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Olivia Spencer</div>
												<span class="text-muted">Thank you!</span>
											</div>
										</a>
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av4.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Sarah Ruiz</div>
												<span class="text-muted">2 hours ago</span>
											</div>
										</a>
										<a href="#" class="list-group-item">
											<div class="media-left">
												<img class="img-circle img-xs" src="<?php echo base_url();?>dash/img/av3.png" alt="Profile Picture">
											</div>
											<div class="media-body">
												<div class="text-lg">Paul Aguilar</div>
												<span class="text-muted">2 hours ago</span>
											</div>
										</a>
									</div>


									<hr>
									<h4 class="pad-hor text-thin">
										<span class="pull-right badge badge-success">Offline</span> Works
									</h4>

									<!--Works-->
									<div class="list-group bg-trans">
										<a href="#" class="list-group-item">
											<span class="badge badge-purple badge-icon badge-fw pull-left"></span> Joey K. Greyson
										</a>
										<a href="#" class="list-group-item">
											<span class="badge badge-info badge-icon badge-fw pull-left"></span> Andrea Branden
										</a>
										<a href="#" class="list-group-item">
											<span class="badge badge-pink badge-icon badge-fw pull-left"></span> Lucy Moon
										</a>
										<a href="#" class="list-group-item">
											<span class="badge badge-success badge-icon badge-fw pull-left"></span> Johny Juan
										</a>
										<a href="#" class="list-group-item">
											<span class="badge badge-danger badge-icon badge-fw pull-left"></span> Susan Sun
										</a>
									</div>

								</div>
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<!--End first tab (Contact list)-->


								<!--Second tab (Custom layout)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<div class="tab-pane fade" id="demo-asd-tab-2">

									<!--Monthly billing-->
									<div class="pad-all">
										<h4 class="text-lg mar-no">Monthly Billing</h4>
										<p class="text-sm">January 2015</p>
										<button class="btn btn-block btn-success mar-top">Pay Now</button>
									</div>


									<hr>

									<!--Information-->
									<div class="text-center clearfix pad-top">
										<div class="col-xs-6">
											<span class="h4">4,327</span>
											<p class="text-muted text-uppercase"><small>Sales</small></p>
										</div>
										<div class="col-xs-6">
											<span class="h4">$ 1,252</span>
											<p class="text-muted text-uppercase"><small>Earning</small></p>
										</div>
									</div>


									<hr>

									<!--Simple Menu-->
									<div class="list-group bg-trans">
										<a href="#" class="list-group-item"><span class="label label-danger pull-right">Featured</span>Edit Password</a>
										<a href="#" class="list-group-item">Email</a>
										<a href="#" class="list-group-item"><span class="label label-success pull-right">New</span>Chat</a>
										<a href="#" class="list-group-item">Reports</a>
										<a href="#" class="list-group-item">Transfer Funds</a>
									</div>


									<hr>

									<div class="text-center">Questions?
										<p class="text-lg text-semibold"> (415) 234-53454 </p>
										<small><em>We are here 24/7</em></small>
									</div>
								</div>
								<!--End second tab (Custom layout)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


								<!--Third tab (Settings)-->
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<div class="tab-pane fade" id="demo-asd-tab-3">
									<ul class="list-group bg-trans">
										<li class="list-header">
											<h4 class="text-thin">Account Settings</h4>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											<p>Show my personal status</p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											<p>Show offline contact</p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox">
											</div>
											<p>Invisible mode </p>
											<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
										</li>
									</ul>


									<hr>

									<ul class="list-group bg-trans">
										<li class="list-header"><h4 class="text-thin">Public Settings</h4></li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											Online status
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox" checked>
											</div>
											Show offline contact
										</li>
										<li class="list-group-item">
											<div class="pull-right">
												<input class="demo-switch" type="checkbox">
											</div>
											Show my device icon
										</li>
									</ul>



									<hr>

									<h4 class="pad-hor text-thin">Task Progress</h4>
									<div class="pad-all">
										<p>Upgrade Progress</p>
										<div class="progress progress-sm">
											<div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>
										</div>
										<small class="text-muted">15% Completed</small>
									</div>
									<div class="pad-hor">
										<p>Database</p>
										<div class="progress progress-sm">
											<div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>
										</div>
										<small class="text-muted">17/23 Database</small>
									</div>

								</div>
								<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
								<!--Third tab (Settings)-->

							</div>
						</div>
					</div>
				</div>
			</aside>
			<!--===================================================-->
			<!--END ASIDE-->

		</div>

		

        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">

            <!-- Visible when footer positions are fixed -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            
 <div id="mainnav-shortcut">
                        <ul class="list-unstyled">
                            <li class="col-xs-4" data-content="Additional Sidebar">
                                <p class="pad-lft">&#0169; 2015 Your SearchMaadi</p>
                            </li>
                            <li class="col-xs-4" data-content="Notification">
                               <a href="www.searchmaadi.com"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-sign-in fa-2x">Web Login</button></a>
                            </li>
                            <li class="col-xs-4" data-content="Page Alerts">
                                <a id="demo-page-alert" class="shortcut-grid" href="#">
                                    <i class="fa fa-bell"></i>
                                </a>
                            </li>
                        </ul>
                    </div>


            <!-- Visible when footer positions are static -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <div class="hide-fixed pull-right pad-rgt">
			
			</div>



            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
            <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
            <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->




        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->


        <!-- SCROLL TOP BUTTON -->
        <!--===================================================-->
        <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
        <!--===================================================-->



	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->

    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="<?php echo base_url();?>dash/js/jquery-2.1.1.min.js"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url();?>dash/js/bootstrap.min.js"></script>


    <!--Fast Click [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/fast-click/fastclick.min.js"></script>

    
    <!--Nifty Admin [ RECOMMENDED ]-->
    <script src="<?php echo base_url();?>dash/js/nifty.min.js"></script>


    <!--Switchery [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/switchery/switchery.min.js"></script>


    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-select/bootstrap-select.min.js"></script>

	 <!--Bootstrap Tags Input [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>


    <!--X-editable [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/x-editable/js/bootstrap-editable.min.js"></script>


    <!--Bootstrap Table [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-table/bootstrap-table.min.js"></script>


    <!--Bootstrap Table Extension [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>
	
	<!--Summernote [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/summernote/summernote.min.js"></script>


    <!--Demo script [ DEMONSTRATION ]-->
    <script src="<?php echo base_url();?>dash/js/demo/nifty-demo.min.js"></script>


    <!--Bootstrap Table Sample [ SAMPLE ]-->
    <script src="<?php echo base_url();?>dash/js/demo/tables-bs-table.js"></script>
	 <!--Form Wizard [ SAMPLE ]-->
    <script src="<?php echo base_url();?>dash/js/demo/form-wizard.js"></script>
	
	   <!--Bootstrap Wizard [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
 
 <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="<?php echo base_url();?>dash/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
	   
	 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>dash/css/bootstrap-multiselect.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>dash/js/bootstrap-multiselect.js"></script>	
    
	<script>
 $(document).ready(function() {
	
	 	 
	//to store customer data
$("#saveid").click(function() {
	
var name = $("#name").val();
var email = $("#email").val();
var phonenumber = $("#number").val();
var landlinenumber = $("#landline").val();
var business = $("#business").val();

if (phonenumber == '') {
alert("Insertion Failed Phone Number Is Blank....!!");
} else {
	
// Returns successful data submission message when the entered information is stored in database.
$.post("<?php echo base_url();?>Iro/savecustdata", {
	
   Name:name,
   Email:email,
   Number:phonenumber,
   LandlineNumber:landlinenumber,
   BusinessProfile:business
}, function(data) {
alert(data);
$('#form')[0].reset(); // To reset form fields
});
}
});
//end
//business data of customer
$("#businessdata").click(function() {
	

var custid = $("#cuid").val();
var companyname = $("#companyname").val();
var categoryname = $("#categoryname").val();
var decmakername = $("#decmakername").val();
var emailaddress = $("#emailaddress").val();
var mobilenumber = $("#mobilenumber").val();
var landlinenumber = $("#landlinenumber").val();
var address = $("#address").val();
var clientresponse = $("#clientresponse").val();
//var tmename = $("#tmename").val();
var differed = $("#differed").val();


if (mobilenumber == '') {
alert("Insertion Failed Phone Number Is Blank....!!");
} else {
	
// Returns successful data submission message when the entered information is stored in database.
$.post("<?php echo base_url();?>Iro/savebusinessdata", {
	
   
   custid:custid,
   companyname:companyname,
   categoryname:categoryname,
   decmakername:decmakername,
   emailaddress:emailaddress,
   mobilenumber:mobilenumber,
   landlinenumber:landlinenumber,
   address:address,
   clientresponse:clientresponse,
   //tmename:tmename,
   differed:differed
}, function(data) {
alert(data);
$('#form')[0].reset(); // To reset form fields
});
}
});

//end

//for call transfer to tmeinfo

       $('#clientresponse').change(function(){
		   if ($(this).val() == 'Yes') {
               $('#responseresult').css({'display':'block'});           
                         
           }if ($(this).val() == 'No') {
               $('#responseresult').css({'display':'none'});           
                         
           }
        });
  

//end


// to update breakin breakout
$(".breaks").click(function(){
	alert()
var pos = $(this).attr("id");
alert("id: " + pos);
 $.ajax({
                    type: "POST",
                    url: "<?php echo base_url();?>iro/updatebreak/",
                    data: '{Breaktype: "pos" }',
                    success: function (response) {
                        alert("Hello: " + response.Breaktype + " .\nCurrent Date and Time: " + response.DateTime);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    },
                    error: function (response) {
                        alert(response.responseText);
                    }
                });
            });

//show category


	 $('#ctg').change(function () {
        if (!this.checked) 
        //  ^
           $('#autoUpdate').hide();
        else 
            $('#autoUpdate').show();
    });

//end			

});
//alert("id: " + pos);
/*$.post("<?php echo base_url();?>iro/updatebreak/", {
Breaktype:pos}, function(data) {
alert(data);

});*/


//end


    </script>
  
<script type="text/javascript">
	 $(document).ready(function() {
        $("#check").click(function(){
			//alert('456');
            var favorite = [];
            $.each($("input[name='subcategory1']:checked"), function(){            
                favorite.push($(this).val());
            });
			//alert('<b>Email:  '+favorite.join+'</b>');
           // alert("My favourite subcategorys are: " + favorite.join(", "));
			//$('#txt30').html('<b>Email:  '+favorite.join+'</b>');
			$('#txt30').val(favorite.join(", "));
        });
    });
        $(function () {
            $("#btnAdd").bind("click", function () {
                var div = $("<div />");
                div.html(GetDynamicTextBox(''));
                $("#TextBoxContainer").append(div);
            });
            $("#btnGet").bind("click", function () {
                var values = "";
                $("input[name=DynamicTextBox]").each(function () {
                    values += $(this).val() + "\n";
                });
                alert(values);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("div").remove();
            });
			 function GetDynamicTextBox(value) {
            return '<input name = "number2[]" placeholder="Secondary Number" class="form-control" maxlength="10"  pattern="[0-9]{10}" type="text" value = "' + value + '" /><input type="button" value="Remove" class="btn btn-purple btn-labeled remove" />'
            
           
            
        }
        });
       
		 $(function () {
            $("#btnAdd1").bind("click", function () {
                var div = $("<div />");
                div.html(GetDynamicTextBox(''));
                $("#TextBoxContainer1").append(div);
            });
            $("#btnGet").bind("click", function () {
                var values = "";
                $("input[name=DynamicTextBox]").each(function () {
                    values += $(this).val() + "\n";
                });
                alert(values);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("div").remove();
            });
			function GetDynamicTextBox(value) {
            return '<input name = "landline2[]" placeholder="Secondary Landline" class="form-control"  maxlength="10"  pattern="[0-9]{10}" type="text" value = "' + value + '" /><input type="button" value="Remove" class="btn btn-purple btn-labeled remove" />'
            
           
            
        }
        });
        
		$(function () {
            $("#btnAdd2").bind("click", function () {
                var div = $("<div />");
                div.html(GetDynamicTextBox(''));
                $("#TextBoxContainer2").append(div);
            });
            $("#btnGet").bind("click", function () {
                var values = "";
                $("input[name=DynamicTextBox]").each(function () {
                    values += $(this).val() + "\n";
                });
                alert(values);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("div").remove();
            });
			function GetDynamicTextBox(value) {
            return '<input name = "email2[]" placeholder="Secondary Email" class="form-control" type="email" value = "' + value + '" /><input type="button" value="Remove" class="btn btn-purple btn-labeled remove" />'
      
        }
        });
        
    </script>
	 <script type="text/javascript">
        function ToggleReadOnlyState () {
			//alert('hii');
            var textarea = document.getElementById ("txt9");
            textarea.readOnly = !textarea.readOnly;
        }
    </script>
	<script type="text/javascript">

$(function() {

    $('#chkveg').multiselect({

        includeSelectAllOption: true
    });

    $('#btnget').click(function(){

        alert($('#chkveg').val());
    });
	
});

</script>
 
	
	<script type="text/javascript" >
	
        $(function() {
			$('#city').val($("#txt7 option:selected").text());
/*
            $(".delbutton").click(function() {
                var del_id = $(this).attr("id");
				alert('hii');
				
                var info = 'id=' + del_id;
                if (confirm("Sure you want to delete this post? This cannot be undone later.")) {
                    $.ajax({
                        type : "POST",
                        url : "delete_entry.php", //URL to the delete php script
                        data : info,
                        success : function() {
                        }
                    });
                    $(this).parents(".record").animate("fast").animate({
                        opacity : "hide"
                    }, "slow");
                }
                return false;
				
            });
			*/
			$('#txt1').keyup(function() {
        var txtVal = $(this).val();
       // $('#company').val(txtVal);
	   
	   $('#company').html('<b>Company Name:  '+txtVal+'</b>');
    });
	$('#txt2').keyup(function() {
        var txtVal = $(this).val();
		$('#tagline').html('<b>Company Tagline:  '+txtVal+'</b>');
      
    });
	$('#txt3').keyup(function() {
        var txtVal = $(this).val();
		$('#person').html('<b>Contact Person:  '+txtVal+'</b>');
       // $('#person').val(txtVal);
    });
	$('#txt4').keyup(function() {
        var txtVal = $(this).val();
		$('#email').html('<b>Email:  '+txtVal+'</b>');
        //$('#email').val(txtVal);
    });
	$('#txt5').keyup(function() {
        var txtVal = $(this).val();
		$('#number').html('<b>Contact Number:  '+txtVal+'</b>');
        //$('#number').val(txtVal);
    });
	$('#txt7').change(function(){
			 var value= $("#txt7 option:selected").text();
            //$('#city').val(value);
            //$('#cat').val(value);
			$('#city').html('<b>City :  '+value+'</b>');
        });
	$('#txt7').keyup(function() {
        var txtVal = $(this).val();
		$('#city').html('<b>City:  '+txtVal+'</b>');
        //$('#landline').val(txtVal);
    });
	$('#txt6').keyup(function() {
        var txtVal = $(this).val();
		$('#landline').html('<b>Landline Number:  '+txtVal+'</b>');
        //$('#landline').val(txtVal);
    });
	$('#txt10').keyup(function() {
        var txtVal = $(this).val();
		$('#address').html('<b>Address:  '+txtVal+'</b>');
       // $('#address').val(txtVal);
    });
	$('#txt11').keyup(function() {
        var txtVal = $(this).val();
		$('#web').html('<b>Website:  '+txtVal+'</b>');
        //$('#web').val(txtVal);
    });
	$('#txt25').keyup(function() {
        var txtVal = $(this).val();
		$('#fax').html('<b>Fax:  '+txtVal+'</b>');
        //$('#web').val(txtVal);
    });
	/*
$('#txt7').change(function(){
	var value= $("#txt7 option:selected").text();
            $('#city').val(value);
        });
*/
		 $('#txt8').change(function(){
			 var value= $("#txt8 option:selected").text();
            //$('#city').val(value);
            //$('#area').val(value);
			$('#area').html('<b>Area:  '+value+'</b>');
        });
		 $('#txt12').change(function(){
			 var value= $("#txt12 option:selected").text();
            //$('#city').val(value);
            //$('#cat').val(value);
			$('#cat').html('<b>Category Name:  '+value+'</b>');
        });
		 
		 $('.area select').change(function () {
			var area = $(this).val();
                    //alert(area);
				   
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_pin", 
                   	async: false,
                   	type: "POST", 
                   	data: "area="+area, 
                   	dataType: "html", 
                   	success: function(data) {
                   		
                       // alert(data);
					   $('#txt9').val(data);
                      // $('#pincode').val(data);
					   $('#pincode').html('<b>Pincode:  '+data+'</b>');
                   }
				  
               });
			 });  
			 <!--- start ME-->
			  $('.area select').change(function () {
			var area = $(this).val();
                    //alert(area);
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_me", 
                   	async: false,
                   	type: "POST", 
                   	data: "area="+area, 
                   	dataType: "html", 
                   	success: function(data) {
						$('#appointment .selectme').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                      // $('#pincode').val(data);
					   //$('#pincode').html('<b>Pincode:  '+data+'</b>');
                   }
				  
               });
			 });  
			 $('.me select').change(function () {
			var me = $(this).val();
			var medate = $("#medate").val();
                   //alert(medate);
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_metime", 
                   	async: false,
                   	type: "POST", 
                   	data: "me="+me+"&medate="+medate, 
                   	dataType: "html", 
                   	success: function(data) {
						$('.metime .selecttime').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                      // $('#pincode').val(data);
					   //$('#pincode').html('<b>Pincode:  '+data+'</b>');
                   }
				  
               });
			 });  
			 <!-- end ME-->
              /*     
			   $("#editpin").click(function(event){
				   alert('hii');
				   
   event.preventDefault();
   $('.txt9').prop("disabled", false); // Element(s) are now enabled.
 
});  
  */

$('.subcat select').change(function () {
			var cat = $(this).val();
                    //alert(cat);
				
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_sub", 
                   	async: false,
                   	type: "POST", 
                   	data: "cat="+cat, 
                   	dataType: "html", 
                   	success: function(data) {
                   		$('.getsubcat1').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                       //$('#pincode').val(data);
					  // $('#txt14').val(data)
                   }
				  
               }); 
			 
			   }) ;
			   
		/* start area and pincode*/	   
$('.city select').change(function () {
			var city = $(this).val();
                   // alert(city);
				
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_city", 
                   	async: false,
                   	type: "POST", 
                   	data: "city="+city, 
                   	dataType: "html", 
                   	success: function(data) {
                   		$('.area .selectpic').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                       //$('#pincode').val(data);
					  // $('#txt14').val(data)
                   }
				  
               }); 
			 
			   }) ;
			   $('.city select').change(function () {
			var city = $(this).val();
                   // alert(city);
				
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_std", 
                   	async: false,
                   	type: "POST", 
                   	data: "city="+city, 
                   	dataType: "html", 
                   	success: function(data) {
						$('#stdcode').val(data);
                   		//$('.stdcode .selectpic').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                       //$('#pincode').val(data);
					  // $('#txt14').val(data)
                   }
				  
               }); 
			 
			   }) ;
			   /* end area and pincode*/
/* start Package and price*/	   
$('.pad-no .class12').change(function () {
			var pack = $(this).val();
                  // alert(pack);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_package", 
                   	async: false,
                   	type: "POST", 
                   	data: "pack="+pack, 
                   	dataType: "html", 
                   	success: function(data) {
                   	$('.packdetails').html(data);
                       //alert(data);
					   //$('#txt9').val(data);
                       //$('#pincode').val(data);
					  // $('#txt14').val(data)
                   }
				  
               }); 
			 
			   }) ;
			   $('.package select').change(function () {
			var pack = $(this).val();
			 // var city=   $(".city option:selected").val();
           // var city=     $('.city select option:selected').val();
			//var city=  $(".txt7 option:selected").val();
			var city = $("#txt7 option:selected").val();
				
                   // console.log(selCountry);
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_packprice", 
                   	async: false,
                   	type: "POST", 
                   	data: "pack="+pack+"&city="+city, 
                   	dataType: "html", 
                   	success: function(data) {
						$('#txt9').val(data);
                   		
                   }
				  
               }); 
			 
			   }) ;
			  $('.zone select').change(function () {
			var city = $(this).val();
			
			
                   $.ajax({   
                   	url: "<?php  echo base_url();?>admin/ajax_call_area", 
                   	async: false,
                   	type: "POST", 
                   	data: "city="+city, 
                   	dataType: "html", 
                   	success: function(data) {
						//$('#txt9').val(data);
						//$('.area .selectpic').html(data);
                   		//$('#text').html(data);
						//alert(data);
                   }
				  
               }); 
			 
			   }) ;
			   /* end Package and price*/			   
			   $('#txt13').change(function(){
			 var value= $("#txt13 option:selected").text();
            //$('#city').val(value);
            $('#subcat').val(value);
        });
			    $('.time').datetimepicker({
        format: 'HH:mm:ss'
    });
		  $('.time1').datetimepicker({
        format: 'HH:mm:ss'
    });
	 $('.time2').datetimepicker({
        format: 'HH:mm:ss'
    });
	$('.time3').datetimepicker({
        format: 'HH:mm:ss'
    });
	
	
	$('#txt14').change(function(){
		alert('25');
    $('#t').val($(this).val());
})
	
	$('#txt15').keyup(function() {
        var txtVal = $(this).val();
        $('#morning_to').val(txtVal);
    });
	$('#txt16').keyup(function() {
        var txtVal = $(this).val();
        $('#evening_from').val(txtVal);
    });
	$('#txt17').keyup(function() {
        var txtVal = $(this).val();
        $('#evening_to').val(txtVal);
    });
	
	$("#multiple").change(function() {
		//alert('5');
        var multipleValues = $("#multiple").val() || "";
        var result = "";
        if (multipleValues != "") {
            var aVal = multipleValues.toString().split(",");
            var count = $("#multiple :selected").length;
            $.each(aVal, function(i, value) {
               
               // result += "<input type='text'  value='"  "'>";
result += $("#multiple").find("option[value=" + value + "]").text().trim() +",";
                //alert("Number of selected Names=" + count);
            });
        }
        //Set Result
        $("#sucatresult").html(result);

    });

	$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select Languages',
    search: true,
    selectAll: true
});

		 });
		 $(document).ready(function(){
    $('#disposition').on('change', function() {
		alert('5');
      if (this.value == '1' )
    
      {
		
        $("#callback").show();
      }
      else
      {
        $("#callback").hide();
      }
	   if (this.value == '12')
    
      {
		
        $("#appointment").show();
      }
      else
      {
        $("#appointment").hide();
      }
	  if (this.value == '7' )
    
      {
		
        $("#interest").show();
      }
      else
      {
        $("#interest").hide();
      }
	  if (this.value == '3' )
    
      {
		
        $("#neft").show();
      }
      else
      {
        $("#neft").hide();
      }
	  if (this.value == '4' )
    
      {
		
        $("#cash").show();
      }
      else
      {
        $("#cash").hide();
      }
    });
});
$(document).ready(function() {
	//to store customer data
$("#dispositionsave").click(function() {
var name = $("#txt1").val();
var tagline = $("#txt2").val();
var person = $("#txt3").val();
var designation = $("#txt21").val();
var address = $("#txt10").val();
var city = $("#txt7").val();
var area = $("#txt8").val();
var landmark = $("#txt31").val();//
var stdcode = $("#stdcode").val();//
var landline = $("#txt6").val();//
var website = $("#txt11").val();//
var fax = $("#txt25").val();//
var subctg = $("#txt30").val();//
var starttime = $("#txt14").val();//
var endtime = $("#txt15").val();//
var language = $("#txt20").val();//
var tollfree = $("#tollfree").val();//
var aboutcompany = $("#abtcompany").val();//
var email = $("#txt4").val();
var pincode = $("#txt9").val();
var number = $("#txt5").val();
var disposition = $("#disposition").val();
var date = $("#txt31").val();
var mename = $("#me").val();//
var medate = $("#medate").val();//
var metime = $("#metime").val();//
var time = $("#txt32").val();
var feedback = $("#txt33").val();
var mefeedback = $("#mfeedback").val();//
var category = $("#txt12").val();//
//alert(feedback);
//if any data is blank
if (name == '' ) {
	var name = 'NA';
}if (tagline == '' ) {
var tagline = 'NA';
}if (person == '' ) {
var person = 'NA';
}if (designation == '' ) {
var designation = 'NA';
}if (address == '' ) {
var address = 'NA';
}if (city == '' ) {
var city = 'NA';
}if (area == '' ) {
var area = 'NA';
}if (email == '' ) {
var email = 'NA';
}if (pincode == '' ) {
var pincode = 'NA';
}if (number == '' ) {
var number = 'NA';
}if (disposition == '' ) {
var disposition = 'NA';
}if (date == '' ) {
var date = 'NA';
}if (time == '' ) {
var time = 'NA';
}if (feedback == '' ) {
var feedback = 'NA';
}if (medate == '' ) {
var medate = 'NA';
}if (metime == '' ) {
var metime = 'NA';
}if (mefeedback == '' ) {
var mefeedback = 'NA';
}if (mename == '' ) {
var mename = 'NA';
}if (category == '' ) {
var category = 'NA';
}

if (landmark == '' ) {
var landmark = 'NA';
}if (stdcode == '' ) {
var stdcode = 'NA';
}if (landline == '' ) {
var landline = 'NA';
}if (website == '' ) {
var website = 'NA';
}if (fax == '' ) {
var fax = 'NA';
}if (subctg == '' ) {
var subctg = 'NA';
}if (starttime == '' ) {
var starttime = 'NA';
}if (endtime == '' ) {
var endtime = 'NA';
}if (language == '' ) {
var language = 'NA';
}if (tollfree == '' ) {
var tollfree = 'NA';
}if (aboutcompany == '' ) {
var aboutcompany = 'NA';
}
// Returns successful data submission message when the entered information is stored in database.
$.post("<?php echo base_url();?>admin/savedisposition/", {
   name:name,
   tagline:tagline,
   person:person,
   designation:designation,
   address:address,
   city:city,
   area:area, 
   email:email,
   pincode:pincode,
   number:number,
   disposition:disposition,
   date:date,
   time:time,
   medate:medate,
   metime:metime,
   mefeedback:mefeedback,
   mename:mename,
   aboutcompany:aboutcompany,
   tollfree:tollfree,
   language:language,
   endtime:endtime,
   starttime:starttime,
   subctg:subctg,
   fax:fax,
   website:website,
   landline:landline,
   stdcode:stdcode,
   landmark:landmark,
   feedback:feedback
}, function(data) {
alert(data);
$('#form')[0].reset(); // To reset form fields
});

});
$(':checkbox').on('click', function () {
    $(this).parent('td').next('td').find('table').slideToggle();
});
});

  $(function () {
  $('.class12').click(function () {
    var pack = $(this).attr('id');
	 
//$('#txt9'+pack).val(data);
		 if ($(this).is(":checked")) {
			  //alert(pack);
                
				 $('#show'+pack).hide();
				 $('#txt9'+pack).show();
            } else {
				 $('#show'+pack).show();
                $('#txt9'+pack).hide();
				 
            }	   
			   
    //alert(id);
  });
});

 $(document).ready(function(){
    $('.class12').on('change', function() {
		//alert(this.value);
      if (this.value == '1' || this.value == '2')
    
      {
        $("#showtop").show();
      }
      else
      {
        $("#showtop").hide();
      }
	 if (this.value == '3' )
    
      {
        $("#showkk").show();
      }
      else
      {
        $("#showkk").hide();
      }
	 
    });
});
          /*         
        https://stackoverflow.com/questions/220767/auto-refreshing-div-with-jquery-settimeout-or-another-method 

http://www.webslesson.info/2017/04/dynamic-autocomplete-search-using-bootstrap-typeahead-with-php-ajax.html
https://stackoverflow.com/questions/42972187/generate-pdf-output-and-send-it-to-email-using-phpmailer
https://stackoverflow.com/questions/25084926/how-to-display-categories-subcategories-sub-subcategories-in-select-list-php
language:-
https://stackoverflow.com/questions/10486833/implementing-google-translate-with-custom-flag-icons
http://www.freshcodehub.com/Article/26/implementing-google-translate-with-custom-flag-icons
*/		
 </script>
 	<script type="text/javascript">
        var checkList = document.getElementById('list1');
        checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (checkList.classList.contains('visible'))
                checkList.classList.remove('visible');
            else
                checkList.classList.add('visible');
        }
    </script>	
    
	
	
</body>
</html>