<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Add CityWise Category</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> <?php if(isset($citywisecatg)) {echo "Update CityWise Category"; } else { echo "Add CityWise Category"; } ?></h3>
						</div>
						<div class="panel-body">
						<form action="<?php echo base_url();?>Report/savecitywisecatg" method="post" enctype="multipart/form-data">
							<div class="row col-lg-12">
							<div class="col-lg-4">
							
						<?php if(isset($citywisecatg)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($citywisecatg)) { echo $citywisecatg->ID;}?>">
								<?php 
								}
									?>	
									<label class="control-label"><b>Select Category</b></label>
								<select class="selectpicker form-control" name="category[]" multiple="multiple" required >
								  <option value="">Select Category</option>
										<?php
										
												  foreach($categorylist as $catg) 
												  { ?>
													<option value="<?php echo $catg->ID; ?>" <?php //if(isset($citywisecatg)) { if($catg->ID== $citywisecatg->Category){ echo "selected";}}?>><?php echo $catg->Name; ?></option>
												  <?php } ?>
								</select>
							
						</div>
							<div class="col-lg-4">
							<label class="control-label"><b>City</b></label>
								<div class="form-group city">
											
											<select class="selectpicker"   name="city" required id="txt7" data-live-search="true">
												 <option value="">Select City </option>
                                   <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_location` where flag='0'")->result_array();
                
                        foreach ($query  as $lac1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $lac1['ID']; ?>" <?php //if(isset($contractinfo)) { if($contractinfo->City== $lac1['ID']) {echo "selected";} else{ }}else{ if($lac1['ID'] == $setcity) {echo "selected";} else{ }}?><?php ?>><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
							
						</div>
						
						
						</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Submit</button>
								<!--<a href="<?php echo base_url();?>report/citywisecatginfo" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>-->
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

	</div>
<?php include('footer.php');?>