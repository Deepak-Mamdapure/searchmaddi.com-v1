<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Contract List</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php // echo $mess;?> Contract List</h3>
						</div>
							<div class="panel-body">
							<?php if($this->session->userdata('Role') == '1'){ ?>
							<div class="row">
					<form action="<?php echo base_url();?>user/assigntoql" method="post">
														<div class="col-sm-3">
												<div class="form-group">
											<label class="control-label"><b>TME List:</b></label>
											<select class="selectpic"  name="tme[]" required  multiple="multiple" id="chkveg" >
											
												  <?php
           
            $query=$this->db->query("SELECT * FROM `tbl_admin` where Role=3")->result_array();
                
                        foreach ($query  as $tmename)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $tmename['Emp_ID']; ?>" ><?php echo $tmename['Name']; ?></option>
                                        
                                        <?php
						}
                                      
                                        ?>
                                   
												</select>
												</div>
												
												
											</div>
											<div class="col-sm-4">
												<div class="form-group">
											<label class="control-label"><b>Quality List:</b></label>
											<select class="selectpicker"  name="quality" required >
											<option value="">Select Quality</option>
												  <?php
           
            $query=$this->db->query("SELECT * FROM `tbl_admin` where Role=5")->result_array();
                
                        foreach ($query  as $tmename)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $tmename['Emp_ID']; ?>" ><?php echo $tmename['Name']; ?></option>
                                        
                                        <?php
						}
                                      
                                        ?>
                                   
												</select>
												</div>
												
												
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<button class="btn btn-info" type="submit" name="to_web">assigned </b></button>
												</div>
												
											</div>
											
											
										</form>	
										</div>
							<?php } ?>
							<form action="<?php echo base_url();?>admin/newbusiness" method="post">
									<div class="col-lg-12 row well">
									
									<div class="col-lg-9">
												<div class="col-lg-7">
												<input type="text" class="form-control" name="searchcontracts" placeholder="Search By Company Name,Phone,Email,ContractId" />
												</div>
												<div class="col-md-2">
												<input type="submit" value="Search" class="btn btn-sm btn-purple" />
											</div>
									</div>
									<label class="control-label ">Select Contract Type</label>
									<div class="col-lg-3">
											
											<select class="selectpicker"  name="contract" required onchange="this.form.submit()"  data-live-search="true">
											 <option value="1"  <?php if(isset($flagid)) { if($flagid==1){ echo "selected";}}?>>Call Back</option>
												  <option value="2"  <?php if(isset($flagid)) { if($flagid==2){ echo "selected";}}?>>Switch Off</option>
												   <option value="3"  <?php if(isset($flagid)) { if($flagid==3){ echo "selected";}}?>>Not Contactable</option>
												    <option value="4"  <?php if(isset($flagid)) { if($flagid==4){ echo "selected";}}?>>Do Not Disturb </option>
													 <option value="5"  <?php if(isset($flagid)) { if($flagid==5){ echo "selected";}}?>>Not Reachable </option>
													  <option value="6"  <?php if(isset($flagid)) { if($flagid==6){ echo "selected";}}?>>Follow UP </option>
													   <option value="7"  <?php if(isset($flagid)) { if($flagid==7){ echo "selected";}}?>>Not Interested</option>
													    <option value="8"  <?php if(isset($flagid)) { if($flagid==8){ echo "selected";}}?>>RNR </option>
														 <option value="9"  <?php if(isset($flagid)) { if($flagid==9){ echo "selected";}}?>>Not in Business</option>
														  <option value="10"  <?php if(isset($flagid)) { if($flagid==10){ echo "selected";}}?>>Wrong Number</option>
														   <option value="11"  <?php if(isset($flagid)) { if($flagid==11){ echo "selected";}}?>>Government sectors </option>
														    <option value="12"  <?php if(isset($flagid)) { if($flagid==12){ echo "selected";}}?>>Appointment </option>
												<!-- <option value="">Select Contract Type </option>
												 <option value="0" <?php if(isset($flagid)) { if($flagid==0){ echo "selected";}}?>>New Contract</option>
												 <option value="1" <?php if(isset($flagid)) { if($flagid==1){ echo "selected";}}else{ echo "selected";}?>>Website Contract</option>
												 <option value="2" <?php if(isset($flagid)) { if($flagid==2){ echo "selected";}}?>>Quality Contract</option>
												 <option value="3" <?php if(isset($flagid)) { if($flagid==3){ echo "selected";}}?>>Rejected Contract</option>
							-->
												</select>
												
												</div>
												
												
									</div>
								</form>
								
								<br /><br /><br /><br /><br /><br />
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">ContractID</th>
										<th data-field="agentname" data-sortable="true">Agent Name</th>
										<th data-field="name" data-sortable="true">CompanyName</th>
										<th data-field="track" data-sortable="true" >ContactDetails</th>
										<th data-field="status" data-sortable="true" >Category</th>
										<th data-field="name25" data-sortable="true">Address</th>
										<th data-field="stat" data-sortable="true" >Status</th>
										<th data-field="dat" data-sortable="true" >PaymentType</th>
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($contractlist  as $contract) { 
										?>	
                    <tr >
					<td><?php echo $contract->ContractID;?></td>
					<td><?php $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $contract->EmpID))->row_array(); 
echo $emp['Name'];?></td>
                        <!--<td><img src="<?php //echo base_url().$contract->Image;?>" width="100" alt="" /></td>-->
						<td><?php echo $contract->CompanyName."<br />" ."Tag Line: ". $contract->Tagline;?></td>
							<td><?php echo $contract->ContactPerson;?>
								<br/><?php echo $contract->Email;?><br/><?php echo $contract->Number;?><br/><?php echo $contract->Landline;?></td>
								<td><?php $category= $this->db->get_where('tbl_category' , array('ID' => $contract->Category))->row_array(); 
echo $category['Name'];?></td>
								<td><?php $City= $this->db->get_where('tbl_location' , array('ID' => $contract->City))->row_array(); 
echo $City['Name'];?>
								<br/>Std:<?php echo $contract->STDcode;?><br/>Pin:<?php echo $contract->Pincode;?><br/>Area:<?php echo $contract->AreaName;?><br/>Address:<?php echo $contract->Address;?></td>
								<td><?php 
								if($contract->flag == 0){
									echo "Process" ;
								}elseif($contract->flag == 1){
									echo "Active" ;
								}elseif($contract->flag == 2){
									echo "Review" ;
								}
								elseif($contract->flag == 3){
									echo "Reject";
									}
								?></td>
								<td><?php if($contract->PaymentType != '' && $contract->PaymentType != 'NA'){ echo implode (", <br />", json_decode($contract->PaymentType)) ;}?></td>
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						<a  href="<?php echo base_url();?>report/offers/<?php echo $contract->ContractID;?>" title="Add Offer" class="label label-table label-warning btn-warning">
                               Add Offer
                            </a>
							 <a  href="<?php echo base_url();?>report/events/<?php echo $contract->ContractID;?>" title="Add Events" class="label label-table label-warning btn-warning">
                               Add Event
                            </a>
							<a  href="<?php echo base_url();?>report/movies/<?php echo $contract->ContractID;?>" title="Add Offer" class="label label-table label-warning btn-warning">
                               Add Movies
                            </a>
						<a  href="<?php echo base_url();?>admin/editcontract/<?php echo $contract->ContractID;?>" title="Edit/View" class="label label-table label-success btn-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deletecontract/<?php echo $contract->ID;?>" title="Delete" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin/makesale/<?php echo $contract->ContractID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
				</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>