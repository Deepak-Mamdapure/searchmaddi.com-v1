<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Famous Personality</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Famous Personality</h3>
						</div>
						<div class="panel-body">
							<a href="<?php echo base_url();?>report/addfamouspersons"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Personalities</button></a>
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="name"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="img" data-sortable="true" >Image</th>
										<th data-field="name" data-sortable="true">Person Name</th>
										<th data-field="spec" data-sortable="true" >Specialization</th>
										<th data-field="dob" data-sortable="true" >DOB</th>
										<th data-field="dod" data-sortable="true" >DOD</th>
										<th data-field="con" data-sortable="true" >Contribution</th>
										<th data-field="place" data-sortable="true" >Place</th>
										<th data-field="status" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($famouspersons  as $famouspersons) { 
										?>	
                    <tr >
                        <td><img src="<?php echo base_url().$famouspersons->Image;?>" width="100" alt="" /></td>
						<td><?php echo $famouspersons->PersonName;?></td>
						<td><?php echo $famouspersons->Specialization;?></td>
						<td><?php echo $famouspersons->Dateofbirth;?></td>
						<td><?php echo $famouspersons->Dateofdeath;?></td>
						<td><?php echo $famouspersons->Contribution;?></td>
						<td><?php echo $famouspersons->Place;?></td>
                        
                        <td class="form-group"> <a  href="<?php echo base_url();?>report/updatepersondata/<?php echo $famouspersons->ID;?>" class="label label-table label-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deletefamouspersons/<?php echo $famouspersons->ID;?>" class="label label-table label-success btn-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>