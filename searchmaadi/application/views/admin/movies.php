<?php include('header.php');?>
<script type="text/javascript">
  function showImage(src, target1) {
            var fr = new FileReader();

             fr.onload = function(){
      target1.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putsmallImage() {
            var src = document.getElementById("select_smallimage");
            var target = document.getElementById("target1");
            showImage(src, target1);
        }
		
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Movies</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php if(isset($movieinfo)) {echo "Update Movies Details"; } else { echo "Add Movies Details"; } ?></h3>
						</div>
						<div class="panel-body">
						<a href="<?php echo base_url();?>report/movieslists/<?php if(isset($contractinfo)) { echo $contractinfo->ContractID;}?>" class="btn btn-purple btn-labeled fa fa-eye ">View Movie List</a>
								<hr />
							<form action="<?php echo base_url();?>report/savemovies" method="post" enctype="multipart/form-data">
								<?php if(isset($movieinfo)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($movieinfo)) { echo $movieinfo->ID;}?>">
								<input type="hidden" value="<?php if(isset($movieinfo)) {echo $movieinfo->Image;}?>" name="imageupdate">
								<?php 
								}
									?>		
								<?php if(isset($contractinfo)) {
									?>
								<input type="hidden" name="contractid" value="<?php if(isset($contractinfo)) { echo $contractinfo->ContractID;}?>">
								<?php 
								}
									?>										
						<div class="row col-lg-12">
						<div class="col-lg-4">
						<label>ContractId</label>
							<input type="text" readonly name="contractId" class="form-control" placeholder="ContractId" value="<?php if(isset($movieinfo)) { echo $movieinfo->ContractId;}elseif(isset($contractinfo)) { echo $contractinfo->ContractID;} elseif(empty($movieinfo) && empty($contractinfo)){ echo 'KAR'.date('dmY').rand(0,100000) ;} ?>" required >
							
						</div>
						
						
							<div class="col-lg-4">
						<label>Company Name</label>
								<input type="text"  name="companyname" class="form-control" placeholder="Company Name" value="<?php if(isset($contractinfo)) {echo $contractinfo->CompanyName;} ;?>" required <?php if(isset($contractinfo)) { echo 'readonly';} ;?> >
						
						</div>
						</div>
						<br>
						<br>
						<br>
						<br>
						<div class="row" id="TextBoxContainerMovie">
						<div class="col-lg-12">
						<label class="control-label"><h3>Movies</h3><!--<input id="btnAddMovie" type="button" value="Add" />--></label>
						<br />
						<br />
						<div class="col-lg-3">
						<label>Movie Name</label>
							<input type="text" name="moviename" id="movies" class="form-control" placeholder="Event Title"  value="<?php if(isset($movieinfo)) { echo $movieinfo->MovieName;};?>" required >
							
						
					</div>
					
						
						<div class="col-lg-3">
						
								<label>Movie Language</label>
								<select class="selectpicker form-control" name="language[]" id="movies" multiple="multiple" required >
												 <option value="">Select Language </option>
												 <?php 
												if(isset($movieinfo)) {
												$lan = $movieinfo->Language;
												$lang= explode(',',$lan);
												}
												 ?>
												<option value="Hindi" <?php if(isset($movieinfo)) { if(in_array("Hindi",$lang)){ echo "selected";}}?>>Hindi</option>
												 <option value="English" <?php if(isset($movieinfo)) { if(in_array("English",$lang)){ echo "selected";}}?>>English</option>
												 <option value="Tamil" <?php if(isset($movieinfo)) { if(in_array("Tamil",$lang)){ echo "selected";}}?>>Tamil</option>
												 <option value="Telugu" <?php if(isset($movieinfo)) { if(in_array("Telugu",$lang)){ echo "selected";}}?>>Telugu</option>
												 <option value="Kannada" <?php if(isset($movieinfo)) { if(in_array("Kannada",$lang)){ echo "selected";}}?>>Kannada</option>
												
												</select>
					</div>
					
					<div class="col-lg-3">
						<label>Movie Time</label>
								<input type="text" name="movietime" class="form-control" id="movies" value="<?php if(isset($movieinfo)) { echo $movieinfo->ShowTime;};?>" placeholder="Add Movie Times" data-role="tagsinput">
												
					</div>
					<div class="col-lg-3">
										<label class="col-md-4 control-label">Add Movie Image (max 500 kb)</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
												<input <?php if(!isset($movieinfo)) {echo "required";}?> type="file" id="select_smallimage" name="image" onchange="putsmallImage()" />
												<?php if(!isset($movieinfo)) { ?> <img id="target1" height="100" width="100"/> <?php  } else{ ?>
												 <img id="target1" height="100" width="100" src="<?php echo base_url().$movieinfo->Image;?>" /> <?php } ?>
											</span>
										</div>
									
						</div>
					


					</div>
					
					</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Save</button>
							<a href="<?php echo base_url();?>report/contractlist" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>