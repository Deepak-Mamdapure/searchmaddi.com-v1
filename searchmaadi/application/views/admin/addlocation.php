<?php include('header.php');?>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<script type="text/javascript">
  function showImage(src, target1) {
            var fr = new FileReader();

             fr.onload = function(){
      target1.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putsmallImage() {
            var src = document.getElementById("select_smallimage");
            var target = document.getElementById("target1");
            showImage(src, target1);
        }
		 function showImage(src, target2) {
            var fr = new FileReader();

             fr.onload = function(){
      target2.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
		function putbigImage() {
            var src = document.getElementById("select_bigimage");
            var target = document.getElementById("target2");
            showImage(src, target2);
        }
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Location Info</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($location)) {echo "Update Location"; } else{ echo "Add Location" ; } ?></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>admin/storeLocation" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($location)) {echo $location->ID;}?>" name="id">
									<input type="hidden" value="<?php if(isset($location)) {echo $location->SmallImage;}?>" name="smallimageupdate">
									<input type="hidden" value="<?php if(isset($location)) {echo $location->BigImage;}?>" name="bigimageupdate">
									<div class="form-group">
											<label class="col-md-4 control-label">Location Small Image (186X186 px(max 100 kb))</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
												<input <?php if(!isset($location)) {echo "required";}?> type="file" id="select_smallimage" name="smallimage" onchange="putsmallImage()" />
												<?php if(!isset($location)) { ?> <img id="target1" height="100" width="100" /> <?php }else{ ?>
												<img id="target1" height="100" width="100"  src="<?php echo base_url().$location->SmallImage;?> " />  <?php } ?>
											</span>
										</div>
									
									</div>
									<div class="form-group">
											<label class="col-md-4 control-label">Location Large Image (1045X423 px(max 500 kb))</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
											<input <?php if(!isset($location)) {echo "required";}?> type="file" id="select_bigimage" name="bigimage" onchange="putbigImage()" />
											<?php if(!isset($location)) { ?> <img id="target2" height="100" width="100" /><?php }else{ ?>
											 <img id="target2" height="100" width="100" src="<?php echo base_url().$location->BigImage; ?>"/><?php } ?>
											</span>
										</div>
									</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Location Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Location Name" name="name"value="<?php if(isset($location)) {echo $location->Name;}?>" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Kannada Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Kannada Name" name="kannada" value="<?php if(isset($location)) {echo $location->Kannada;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">STD Code</label>
											<div class="col-sm-9">
												<input type="text" placeholder="STD Code" name="stdcode" value="<?php if(isset($location)) {echo $location->STDCode;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
									<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputdesc">Description:</label><br/><br/>
												<textarea name="description" rows="3" cols="60" placeholder="Description"><?php if(isset($location)) {echo $location->Description;}?></textarea>
												
											
										</div>
									
									</div>
									<div class="panel-footer text-right">
									<a href="<?php echo base_url();?>admin/locationinfo" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
									
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($area)) {echo "Update Sub-Location"; } else{ echo "Add Sub-Location" ; } ?></h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<form action="<?php echo base_url();?>admin/storearea" method="post">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($area)) { echo $area->ID;}?>" name="id">
									<?php //print_r($area);exit;?>
										<div class="row">
											<div class="col-sm-6">
											<div class="form-group">
											<label class="control-label">Location Name</label>
											<select class="selectpicker"  name="location" required>
												 <option value="">Select Location </option>
                                    <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_location` where flag=0")->result_array();
                
                        foreach ($query  as $loc)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $loc['ID']; ?>" <?php if(isset($area)) {if($loc['ID'] == $area->Parent){ echo "selected";}}?>><?php echo $loc['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">Area Name</label>
													<input type="text" name="name" value= "<?php if(isset($area)) { echo $area->Name;}?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">Kannada Name</label>
													<input type="text" name="kannada" value= "<?php if(isset($area)) { echo $area->Kannada;}?>" required class="form-control">
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">ZipCode</label>
													<input type="text" name="zip" value= "<?php if(isset($area)) { echo $area->Zip;}?>" required class="form-control">
												</div>
											</div>
											
										</div>
									</div>
									<div class="panel-footer text-right">
									<a href="<?php echo base_url();?>admin/locationinfo" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
										
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
