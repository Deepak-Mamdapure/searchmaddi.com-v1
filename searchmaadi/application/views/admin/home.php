<?php include('header.php');?>

		<div class="boxed">
<?php 
																
									if($this->session->userdata('Role')==1 || $this->session->userdata('Role')==9)
									{
										
									?>
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-center"></h1>

					<!--Searchbox-->
					<!-- 
<div class="searchbox">
						<div class="input-group custom-search-form">
							<input type="text" class="form-control" placeholder="Search..">
							<span class="input-group-btn">
								<button class="text-muted" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</div>
 -->
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->


				<!--Breadcrumb-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<!-- 
	<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
 -->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End breadcrumb-->


		

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--Tiles - Bright Version-->
					<!--===================================================-->
					<div class="row">
					<?php 
																
									if($this->session->userdata('Role')==1 )
									{
										
									?>
					    <div class="col-sm-6 col-lg-4">
					
					        <!--Registered User-->
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					        <div class="panel media pad-all">
					            <div class="media-left">
					                <span class="icon-wrap icon-wrap-sm icon-circle bg-success">
					                <i class="fa fa-user fa-2x"></i>
					                </span>
					            </div>
						<div class="media-body">
					                <p class="text-2x mar-no text-mint">250</p>
					                <p class="text-muted mar-no">Total Customers</p>
					            </div>
					             <div class="media-body">
					                <p class="text-2x mar-no text-mint">25 </p>
					                <p class="text-muted mar-no">Today Customers</p>
					            </div>
					             <!--<div class="media-body">
					                <p class="text-2x mar-no text-mint">5</p>
					                <p class="text-muted mar-no">Search by</p>
					            </div>-->
					            
					        </div>
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
					    </div>
					    <div class="col-sm-6 col-lg-4">
					
					        <!--New Order-->
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					        <div class="panel media pad-all">
					            <div class="media-left">
					                <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
					                <i class="fa fa-industry fa-2x"></i>
					                </span>
					            </div>
					<div class="media-body">
					                <p class="text-2x mar-no text-mint">250</p>
					                <p class="text-muted mar-no">Total Company</p>
					            </div>
					             <div class="media-body">
					                <p class="text-2x mar-no text-mint">25</p>
					                <p class="text-muted mar-no">Today Company</p>
					            </div>
					            
					        </div>
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
					    </div>
					    <div class="col-sm-6 col-lg-4">
					
					        <!--Comments-->
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					        <div class="panel media pad-all">
					            <div class="media-left">
					                <span class="icon-wrap icon-wrap-sm icon-circle bg-warning">
					                <i class="fa fa-tags fa-2x"></i>
					                </span>
					            </div>
					
					            
								<div class="media-body">
					                <p class="text-2x mar-no text-mint">250</p>
					                <p class="text-muted mar-no">Total Category</p>
					            </div>
					             <div class="media-body">
					                <p class="text-2x mar-no text-mint">25</p>
					                <p class="text-muted mar-no">Today Category</p>
					            </div>
					             <!--<div class="media-body">
					                <p class="text-2x mar-no text-mint">5</p>
					                <p class="text-muted mar-no">Search by</p>
					            </div>-->
					        </div>
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
					    </div>
						<?php
									}
									?>
					    <div class="col-sm-6 col-lg-4">
					
					        <!--Sales-->
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					        <div class="panel media pad-all">
					            <div class="media-left">
					                <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
					                <i class="fa fa-tripadvisor fa-2x"></i>
					                </span>
					            </div>
					<div class="media-body">
					                <p class="text-2x mar-no text-mint"><?php if(isset($totalvisitor)) {echo $totalvisitor->totalvisitor;}?></p>
					                <p class="text-muted mar-no">Total Visitors</p>
					            </div>
					             <div class="media-body">
					                <p class="text-2x mar-no text-mint"><?php if(isset($totalvisitor)) {echo $totalvisitor->today;}?></p>
					                <p class="text-muted mar-no">Today Visitors</p>
					            </div>
					           
					        </div>
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
					    </div>
						<?php
						if($this->session->userdata('Role')==9 )
									{
										
						?>
						<div class="col-sm-6 col-lg-4">
					
					        <!--Sales-->
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					        <div class="panel media pad-all">
					            <div class="media-left">
					                <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
					                <i class="fa fa-thumbs-up fa-2x"></i>
					                </span>
					            </div>
					<div class="media-body">
					                <p class="text-2x mar-no text-mint"><?php if(isset($totalvisitor)) {echo $totalvisitor->totalvisitor;}?></p>
					                <p class="text-muted mar-no">Total Likes</p>
					            </div>
					             <div class="media-body">
					                <p class="text-2x mar-no text-mint"><?php if(isset($totalvisitor)) {echo $totalvisitor->today;}?></p>
					                <p class="text-muted mar-no">Today Likes</p>
					            </div>
					           
					        </div>
					        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
					
					    </div>
						<?php
									}
						?>
						
					</div>
					<!--===================================================-->
					<!--End Tiles - Bright Version-->
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Visitor Details</h3>
						</div>
						<div class="panel-body">
							<!--<a href="<?php echo base_url();?>admin/addcategory"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Category and Sub-Category</button></a>-->
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="10"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true" >Sl.No</th>
										<th data-field="name" data-sortable="true">City</th>
										<th data-field="date" data-sortable="true" >Region</th>
										<th data-field="track" data-sortable="true" >Country</th>
										<th data-field="amount" data-sortable="true" >Count</th>
										<th data-field="status" data-sortable="true" >Created</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
				   
											$i=1;
										foreach ($visitor  as $visitorinfo) { 
										
										?>	
                    <tr >
                        <td><?php  echo $i;?></td>
						<td><?php  echo $visitorinfo->City ;?></td>
						<td><?php echo $visitorinfo->Region ;?></td>
						<td><?php echo $visitorinfo->Country ;?></td>
						<td><?php echo $visitorinfo->num ;?></td>
						<td><?php echo $visitorinfo->Created ;?></td>
                        		
                 
				
                    </tr>
					  <?php
					 
											   $i++;
										}
										
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
				
					
					<div class="row">
					   
					    
					   
					</div>
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
<?php
									}
									else
									{
										
									?>
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content" >
										<!--Tiles - Bright Version-->
					<!--===================================================-->
					
					<!--===================================================-->
					<!--End Tiles - Bright Version-->
					<!--Custom Toolbar-->
					<!--===================================================-->
				
       <div class="row">
          <div class="col-md-12">
            <div class="text-xs-center text-lg-center">
           <img src="<?php echo base_url();?>dash/img/bg-img/bg-img-2.png" class="img-thumbnail">
           </div>
          </div>
       </div>
 

				

					
					<div class="row" style="url('<?php echo base_url();?>dash/img/bg-img/bg-img-2.png');height: 100%; background-position: center;background-repeat: no-repeat;background-size: cover;">
					   
					    
					   
					</div>
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->
<?php
									}
?>

<?php include('footer.php');?>
			
