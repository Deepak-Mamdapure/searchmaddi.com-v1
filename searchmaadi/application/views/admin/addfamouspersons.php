<?php include('header.php');?>

<script type="text/javascript">
  function showImage(src, target) {
            var fr = new FileReader();

             fr.onload = function(){
      target.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putImage() {
            var src = document.getElementById("select_image");
            var target = document.getElementById("target");
            showImage(src, target);
        }
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Add Famous Personalities</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($famouspersons)) { echo "Update Famous Persons" ; } else{ echo "Add Famous Persons" ; } ?></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>report/savefamouspersons" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($famouspersons)) {echo $famouspersons->ID;}?>" name="id">
									<input type="hidden" value="<?php if(isset($famouspersons)) {echo $famouspersons->Image;}?>" name="imageupdate">
									<div class="col-sm-6">
									<div class="form-group">
										<label class="col-md-3 control-label">Person Image</label>
										<div class="col-md-6">
											<span class="pull-left btn btn-default btn-file">
											Browse... 
<input  type="file" id="select_image" name="image" onchange="putImage()" />
<?php if(!isset($famouspersons)) { ?><img id="target" height="100" width="100" /><?php  } else{ ?>
<img id="target" height="100" width="100"  src="<?php echo base_url().$famouspersons->Image;?>" /><?php } ?>
											</span>
										</div>
									</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Person Name</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Person Name" value="<?php if(isset($famouspersons)) {echo $famouspersons->PersonName;}?>" name="name" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Contribution
											</label>
											<div class="col-sm-6">
												<textarea type="text" placeholder="Contribution" name="contribution" required id="demo-hor-inputemail" class="form-control"><?php if(isset($famouspersons)) {echo $famouspersons->Contribution;}?></textarea>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Specialization</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Specialization" name="specialization" value="<?php if(isset($famouspersons)) {echo $famouspersons->Specialization;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Date of birth</label>
											<div class="col-sm-6">
												<input type="date" placeholder="Date of birth" value="<?php if(isset($famouspersons)) {echo $famouspersons->Dateofbirth;}?>" name="dob" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Date of death</label>
											<div class="col-sm-6">
												<input type="date" placeholder="Date of death" value="<?php if(isset($famouspersons)) {echo $famouspersons->Dateofdeath;}?>" name="dod" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div><div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Place</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Place" value="<?php if(isset($famouspersons)) {echo $famouspersons->Place;}?>" name="place" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										
									
									</div>
									</div>
									<div class="panel-footer text-center">
										<button class="btn btn-info" type="submit">Save</button>
										<a href="<?php echo base_url();?>report/famouspersons" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
									
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
