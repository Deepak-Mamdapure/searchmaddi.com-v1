<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Iro Business</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php // echo $mess;?> Business Details</h3>
						</div>
							<div class="panel-body">
							
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">ContractID</th>
										
										<th data-field="agentname" data-sortable="true">CompanyName</th>
										<th data-field="id" data-sortable="true">DecisionMaker</th>
										<th data-field="name" data-sortable="true">	CategoryName</th>
										<th data-field="track" data-sortable="true" >ContactDetails</th>
										
										<th data-field="name25" data-sortable="true">Address</th>
										<th data-field="stat" data-sortable="true" >ClientResponse</th>
										
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($contractlist  as $contract) { 
										?>	
                    <tr>
					<!--<td><?php echo $contract->ContractID;?></td>-->
					<td><?php $emp= $this->db->get_where('tbl_user' , array('ID' => $contract->CustomerID))->row_array(); 
echo $emp['Name'];?></td>
                        <!--<td><img src="<?php //echo base_url().$contract->Image;?>" width="100" alt="" /></td>-->
						<td><?php echo $contract->CompanyName;?></td>
							<td><?php echo $contract->DecisionMaker;?></td>
							<td><?php $category= $this->db->get_where('tbl_category' , array('ID' => $contract->CategoryName))->row_array(); 
echo $category['Name'];?></td>
								<td><br/><?php echo $contract->Email;?><br/><?php echo $contract->Mobile;?><br/><?php echo $contract->Landline;?></td>
								
							<td><br/><?php echo $contract->Address;?><br/></td>
							<td><br/><?php echo $contract->ClientResponse;?><br/></td>
								
             
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin/<?php echo $contract->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
						
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
				</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>