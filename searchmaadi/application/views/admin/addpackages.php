<?php include('header.php');?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Adding Package Info</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-8">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Add Package</h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>report/savepackages" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($packages)) {echo $packages->ID;}?>" name="id">
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Package Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Package Name" value="<?php if(isset($packages)) {echo $packages->Name;}?>" name="name" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Bangalore Price</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Bangalore Price" name="bangaloreprice" value="<?php if(isset($packages)) {echo $packages->Price1;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputpass">Smart City Price</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Smart City Price" name="smartcity" value="<?php if(isset($packages)) {echo $packages->Price2;}?>"  required id="demo-hor-inputpass" class="form-control">
											</div>
										</div>
												<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputdesc">Description:</label><br/><br/>
												<textarea name="description" rows="20" cols="60" placeholder="Description"><?php if(isset($packages)) {echo $packages->Description;}?></textarea>
												
											
										</div>
									
									</div>
									<div class="panel-footer text-right">
									<a href="<?php echo base_url();?>report/packagelist" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
