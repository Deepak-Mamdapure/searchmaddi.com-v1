<?php include('header.php');?>
<script type="text/javascript">
  function showImage(src, target1) {
            var fr = new FileReader();

             fr.onload = function(){
      target1.src = fr.result;
    }
           fr.readAsDataURL(src.files[0]);

        }
        function putsmallImage() {
            var src = document.getElementById("select_smallimage");
            var target = document.getElementById("target1");
            showImage(src, target1);
        }
		
</script>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Events</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"><?php if(isset($eventinfo)) {echo "Update Event Details"; } else { echo "Add Event Details"; } ?></h3>
						</div>
						<div class="panel-body">
						<a href="<?php echo base_url();?>report/eventlists" class="btn btn-purple btn-labeled fa fa-eye ">View Event List</a>
								<hr />
							<form action="<?php echo base_url();?>report/saveevents" method="post" enctype="multipart/form-data">
								<?php if(isset($eventinfo)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($eventinfo)) { echo $eventinfo->EventId;}?>">
								<?php 
								}
									?>		
								<?php if(isset($contractinfo)) {
									?>
								<input type="hidden" name="contractid" value="<?php if(isset($contractinfo)) { echo $contractinfo->ContractID;}?>">
								<?php 
								}
									?>										
						<div class="row col-lg-12">
							<div class="col-lg-4">
							<label>ContractId</label>
							<input type="text" readonly name="contractId" class="form-control" placeholder="ContractId" value="<?php if(isset($eventinfo)) { echo $eventinfo->ContractId;}if(isset($contractinfo)) { echo $contractinfo->ContractID;} elseif(empty($eventinfo) && empty($contractinfo)){ echo 'KAR'.date('dmY').rand(0,100000) ;} ?>" required >
							
						<br />
						<label>Company Name</label>
								<input type="text"  name="companyname" class="form-control" placeholder="Company Name" value="<?php if(isset($eventinfo)) { echo $eventinfo->CompanyName;}if(isset($contractinfo)) { echo $contractinfo->CompanyName;} ;?>" required <?php if(isset($eventinfo)) { echo 'readonly';}if(isset($contractinfo)) { echo 'readonly';} ;?> >
							
						<br />
						<label>Contact Person</label>
								<input type="text" name="contactperson" class="form-control" placeholder="Contact Person" value="<?php if(isset($eventinfo)) { echo $eventinfo->ContactPerson;}if(isset($contractinfo)) { echo $contractinfo->ContactPerson;} ;?>" required >
							<br />
							<label>Phone Number</label>
								<input type="text" name="phonenumber" class="form-control" placeholder="Phone Number" pattern="/\[0-9]/g" value="<?php if(isset($eventinfo)) { echo $eventinfo->PhoneNumber;}if(isset($contractinfo)) { echo $contractinfo->Number;} ;?>"  maxlength="10" required >
							
						<br />
						<label>Email</label>
								<input type="email" name="email" class="form-control" placeholder="Email Id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php if(isset($eventinfo)) { echo $eventinfo->Email;}if(isset($contractinfo)) { echo $contractinfo->Email;} ;?>" required >
							<br />
							<label>Website</label>
							<input type="text" name="website" class="form-control" placeholder="Website"  value="<?php if(isset($eventinfo)) { echo $eventinfo->Website;}if(isset($contractinfo)) { echo $contractinfo->Web;} ;?>" required >
							
						</div>
						<div class="col-lg-4">
						<label>Event Name/Title</label>
							<input type="text" name="title" class="form-control" placeholder="Event Title"  value="<?php if(isset($eventinfo)) { echo $eventinfo->Title;};?>" required >
							<br />
							<label>Event Description</label>
							<textarea name="description" rows="4" cols="50"  placeholder="Event Description"><?php if(isset($eventinfo)) { echo $eventinfo->Description;};?></textarea>
						<br />
						<label>Event Address</label>
						<textarea name="address" rows="4" cols="50"  placeholder="Event Address" required ><?php if(isset($eventinfo)) { echo $eventinfo->Address;};?></textarea>
						<br /><br />
						<label>Event Fees</label>
						<input type="text" name="eventfees" class="form-control" placeholder="Event Fees"  value="<?php if(isset($eventinfo)) { echo $eventinfo->EventFees;};?>" required >
					<label>Event Type</label><br />
						<select class="selectpicker"   name="eventtype" required id="txt7">
												 <option value="">Select Event Type </option>
                                   <?php
            
						$query=$this->db->query("SELECT * FROM `tbl_eventtypes` where `Status` = '0'")->result_array();
                
                        foreach ($query  as $events)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $events['Name']; ?>" <?php if(isset($eventinfo)) { if($eventinfo->EventType== $events['Name']) {echo "selected";} else{ }}?><?php ?>><?php echo $events['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
						
					</div>
						
						<div class="col-lg-2">
								<label>Event Start Date</label>
								<input type="date" name="startdate" class="form-control"  value="<?php if(isset($eventinfo)) { echo $eventinfo->StartDate;} ;?>" required >
						<br />
							<label>Event End Date</label>
						<input type="date" name="enddate" class="form-control" value="<?php if(isset($eventinfo)) { echo $eventinfo->EndDate;} ;?>" required >
						</div><div class="col-lg-2">
						<label>Event Start Time</label>
								<input type="time" name="starttime" class="form-control" placeholder="Event Start Time"  value="<?php if(isset($eventinfo)) { echo $eventinfo->StartTime;} ;?>" required >
					<br />
					<label>Event End Time</label>
								<input type="time" name="endtime" class="form-control" placeholder="Event End Time"  value="<?php if(isset($eventinfo)) { echo $eventinfo->EndTime;} ;?>" required >
						
						</div>
						<div class="form-group">
											<label class="col-md-4 control-label">Add Event Image (max 500 kb)</label>
									
										<div class="col-md-4">
											<span class="pull-left btn btn-default btn-file">
											Browse...
												<input <?php if(!isset($eventinfo)) {echo "required";}?> type="file" id="select_smallimage" name="smallimage" onchange="putsmallImage()" />
												<?php if(!isset($eventinfo)) { ?> <img id="target1" height="100" width="100"/> <?php  } else{ ?>
												 <img id="target1" height="100" width="100" src="<?php echo base_url().$eventinfo->EventImage;?>" /> <?php } ?>
											</span>
										</div>
									
									</div>
						</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Save</button>
							<a href="<?php echo base_url();?>report/contractlist" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					</div>
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>