<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Movie List</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Movie List</h3>
						</div>
						<div class="panel-body">
						<a href="<?php echo base_url();?>report/movies/<?php echo $contractid; ?>" class="btn btn-purple btn-labeled fa fa-plus pull-left">Add More Movies</a>
						<!--<a href="<?php echo base_url();?>report/movieslistss" class="btn btn-purple btn-labeled fa fa-plus">Add movieslistss Without Contract</a>
						-->
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="companyname"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										<th data-field="img" data-sortable="true">Movie Image</th>
										<th data-field="companyname" data-sortable="true">CompanyName</th>
										
										<th data-field="movieslistsname" data-sortable="true">Movie Name</th>
										<th data-field="desc" data-sortable="true" >Language</th>
										<th data-field="address" data-sortable="true" >ShowTime</th>
									
										<th data-field="setting" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($movieslist  as $movieslists) { 
										?>	
                    <tr >
					<td><img src="<?php echo base_url().$movieslists->Image;?>" width="100" alt="" /></td>
					<td><?php $con= $this->db->get_where('tbl_contract' , array('ContractID' => $movieslists->ContractId))->row_array();echo $con['CompanyName'];?></td>
						
                        		<td><?php echo $movieslists->MovieName;?></td>
                        		<td><?php echo $movieslists->Language;?></td>
                        		<td><?php echo $movieslists->ShowTime;?></td>
                        		
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						
						<a  href="<?php echo base_url();?>report/updatemovies/<?php echo $movieslists->ContractId;?>/<?php echo $movieslists->ID;?>" class="label label-table label-success btn-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deletemovies/<?php echo $movieslists->ContractId;?>/<?php echo $movieslists->ID;?>" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin//<?php echo $movieslists->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>