<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow"> Add Event Type</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php if(isset($eventtype)) {echo "Update Event Type";}else{echo "Add Event Type" ; }?></h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>report/saveeventtype" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($eventtype)) {echo $eventtype->ID;}?>" name="id">
										<div class="form-group">
											<label class="col-sm-3 control-label" for="demo-hor-inputemail">Event Type Name</label>
											<div class="col-sm-9">
												<input type="text" placeholder="Event Type Name" value="<?php if(isset($eventtype)) {echo $eventtype->Name;}?>" name="name" required id="demo-hor-inputemail" class="form-control">
											</div>
										</div>
										
											
									
									</div>
									<div class="panel-footer text-right">
										<button class="btn btn-info" type="submit">Save</button>
										<a  href="<?php echo base_url();?>report/addeventtype/" class="btn btn-default" >Cancel</a>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
					
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Event Type List</h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Name</th>
										<th data-field="agentname" data-sortable="true">Status</th>
										
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($eventlists  as $eventlist) { 
										?>	
                    <tr >
					<td><?php echo $eventlist->Name;?></td>
					<td><?php echo $eventlist->Status;?></td>
					
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						
						<a  href="<?php echo base_url();?>report/addeventtype/<?php echo $eventlist->ID;?>" title="Edit/View" class="label label-table label-success btn-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deleteeventtype/<?php echo $eventlist->ID;?>" title="Delete" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin//<?php echo $contract->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					
					
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
