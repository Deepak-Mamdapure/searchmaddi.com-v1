<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Sub Category</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Sub Category Details</h3>
						</div>
						<div class="panel-body">
							<a href="<?php echo base_url();?>admin/addcategory"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Category and Sub-Category</button></a>
							<a href="<?php echo base_url();?>admin/categoryinfo" class="btn btn-info btn-labeled fa fa-reply">Back To List</a>
							
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										
										<th data-field="name" data-sortable="true">Name</th>
										<th data-field="date" data-sortable="true" >Kannada</th>
										
										<th data-field="status" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($cat  as $catinfo) { 
										?>	
                    <tr >
                       
						<td><?php echo $catinfo->Name;?></td>
						<td><?php echo $catinfo->Kannada;?></td>
                        		
                 
                        <td class="form-group"> <a  href="<?php echo base_url();?>admin/editsubcat/<?php echo $catinfo->ID;?>" class="label label-table label-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>admin/deletecat/<?php echo $catinfo->ID;?>" class="label label-table label-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>