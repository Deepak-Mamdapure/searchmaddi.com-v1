<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Online Details</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Online Data List</h3>
						</div>
						<div class="panel-body">
						<a href="<?php  echo base_url();?>report/onlinedata"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Online Data</button></a>
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="companyname"
								   data-page-list="[5, 10, 20]"
								   data-page-size="20"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="companyname" data-sortable="true">Company Name </th>
										<th data-field="contactperson" data-sortable="true" >Contact Person</th>
										<th data-field="phone" data-sortable="true" >Phone Number</th>
										<th data-field="email" data-sortable="true" >Email</th>
										<th data-field="address" data-sortable="true" >Address</th>
										<th data-field="catg" data-sortable="true" >Category Name</th>
										<th data-field="setting" data-sortable="true" >Setting</th>
										
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($onlinelist  as $datalist) { 
										?>	
                    <tr>
					   
								<td><?php echo $datalist->CompanyName;?></td>
								<td><?php echo $datalist->ContactPerson;?></td>
                        		<td><?php echo $datalist->PhoneNumber;?></td>
                        		<td><?php echo $datalist->Email;?></td>
                        		<td><?php echo $datalist->Address;?></td>
								
								<td><?php $catg= $this->db->get_where('tbl_category' , array('ID' => $datalist->CategoryId))->row_array(); echo $catg['Name'];?></td>
                        		
                        <td class="form-group"> <a  href="<?php echo base_url();?>report/updateonlinedata/<?php echo $datalist->ID;?>" class="label label-table label-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deleteonlinedata/<?php echo $datalist->ID;?>" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>