<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Data Not Found</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Not Found</h3>
								</div>
					
								<!--Horizontal Form-->
								<!--===================================================-->
								<form method="post" action="<?php echo base_url();?>iro/storedata" class="form-horizontal" enctype="multipart/form-data">
									<div class="panel-body">
									<input type="hidden" value="<?php if(isset($datalist)) {echo $datalist->ID;}?>" name="id">
									<div class="col-sm-6">
									
										<div class="form-group">
											<label class="col-sm-3 control-label" >Company Name</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Company Name" value="<?php if(isset($datalist)) {echo $datalist->CompanyName;}?>" name="companyname" required id="name" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label"> Category Name</label>
											<select class="selectpicker"  name="categoryname" >
												 <option value="">Select Category </option>
                                    <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_category` where parent=0")->result_array();
                
                        foreach ($query  as $datalist1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $datalist1['ID']; ?>" <?php if(isset($datalist)) {if($datalist1['ID'] == $datalist->CategoryID){ echo "selected";}}?>><?php echo $datalist1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Category Not Found</label>
											<div class="col-sm-6">
												<input type="text" placeholder="Enter Category, If not found in list" name="catgnotfound" value="<?php if(isset($datalist)){ echo $datalist->CategoryNotFound;} ?>"  id="empid" class="form-control">
											
											</div>
										</div>
										
										
									</div>
									<div class="col-sm-6">
									<div class="form-group city">
											<label class="col-sm-2 control-label"><b>City</b></label>
											<div class="col-sm-6">
											<select class="selectpicker"   name="city" required id="txt7" data-live-search="true">
												 <option value="">Select City </option>
                                   <?php
            
            $query=$this->db->query("SELECT * FROM `tbl_location` where flag='0'")->result_array();
                
                        foreach ($query  as $lac1)
                        { 
                        
                        
                        ?>
                                        <option value="<?php echo $lac1['ID']; ?>" <?php if(isset($datalist)) { if($datalist->CityID== $lac1['ID']) {echo "selected";} else{ }}else{ if($lac1['ID'] == $setcity) {echo "selected";} else{ }}?><?php ?>><?php echo $lac1['Name']; ?></option>
                                        
                                        <?php
                                        }
                                        ?>
												</select>
												</div>
												</div>
										<div class="form-group area">
											<label class="col-sm-2 control-label"><b>Area</b></label>
											<div class="col-sm-6">
											<select class="selectpic"  name="area" required id="txt8" data-live-search="true" style="width: 200px;height: 30px;">
												 <option value="">Select area </option>
												 <?php
 if(isset($datalist)) {
            $query=$this->db->query("SELECT * FROM `tbl_location` where Parent='".$datalist->CityID."'")->result_array();
                
                        foreach ($query  as $area)
                        { 
                        
                        
                        ?>
                                         <option value="<?php echo $area['Name']; ?>" <?php if(isset($datalist)) { if($datalist->AreaID== $area['ID']) {echo "selected";}}?>><?php echo $area['Name']; ?></option>
                                        
                                        <?php
                                        }
 }
                                        ?>
                                   
												</select>
												
												</div>
												</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Employee ID</label>
											<div class="col-sm-5">
												<input type="text" placeholder="Employee ID" name="empid" value="<?php if(isset($datalist)){ echo $datalist->EmployeeId;}else{ echo $this->session->userdata('Emp_ID');} ?>" <?php if(!($this->session->userdata('Emp_ID') == "1001")){ echo 'readonly';}  ?> required id="empid" class="form-control">
											
											</div>
										</div>
											
									</div>
									</div>
									<div class="panel-footer text-center">
									<a href="<?php echo base_url();?>iro/irodatanotfound" class="btn btn-purple btn-labeled fa fa-eye">View List</a>
									
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Horizontal Form-->
					
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
