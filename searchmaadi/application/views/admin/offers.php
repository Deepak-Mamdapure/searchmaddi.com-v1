<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Offer Details</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> Enter Offer Details</h3>
						</div>
						<div class="panel-body">
							<a href="<?php echo base_url();?>report/offerlists" class="btn btn-purple btn-labeled fa fa-eye pull-right">View List</a>
							
						<form action="<?php echo base_url();?>report/saveoffers" method="post" enctype="multipart/form-data">
							<?php if(isset($offerinfo)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($offerinfo)) { echo $offerinfo->OfferId;}?>">
								<?php 
								}
									?>		
								<?php if(isset($contractinfo)) {
									?>
								<input type="hidden" name="contractid" value="<?php if(isset($contractinfo)) { echo $contractinfo->ContractID;}?>">
								<?php 
								}
									?>	
							<div class="row col-lg-12">
							<div class="col-lg-4">
							<input type="text" readonly name="contractId" class="form-control" placeholder="ContractId" value="<?php if(isset($offerinfo)) { echo $offerinfo->ContractId;}else { echo $contractinfo->ContractID;} ?>" required >
							
						<br />
								<input type="text" readonly name="companyname" class="form-control" placeholder="Company Name" value="<?php if(isset($offerinfo)) { echo $offerinfo->CompanyName;}else { echo $contractinfo->CompanyName;} ?>" required >
							
						
						</div>
						<div class="col-lg-4">
							<input type="text" name="title" class="form-control" placeholder="Offer Title" value="<?php if(isset($offerinfo)) { echo $offerinfo->OfferTitle;} ; ?>" required >
							<br />
							<textarea name="description" rows="2" cols="50"  placeholder="Offer Description"><?php if(isset($offerinfo)) { echo $offerinfo->OfferDescription;}; ?></textarea>
						<br />
						</div>
						<div class="col-lg-2">
								<label>Valid From</label>
								<input type="date" name="validfrom" class="form-control" value="<?php if(isset($offerinfo)) { echo $offerinfo->ValidFrom;} ;?>" required >
						</div>
						<div class="col-lg-2">
							<label>Valid Till</label>
						<input type="date" name="validto" class="form-control" value="<?php if(isset($offerinfo)) { echo $offerinfo->ValidTo;}; ?>" required >
						</div>
						</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Save</button>
								<a href="<?php echo base_url();?>report/contractlist" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>