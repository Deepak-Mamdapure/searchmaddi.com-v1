<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">ME Allocation List</h1>  

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Allocation List</h3>
						</div>
							<div class="panel-body">
							<a href="<?php echo base_url();?>report/allocateme"><button id="demo-btn-addrow" class=" pull-left btn btn-purple btn-labeled fa fa-plus">Allocate ME Agents</button></a>
							<br /><br /><br />
							<div class="col-lg-12 row well">
							<form action="<?php echo base_url();?>report/" method="post">
									
									
									<div class="col-lg-9">
												<div class="col-lg-7">
												<input type="text" class="form-control" name="searchcontracts" placeholder="Search By ME Name, ME Phone,ME Code" />
												</div>
												<div class="col-md-2">
												<input type="submit" value="Search" class="btn btn-sm btn-purple" />
											</div>
									</div>
									
								</form>
									
									</div>
								<br /><br /><br /><br />
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-sort-name="id"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="id" data-sortable="true">Dasabase Agent Name</th>
										<th data-field="agentname" data-sortable="true">ME Agent Name</th>
										<th data-field="name" data-sortable="true">City</th>
										<th data-field="name2" data-sortable="true">Area</th>
										<th data-field="amount" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($meallocationlist  as $allocate) { 
										?>	
                    <tr>
					<td><?php $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $allocate->Dasabase_id))->row_array(); echo $emp['Name'];?></td>
					<td><?php $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $allocate->ME_id))->row_array(); echo $emp['Name'];?></td>
					
					<td><?php $City= $this->db->get_where('tbl_location' , array('ID' => $allocate->City))->row_array(); echo $City['Name'];?></td>
					<td><?php $Area= $this->db->get_where('tbl_location' , array('ID' => $allocate->Area))->row_array(); echo $Area['Name'];?></td>
					<td class="form-group">
						
						<a  href="<?php echo base_url();?>report/editallocation/<?php echo $allocate->ID;?>" title="Edit/View" class="label label-table label-success btn-success">
                               Edit/View
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deleteallocation/<?php echo $allocate->ID;?>" title="Delete" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
				</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>