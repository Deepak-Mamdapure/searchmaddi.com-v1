<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Contract Reports</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
					<div class="panel-body">
					<div class="row">
					<form action="<?php echo base_url();?>admin/contractreport" method="post">
														<div class="col-sm-4 text-center">
												<div class="form-group">
													
													<input type="text" name="name" required value="" placeholder="Company Name/ContractID" id="txt1"  class="form-control">
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													
													<button class="btn btn-info" type="submit" name="to_web">Search </b></button>
												</div>
												
											</div>
											
											
										</div>
									
									</form>
										</div>
						<div class="panel-heading">
							<h3 class="panel-title"><?php //echo $mess;?> Details</h3>
						</div>
						<div class="panel-body">
							<!--<a href="<?php // echo base_url();?>admin/"><button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add Contrct</button></a>-->
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   >
								<thead>
									<tr>
										
										
										<th data-field="track" data-sortable="true" >ContractID</th>
										<th data-field="name" data-sortable="true">CompanyName</th>
											<th data-field="da" data-sortable="true" >Package </th>
										<th data-field="date" data-sortable="true" >MOP</th>
										<th data-field="t" data-sortable="true" >Price</th>
										<th data-field="tr" data-sortable="true" >GST</th>
										<th data-field="dat" data-sortable="true" >Contract Status</th>
									
										<th data-field="trac" data-sortable="true" >Bank</th>
										
										<th data-field="tra" data-sortable="true" >Cleared?</th>
										<th data-field="status" data-sortable="true" >Status</th>
										
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
				   if(isset($tme)) {
										$i=1;
										foreach ($tme  as $tmeinfo) { 
										?>	
                    <tr >
					
                        <!--
						<td><?php // $emp= $this->db->get_where('tbl_admin' , array('Emp_ID' => $tmeinfo->EmpID))->row_array(); 
echo $emp['Name'];?></td>
						<td><img src="<?php //echo base_url().$tmeinfo->Image;?>" width="100" alt="" /></td>-->
						<td><?php echo $tmeinfo->ContractID;?></td>
						<td><?php  $emp= $this->db->get_where('tbl_contract' , array('ContractID' => $tmeinfo->ContractID))->row_array(); 
echo $emp['CompanyName'];?></td>
						
                        		<td><?php echo $tmeinfo->Package;?></br> 
								
                       </td>
					   <td><?php echo $tmeinfo->MOP;?></br> </td>
								
      
					   <td>₹<?php echo $tmeinfo->Price;?></br> </td>
								<td><?php echo $tmeinfo->GST;?>%</br> 
					   </td>
					   <td class="form-group">
						<?php
						
						if($tmeinfo->Payment_Status==4 || $tmeinfo->Payment_Status==1)
						{
							echo '<button style="background-color: #4CAF50;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-check fa-2x">Received';
							
						}
						else{
							echo '<button style="background-color: #DAA520;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-ban fa-2x">Pending';
						}
						
						
						
						?>
                               
                            
                            </button>
                          
                            </td>
							 <td class="form-group"> 
						
						<?php
						
						
						 if($tmeinfo->Payment_Status==2 || $tmeinfo->Payment_Status==1)
						{
							echo '<button style="background-color: #4CAF50;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-check fa-2x">Received';
							
						}
						else
						{
							echo '<button style="background-color: #DAA520;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-ban fa-2x">Pending';
						}
						?>
                               
                            
                            </button>
                            
                          
                            </td>
							 <td class="form-group"> 
						<?php
						
						
						if($tmeinfo->Payment_Status==1)
						{
							echo '<button style="background-color: #4CAF50;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-check fa-2x">Cleared';
							
						}
						else{
							echo '<button style="background-color: #DAA520;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-ban fa-2x">Pending';	
						}
						?>
                               
                            
                            </button>
                          
                            </td>
							<td class="form-group"> 
						<?php
						
						
						if($tmeinfo->Payment_Status==3)
						{
							echo '<button style="background-color: #FF4500;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-ban fa-2x">Bounce';
							
						}
						else if($tmeinfo->Payment_Status==1)
						{
							echo '<button style="background-color: #4CAF50;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-check fa-2x">Received';
							
						}
						else
						{
							
						echo '<button style="background-color: #DAA520;" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-ban fa-2x">Pending';	
						}
						?>
                               
                            
                            </button>
                          
                            </td>
				
                    </tr>
					  <?php
											   $i++;
										}
				   }
										
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>