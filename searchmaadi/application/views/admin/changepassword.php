<?php include('header.php');?>

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Change Password</h1>

					<!--Searchbox-->
					
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					
					
					<div class="row">
						
						<div class="col-sm-6">
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Change Password</h3>
								</div>
					
								<!--Block Styled Form -->
								<!--===================================================-->
								<form action="<?php echo base_url();?>user/savenewpassword" method="post">
									<div class="panel-body">
									<div class="row">
											<div class="col-sm-6">
											<div class="form-group">
											<label class="control-label">Old Password</label>
												<input type="text" placeholder="Old Password"  name="oldpassword" required id="oldpassword" class="form-control">
											
										</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label class="control-label">New Password</label>
												<input type="text" placeholder="New Password"  name="newpassword" required id="newpassword" class="form-control">
											</div>
												
											</div>
										</div>
										<div class="row">
											
											
										</div>
									</div>
									<div class="panel-footer text-right">
										<button class="btn btn-info" type="submit">Save</button>
									</div>
								</form>
								<!--===================================================-->
								<!--End Block Styled Form -->
					
							</div>
						</div>
					</div>
					
					
					
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

<?php include('footer.php');?>
