<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Facilities & Amenities</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> <?php if(isset($facilities)) {echo "Update Facilities & Amenities Details"; } else { echo "Add Facilities & Amenities Details"; } ?></h3>
						</div>
						<div class="panel-body">
						<form action="<?php echo base_url();?>Report/savefacilities" method="post" enctype="multipart/form-data">
							<div class="row col-lg-12">
							<div class="col-lg-4">
							
						<?php if(isset($facilities)) {
									?>
								<input type="hidden" name="id" value="<?php if(isset($facilities)) { echo $facilities->ID;}?>">
								<?php 
								}
									?>	
								<select class="form-control" name="category" required >
								  <option value="">Select Category</option>
										<?php
										
												  foreach($categorylist as $catg) 
												  { ?>
													<option value="<?php echo $catg->ID; ?>" <?php if(isset($facilities)) { if($catg->ID== $facilities->Category){ echo "selected";}}?>><?php echo $catg->Name; ?></option>
												  <?php } ?>
								</select>
							
						</div>
							<div class="col-lg-4">
								<input type="text" name="amenities" class="form-control" value="<?php if(isset($facilities)) { echo $facilities->Name ;}?>" placeholder="Facilities & Amenities" required >
							
						
							
						</div>
						
						
						</div>
						<div class="row col-lg-12">
							<br />
							<div class="col-lg-4"></div>
							<div class="col-lg-4" align="center">
								<button class="btn btn-success text-uppercase" type="submit">Save</button>
								<a href="<?php echo base_url();?>report/facilitiesinfo" class="btn btn-purple btn-labeled fa fa-reply">Back To List</a>
							
							</div>
							<div class="col-lg-4"></div>
						</div>
						
						</form>
						
						
						</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->

	</div>
<?php include('footer.php');?>