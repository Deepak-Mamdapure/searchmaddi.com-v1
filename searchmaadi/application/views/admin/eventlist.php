<?php include('header.php');?>
		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow">Events List</h1>

					<!--Searchbox-->
				
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->

				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
										<!--===================================================-->
					
					
					<!--===================================================-->
					
				
					<!--===================================================-->
					
					
					<!--Basic Toolbar-->
					<!--===================================================-->
								<!--===================================================-->
					
					
					<!--Custom Toolbar-->
					<!--===================================================-->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Events List</h3>
						</div>
						<div class="panel-body">
						<a href="<?php echo base_url();?>report/contractlist" class="btn btn-purple btn-labeled fa fa-reply pull-left">Back To Contract List</a>
						<a href="<?php echo base_url();?>report/events" class="btn btn-purple btn-labeled fa fa-plus">Add Events Without Contract</a>
						
							<table id="demo-custom-toolbar" class="demo-add-niftycheck" data-toggle="table"
								   data-url="data/bs-table.json"
								   data-toolbar="#demo-delete-row"
								   data-search="true"
								   data-show-refresh="true"
								   data-show-toggle="true"
								   data-show-columns="true"
								   data-sort-name="companyname"
								   data-page-list="[5, 10, 20]"
								   data-page-size="5"
								   data-pagination="true" data-show-pagination-switch="true">
								<thead>
									<tr>
										
										<th data-field="companyname" data-sortable="true">CompanyName</th>
										<th data-field="eventname" data-sortable="true">Event Name</th>
										<th data-field="desc" data-sortable="true" >Description</th>
										<th data-field="address" data-sortable="true" >Address</th>
										<th data-field="phone" data-sortable="true" >Phone Number</th>
										<th data-field="date" data-sortable="true" >Date</th>
										<th data-field="time" data-sortable="true">Time</th>
										<th data-field="fees" data-sortable="true" >Event Fees</th>
										<th data-field="setting" data-sortable="true" >Setting</th>
										<!--<th data-field="amount" data-align="center" data-sortable="true" data-sorter="priceSorter">Balance</th>
										<th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>
										<th data-field="track" data-align="center" data-sortable="true" data-formatter="trackFormatter">Tracking Number</th>-->
									</tr>
								</thead>
								 <tbody>
                   <?php
											$i=1;
										foreach ($eventlist  as $event) { 
										?>	
                    <tr >
					<td><?php echo $event->CompanyName;?></td>
						<td><?php echo $event->Title;?></td>
                        		<td><?php echo $event->Description;?></td>
                        		<td><?php echo $event->Address;?></td>
                        		<td><?php echo $event->PhoneNumber;?></td>
                        		<td><?php echo $event->StartDate ."<br /> To <br />". $event->EndDate;?></td>
                        		<td><?php echo $event->StartTime . "<br /> To <br />".$event->EndTime;?></td>
								<td><?php echo $event->EventFees;?></td>
								
               <?php
			 
		  if ($this->session->userdata('Role')!=3)
    { 
			   ?>
                        <td class="form-group">
						
						<a  href="<?php echo base_url();?>report/editevent/<?php echo $event->EventId;?>" class="label label-table label-success btn-success">
                               Edit
                            </a>
                            
                            <a  href="<?php echo base_url();?>report/deleteevent/<?php echo $event->EventId;?>" class="label label-table label-danger btn-danger">
                               Delete
                            </a>
							 
                            </td>
							<?php
	}
	else
	{
							?>
							<td class="form-group"> 
                            
                            <a  href="<?php echo base_url();?>admin//<?php echo $event->ID;?>" class="label label-table label-success btn-danger">
                               Make Sale
                            </a>
                            </td>
							<?php
	}
							?>
				
                    </tr>
					  <?php
											   $i++;
										}
												?>
                    
                    </tbody>
							</table>
						</div>
					</div>
					<!--===================================================-->
					
					
					<!--Editable - combination with X-editable-->
					<!--===================================================-->
				
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


<?php include('footer.php');?>