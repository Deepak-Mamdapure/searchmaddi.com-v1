<?php 
class MYPDF extends TCPDF {

	public function Header() {
        $this->SetFont('helvetica', 'B', 20);
    }

	public function Footer() {
		$this->SetY(-25);
        $this->SetFont('helvetica', 'I', 8);
        $this->MultiCell(0, 10, "THANK YOU FOR YOUR BUSINESS!", 0, 'C');
        $this->SetY(-15);
        $this->MultiCell(0, 10, "For invoice enquiries please send email to accounts@foodinns.com(+61 7 3177-7088)"."\n"."Payment terms are strictly 14 days - Penalty may applied on all overdue amounts."."\n"."Commission and fees charged by FoodInns as intermediary service provider to your restaurant.", 0, 'C');
	}

}

?>