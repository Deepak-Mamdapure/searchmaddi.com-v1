<?php
class Reportmod extends CI_Model {
function __construct()
    {
        parent::__construct();
		
    }


		public  function GetHash($qtd) {
        	//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
        	$Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
       		$QuantidadeCaracteres = strlen($Caracteres);
        	$QuantidadeCaracteres--;
        
        	$Hash=NULL;
        	for($x=1;$x<=$qtd;$x++){
            	$Posicao = rand(0,$QuantidadeCaracteres);
            	$Hash .= substr($Caracteres,$Posicao,1);
        	}
        return $Hash;
    	}
			
		 public function contractlist($flagid,$id){
			 $role = $this->session->userdata('Role');
			if($role==1)
			{
			if (!empty($this->input->post('searchcontracts'))) {

					$term = $this->input->post('searchcontracts');     
		 
					$query = $this->db->query("SELECT * FROM tbl_contract WHERE (`Status` = '0' And `flag` = '$flagid' And `Role` = '$id') And (`Tollfree` LIKE '%".$term."%' or `CompanyName` LIKE '%".$term."%' or `Email` LIKE '%".$term."%' or `Number` LIKE '%".$term."%' or `ContractID` LIKE '%".$term."%')"); 
						
					return $query->result();
				}
				else{
					$query = $this->db->get_where('tbl_contract',array('Status' => 0,'flag'=>$flagid, 'Role'=>$id));
					return $query->result();
				}
				
			}else if($role==5){
				if (!empty($this->input->post('searchcontracts'))) {

					$term = $this->input->post('searchcontracts');     
		 
				$query=$this->db->query("SELECT * FROM tbl_contract where flag in (0,2) and EmpID in (select Contract_ID from  tbl_assigned where status=0 and Role='".$id."')  And (`Tollfree` LIKE '%".$term."%' or `CompanyName` LIKE '%".$term."%' or `Email` LIKE '%".$term."%' or `Number` LIKE '%".$term."%' or `ContractID` LIKE '%".$term."%')");
				return $query->result();
				}
				else{
					$query = $this->db->get_where('tbl_contract',array('Status' => 0,'flag'=>$flagid, 'Role'=>$id));
					return $query->result();
				}
			}
			else
			{
			if (!empty($this->input->post('searchcontracts'))) {

					$term = $this->input->post('searchcontracts');     
		 
					$query = $this->db->query("SELECT * FROM tbl_contract WHERE (`Status` = '0' And `flag` = '$flagid') And (`Tollfree` LIKE '%".$term."%' or `CompanyName` LIKE '%".$term."%' or `Email` LIKE '%".$term."%' or `Number` LIKE '%".$term."%' or `ContractID` LIKE '%".$term."%')"); 
						
					return $query->result();
				}
				else{
					$query = $this->db->get_where('tbl_contract',array('Status' => 0,'flag'=>$flagid));
					return $query->result();
				}
				
			}
		 }
		  public function newcontractlist($flagid=""){
			$emp=$this->session->userdata('Emp_ID');
				if (!empty($this->input->post('searchcontracts'))) {

					$term = $this->input->post('searchcontracts');     
		 
				$query=$this->db->query("SELECT * FROM tbl_contract where flag in (0,2) and EmpID in (select Contract_ID from  tbl_assigned where status=0 and EmpID='".$emp."')  And (`Tollfree` LIKE '%".$term."%' or `CompanyName` LIKE '%".$term."%' or `Email` LIKE '%".$term."%' or `Number` LIKE '%".$term."%' or `ContractID` LIKE '%".$term."%')");
				return $query->result();
				}
				else if (!empty($flagid)){
					$query = $this->db->get_where('tbl_contract',array('Status' => 0,'Disposition'=>$flagid, 'EmpID'=>$emp));
					return $query->result();
				}
				else{
					$query = $this->db->get_where('tbl_contract',array('Status' => 0, 'EmpID'=>$emp));
					return $query->result();
				}
			}
			  public function irototme(){
			$emp=$this->session->userdata('Emp_ID');
				
					$query = $this->db->get_where('tbl_customerbusiness',array('Status' => 0, 'TMEID'=>$emp, 'flag'=>0));
					return $query->result();
				
			}
		public function onlinedatalist(){
			
			$query = $this->db->get_where('tbl_online',array('Status' => 0));
			return $query->result();
		}
		public function singleonlinedata($id){
			
			$query = $this->db->get_where('tbl_online',array("Status" => 0, "ID" => $id));
			return $query->result()[0];
		}
		
		
		public function facilitiesdata(){
			$facilities = array();
			$facilities['Name']               =	trim($this->input->post('amenities'));
        $facilities['Category']               =	trim($this->input->post('category'));
		if(empty($this->input->post('id')))
		{
					$this->db->insert('tbl_facilities',$facilities);
						if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return false;
        }
		}
		else {
			$this->db->where('ID',$this->input->post('id'));
			$this->db->update('tbl_facilities',$facilities);
				if($this->db->affected_rows() > 0){
            return 2;
        }else{
            return false;
        }
		}
				
		}
		public function saveonline(){
			$online = array();
			//$online['Name']               =	trim($this->input->post('amenities'));
        $online['CompanyName']               =	trim($this->input->post('companyname'));
		$online['ContactPerson']               =	trim($this->input->post('contactperson'));
        $online['PhoneNumber']               =	trim($this->input->post('phonenumber'));
		$online['Email']               =	trim($this->input->post('email'));
        $online['Address']               =	trim($this->input->post('address'));
		//$online['City']               =	trim($this->input->post('amenities'));
        //$online['PinCode']               =	trim($this->input->post('category'));
		//$online['Area']               =	trim($this->input->post('Email'));
		$online['CategoryId']               =	trim($this->input->post('category'));
       // $online['PinCode']               =	trim($this->input->post('category'));
		
		if(empty($this->input->post('id')))
		{
					$this->db->insert('tbl_online',$online);
		}
		else {
			$this->db->where('ID',$this->input->post('id'));
			$this->db->update('tbl_online',$online);
		}
					if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function facilitieslist(){
			
			$query = $this->db->get_where('tbl_facilities',array('Status' => 0));
			return $query->result();
		}
		public function getContractById($ID){
			  if( $this->router->fetch_method()=="editevent")
			  	{
					$this->db->where('EventId',$ID);
					$query = $this->db->get('tbl_events');
				}elseif( $this->router->fetch_method()=="editoffers")
			  	{
					$this->db->where('OfferId',$ID);
					$query = $this->db->get('tbl_offers');
				}
				elseif( $this->router->fetch_method()=="editfacilities")
			  	{
					$this->db->where('ID',$ID);
					$query = $this->db->get('tbl_facilities');
				}
				elseif( $this->router->fetch_method()=="editonlinedata")
			  	{
					$this->db->where('ID',$ID);
					$query = $this->db->get('tbl_online');
				}
				elseif( $this->router->fetch_method()=="updatepersondata")
			  	{
					$this->db->where('ID',$ID);
					$this->db->where('Status','0');
					$query = $this->db->get('tbl_famouspersonality');
				}
				elseif( $this->router->fetch_method()=="updatepackages")
			  	{
					$this->db->where('ID',$ID);
					$this->db->where('Status','0');
					$query = $this->db->get('tbl_package');
				}
				elseif( $this->router->fetch_method()=="addeventtype")
			  	{
					$this->db->where('ID',$ID);
					$this->db->where('Status','0');
					$query = $this->db->get('tbl_eventtypes');
				}
				elseif( $this->router->fetch_method()=="addsourcetype")
			  	{
					$this->db->where('ID',$ID);
					$this->db->where('Status','0');
					$query = $this->db->get('tbl_sourcetype');
				}
				else{
				  $this->db->where('ContractId',$ID);
				  $query = $this->db->get('tbl_contract');
				}					
        
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
		public function events($contractid){
			
			if ($_FILES['smallimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['smallimage']['name'];
	                  		$file_size =$_FILES['smallimage']['size'];
	                  		$file_tmp =$_FILES['smallimage']['tmp_name'];
	                  		$file_type=$_FILES['smallimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/events/".uniqid();

	                  	 $smallimagelink=$path.".".end($file_name);
	  //$file_ext=strtolower(end(explode('.',$_FILES['userfile']['name'])));
	   //$expensions= array("jpeg","jpg","png","psd");
      /*
      if(in_array($file_ext,$expensions)=== false ){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
	  */
      
      if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }

     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	   $smallimagelink=$this->input->post('smallimageupdate');               		
	                 		
	}
			$data = array(
			"EventId" => "EVE".date('dmY').rand(0,100000),
			"ContractId" => $this->input->post('contractId'),
			"CompanyName" => $this->input->post('companyname'),
			"ContactPerson" => $this->input->post('contactperson'),
			"PhoneNumber" => $this->input->post('phonenumber'),
			"Email" => $this->input->post('email'),
			"Website" => $this->input->post('website'),
			"Title" => $this->input->post('title'),
			"Description" => $this->input->post('description'),
			"Address" => $this->input->post('address'),
			"StartDate" => $this->input->post('startdate'),
			"EndDate" => $this->input->post('enddate'),
			"StartTime" => $this->input->post('starttime'),
			"EndTime" => $this->input->post('endtime'),
			"EventFees" => $this->input->post('eventfees'),
			"EventImage" => $smallimagelink,
			"EventType" => $this->input->post('eventtype')
			
			);
			$this->db->insert('tbl_events',$data);
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function updateevents($eventid){
			if ($_FILES['smallimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['smallimage']['name'];
	                  		$file_size =$_FILES['smallimage']['size'];
	                  		$file_tmp =$_FILES['smallimage']['tmp_name'];
	                  		$file_type=$_FILES['smallimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/events/".uniqid();

	                  	 $smallimagelink=$path.".".end($file_name);
	  //$file_ext=strtolower(end(explode('.',$_FILES['userfile']['name'])));
	   //$expensions= array("jpeg","jpg","png","psd");
      /*
      if(in_array($file_ext,$expensions)=== false ){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
	  */
      
      if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }

     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	   $smallimagelink=$this->input->post('smallimageupdate');               		
	                 		
	}
			$data = array(
			"PhoneNumber" => $this->input->post('phonenumber'),
			"Email" => $this->input->post('email'),
			"Website" => $this->input->post('website'),
			"Title" => $this->input->post('title'),
			"Description" => $this->input->post('description'),
			"Address" => $this->input->post('address'),
			"StartDate" => $this->input->post('startdate'),
			"EndDate" => $this->input->post('enddate'),
			"StartTime" => $this->input->post('starttime'),
			"EndTime" => $this->input->post('endtime'),
			"EventFees" => $this->input->post('eventfees'),
			"EventImage" => $smallimagelink,
			"EventType" => $this->input->post('eventtype')
			
			);
			$this->db->where('EventId',$eventid);
			$this->db->update('tbl_events',$data);
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function offers($contractid){
			$data = array(
			"OfferId" => "Off".date('dmY').rand(0,100000),
			"ContractId" => $this->input->post('contractId'),
			"CompanyName" => $this->input->post('companyname'),
			"OfferTitle" => $this->input->post('title'),
			"OfferDescription" => $this->input->post('description'),
			"ValidFrom" => $this->input->post('validfrom'),
			"ValidTo" => $this->input->post('validto')
			
			);
			$this->db->insert('tbl_offers',$data);
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function updateoffer($offerid){
			$data = array(
			"OfferTitle" => $this->input->post('title'),
			"OfferDescription" => $this->input->post('description'),
			"ValidFrom" => $this->input->post('validfrom'),
			"ValidTo" => $this->input->post('validto')
			
			);
			$this->db->where('OfferId',$offerid);
			$this->db->update('tbl_offers',$data);
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function eventlist(){
			
			$query = $this->db->get_where('tbl_events',array('Status' => 0));
			return $query->result();
		}
		public function saveeventtype($id = ''){
			$data = array(
			"Name" => $this->input->post('name')
			
			);
			if($id != ''){
			$this->db->where('ID',$id);
			$this->db->update('tbl_eventtypes',$data);
			}else{
				$this->db->insert('tbl_eventtypes',$data);
			}
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function savesourcetype($id = ''){
			$data = array(
			"Name" => $this->input->post('name')
			
			);
			if($id != ''){
			$this->db->where('ID',$id);
			$this->db->update('tbl_sourcetype',$data);
			}else{
				$this->db->insert('tbl_sourcetype',$data);
			}
				if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
		}
		public function offerlist(){
			
			$query = $this->db->get_where('tbl_offers',array('Status' => 0));
			return $query->result();
		}
		public function getfacilities($id){
			
			$query = $this->db->get_where('tbl_facilities',array('ID' => $id));
			return $query->result();
		}
		public function deletelist($ID){
			  if( $this->router->fetch_method()=="deleteevent")
			  	{
					$data = array('Status' => '1');
					$this->db->where('EventId',$ID);
					$this->db->update('tbl_events',$data);
				}
				elseif( $this->router->fetch_method()=="deleteoffer")
			  	{
					$data = array('Status' => '1');
					$this->db->where('OfferId',$ID);
					$this->db->update('tbl_offers',$data);
				}elseif( $this->router->fetch_method()=="deletecontract")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_contract',$data);
				}
				elseif( $this->router->fetch_method()=="deletefacilities")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_facilities',$data);
				}
				elseif( $this->router->fetch_method()=="deleteonlinedata")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_online',$data);
				}elseif( $this->router->fetch_method()=="deletefamouspersons")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_famouspersonality',$data);
				}elseif( $this->router->fetch_method()=="deletepackages")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_package',$data);
				}elseif( $this->router->fetch_method()=="deleteeventtype")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_eventtypes',$data);
				}elseif( $this->router->fetch_method()=="deletesourcetype")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_sourcetype',$data);
				}elseif( $this->router->fetch_method()=="deleteallocation")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_meallocation',$data);
				}
				elseif( $this->router->fetch_method()=="deletemovies")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_movie',$data);
				}elseif( $this->router->fetch_method()=="deletenotfound")
			  	{
					$data = array('Status' => '1');
					$this->db->where('ID',$ID);
					$this->db->update('tbl_datanotfound',$data);
				}
					if($this->db->affected_rows() > 0){
					 return true;
					}else{
					 return false;
					}
				  }
		  
		  public function famouspersons(){
			
			$query = $this->db->get_where('tbl_famouspersonality',array('Status' => 0));
			return $query->result();
		}
		
		public function packagelist(){
			
			$query = $this->db->get_where('tbl_package',array('Status' => 0));
			return $query->result();
		}
		public function sourcetypelist(){
			
			$query = $this->db->get_where('tbl_sourcetype',array('Status' => 0));
			return $query->result();
		}
		public function eventtypelist(){
			
			$query = $this->db->get_where('tbl_eventtypes',array('Status' => 0));
			return $query->result();
		}
		public function melist(){
			
			$query = $this->db->get_where('tbl_admin',array('Role' => 4,'Status' => 0));
			return $query->result();
		}
		public function meallocationlist(){
			
			$query = $this->db->get_where('tbl_meallocation',array('Status' => 0));
			return $query->result();
		}
		public function meallocatedlist($id){
			
			$query = $this->db->get_where('tbl_meallocation',array('Status' => 0,'ID' => $id));
			return $query->result();
		}
		public function movieslist($contractid){
			
			$query = $this->db->get_where('tbl_movie',array('Status' => 0,'ContractId'=>$contractid));
			return $query->result();
		}
		public function moviedetails($contractid,$id){
			
			$query = $this->db->get_where('tbl_movie',array('Status' => 0,'ContractId'=>$contractid,'ID'=>$id));
			return $query->result()[0];
		}
		public function irosearchdata(){
			
			$query = $this->db->get_where('tbl_datanotfound',array('Status' => 0));
			return $query->result();
		}
}