<?php
class Iromod extends CI_Model {
function __construct()
    {
        parent::__construct();
		
    }


		public  function GetHash($qtd) {
        	//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
        	$Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
       		$QuantidadeCaracteres = strlen($Caracteres);
        	$QuantidadeCaracteres--;
        
        	$Hash=NULL;
        	for($x=1;$x<=$qtd;$x++){
            	$Posicao = rand(0,$QuantidadeCaracteres);
            	$Hash .= substr($Caracteres,$Posicao,1);
        	}
        return $Hash;
    	}
		
		public function confirmlogin($data){
			$empid = $data['EmpID'];
			$role = $data['Role'];
			
			$query = $this->db->query("SELECT * FROM `tbl_agentlogin` WHERE `Emp_ID` = '$empid' AND `Role` = '$role' AND `Date`= Date(Now())");
			if($query->num_rows() > 0){
					return $query->row();
				}
		}
		public function addlogintime($arr,$data){
			if( $this->router->fetch_method()=="index")
			  	{
					$this->db->insert('tbl_agentlogin',$arr);

				}elseif( $this->router->fetch_method()=="logout")
			  	{
					$this->db->where('ID',$data);
					$query = $this->db->update('tbl_agentlogin',$arr);
				}elseif( $this->router->fetch_method()=="updatebreak")
			  	{
					$empid = $data['EmpID'];
					$role = $data['Role'];
					$logindate = $data['LoginDate'];
					$array = array(
					'Emp_ID' => $empid,
					'Role' => $role,
					'Date' => $logindate
					);
					
					$this->db->where($array);
					$query = $this->db->update('tbl_agentlogin',$arr);
				}
			
		}
		public function customerlist(){
			$phone = $this->input->post('phone');
			
				$query = $this->db->query("Select * from `tbl_user` where `Status`='0'");
				//$query = $this->db->query("Select * from `tbl_user` where `Status`='0' And (`Number`= '$phone' or`LandlineNumber` = '$phone')");
				//$query = $this->db->get_where('tbl_user',array('Status' => 0,'Number' =>$phone));
				//return $query->result()[0];
				 if($query->num_rows() > 0){
					return $query->row();
				}else{
					return false;
				}
			
		}
		public function contractlist(){
				$term = $this->input->post('searchcontracts');     
				$catg = $this->input->post('search_catg');
				$city = $this->input->post('city');
				$area =str_replace(' ', '-', $this->input->post('area'));
				
				//SELECT * FROM `tbl_contract` LEFT JOIN `tbl_client_cat` ON `tbl_client_cat`.`ContractID` = `tbl_contract`.`ID` LEFT JOIN `tbl_client_location` ON `tbl_client_location`.`ContractID` = `tbl_contract`.`ID` WHERE (`tbl_contract`.`Status` = '0' And `tbl_contract`.`flag` = '1')And `tbl_client_location`.`Area` = 'Cubbon Road' or `tbl_client_cat`.`Cat` = 'Berhampur123'
				
			if($this->input->post('area')!="" && $this->input->post('city')!=""){
				
			if ($this->input->post('searchcontracts')!="" || $this->input->post('search_catg')!="" ) {
				$query = $this->db->query("SELECT `tbl_contract`.`ID`, `tbl_contract`.`ContractID` AS ContractID,`tbl_contract`.`CompanyName`, `tbl_contract`.`Tagline`, `tbl_contract`.`Category`, `tbl_contract`.`SubCategory`, `tbl_contract`.`City`, `tbl_contract`.`STDcode`, `tbl_contract`.`Pincode`, `tbl_contract`.`AreaName`, `tbl_contract`.`Landmark`, `tbl_contract`.`ContactPerson`, `tbl_contract`.`Designation`, `tbl_contract`.`Email`, `tbl_contract`.`Number`, `tbl_contract`.`Landline`, `tbl_contract`.`PaymentType`,`tbl_contract`.`Address`,`tbl_contract`.`Tollfree`,`tbl_client_cat`.`ID` As ClientId, `tbl_client_cat`.`Cat`, `tbl_client_cat`.`CatTag`, `tbl_client_cat`.`ContractID`,`tbl_client_location`.`ID` As ClientLocID, `tbl_client_location`.`ContractID` AS CLID, `tbl_client_location`.`Area`, `tbl_client_location`.`AreaTag` FROM `tbl_contract` INNER JOIN `tbl_client_location` ON `tbl_client_location`.`ContractID` = `tbl_contract`.`ContractID` INNER JOIN `tbl_client_cat` ON `tbl_client_cat`.`ContractID` = `tbl_contract`.`ContractID` WHERE `tbl_contract`.`Status` = '0' AND `tbl_contract`.`flag` = '1' AND `tbl_contract`.`City` = '$city' AND `tbl_client_location`.`AreaTag`='$area' And (`tbl_contract`.`Tollfree` LIKE '%".$term."%' or `tbl_contract`.`CompanyName` LIKE '%".$term."%' or `tbl_contract`.`Number` LIKE '%".$term."%' OR `tbl_client_cat`.`Cat` = '%".$catg."%')");
				return $query->result();
			}else{ return false;}
		}
		
		 }
		public function catglist(){
			if(!empty($this->input->post("keyword"))) {
			$query = $this->db->query("SELECT `Cat` FROM `tbl_client_cat` where `Cat` LIKE '%".$this->input->post("keyword")."%' ORDER BY `Cat` LIMIT 0,6");
			return $query->result();
			}
			
			
		}
		
		public function offerlist(){
					$query = $this->db->query("SELECT * FROM `tbl_offers` WHERE `Status` = '0' AND  DATE(`ValidTo`) > DATE(NOW()) OR  DATE(`ValidTo`) = DATE(NOW())ORDER BY `ID` DESC LIMIT 0,1");
					$offer = $query->result();
					
					if(!empty($offer)){
						
					$offercontid = $offer[0]->ContractId;
					
					if($offercontid != ''){
					$query1 = $this->db->query("SELECT MAX(`Price`) As maxprice FROM `tbl_payment` WHERE `ContractID` = '$offercontid' And `Payment_Status` = '0'")->result();
					
					}
					if(!empty($query1)){
					return $offer[0];
				
				}else{
					return false;
				}
					}else{
						return false;
					}
					
				
			//echo "<pre>"; print_r($query->result());exit;
			//$query = $this->db->query("SELECT `tbl_offers`.`ID`, `tbl_offers`.`OfferId`, `tbl_offers`.`ContractId`, `tbl_offers`.`CompanyName`, `tbl_offers`.`OfferTitle`, `tbl_offers`.`OfferDescription`, `tbl_offers`.`ValidFrom`, `tbl_offers`.`ValidTo`, MAX(`tbl_payment`.`Price`) As maxprice  FROM `tbl_offers` INNER JOIN `tbl_payment` ON `tbl_payment`.`ContractID` = `tbl_offers`.`ContractId` WHERE `tbl_offers`.`Status` = '0' AND `tbl_payment`.`Payment_Status` = '0' AND  DATE(`tbl_offers`.`ValidTo`) > DATE(NOW())  ORDER BY `tbl_offers`.`ID` DESC LIMIT 0,1 ");
					
		}
		public function searchresultdata($id){
				$query = $this->db->get_where('tbl_datanotfound',array('Status' => 0,'ID' => $id));
				return $query->result()[0];
				
			
		}
		public function tmenames(){
					$query = $this->db->query("SELECT * FROM `tbl_admin` WHERE `Role`='3'");
					
				if($query->num_rows() > 0){
					return $query->result();
				
				}else{ return false;}	
			//echo "<pre>"; print_r($query->result());exit;
			
		}
		public function senddatatouser($data){
			
			$count = count($data['contractids']);
			if($data['num'] > $count){
				$datasent = $count;
			}else{
				$datasent = $data['num'];
			}
			$numofcont = array_slice($data['contractids'],0,$datasent);
			$contractid = implode(',', $numofcont);
			
			$prev = explode(",",$data['previouddata']);
			$catg = $prev[0];
			$area = $prev[1];
			
			$arr = array(
				 'EmpId' => $data['EmpID'],
				 'UserId' => $data['userid'],
				 'ContractId' => $contractid,
				 'Category' => $catg,
				 'Area' => $area,
				 'Numberofdata' => $datasent
				);
				$this->db->insert('tbl_trackrecords',$arr);
				
				foreach($numofcont as  $contract){
					$query = $this->db->query("SELECT * FROM `tbl_contract` WHERE `ID` = '$contract' ");
					$City= $this->db->get_where('tbl_location' , array('ID' => $query->result()[0]->City))->result()[0];
					$AreaName= $this->db->get_where('tbl_location' , array('ID' => $query->result()[0]->AreaName, ))->result()[0];
					$result[] = array(
						"CompanyName" => $query->result()[0]->CompanyName,
						"ContactNumber" => $query->result()[0]->Number,
						"Address" => "City :" .$City->Name ."<br />Std Code : ". $query->result()[0]->STDcode . "<br />PinCode :". $query->result()[0]->Pincode. "<br />AreaName :". $AreaName->Name. "<br />Address :". $query->result()[0]->Address,
						"PaymentType" => $query->result()[0]->PaymentType
					);
					
				}
				return $result;
			}
		
}