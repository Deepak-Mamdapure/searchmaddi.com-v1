<?php
class Adminmod extends CI_Model {
function __construct()
    {
        parent::__construct();
		
    }


public  function GetHash($qtd) {
        	//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
        	$Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
       		$QuantidadeCaracteres = strlen($Caracteres);
        	$QuantidadeCaracteres--;
        
        	$Hash=NULL;
        	for($x=1;$x<=$qtd;$x++){
            	$Posicao = rand(0,$QuantidadeCaracteres);
            	$Hash .= substr($Caracteres,$Posicao,1);
        	}
        return $Hash;
    	}
public function dashmod($role){
	$query="";
if($role==9)
{
	$user=$this->session->userdata('userid');	
	$query='where ComID=(select ContractID from tbl_contract where UserID='.$user.')';
}
	$date=date("Y-m-d");
    $query=$this->db->query("SELECT count(*) as totalvisitor,(SELECT count(*)  FROM `tbl_visitor` where date(Created)='$date') as today FROM `tbl_visitor` $query");
   return $query->row();
} 
public function dashvisitior($role){
	$query="";
	if($role==9)
{
	$user=$this->session->userdata('userid');	
	$query='and ComID=(select ContractID from tbl_contract where UserID='.$user.')';
}
	$date=date("Y-m-d");
    $query=$this->db->query("SELECT count(*) as num,City,Region,Country,Created FROM `tbl_visitor` where date(Created)='$date' $query  group by City order by Created DESC");
return $query->result();
} 
        //----------------------------------------------------------login-------------------------------------------------------------
public function login($data) {
$condition = "Emp_ID=" . "'" . $data['Emp_ID'] . "' AND " . "Password =" . "'" . $data['Password'] . "'";
$this->db->select('*');
$this->db->from('tbl_admin');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}
public function cat_mod() {
$this->db->select('*');
$this->db->from('tbl_category');
 $this->db->where('Parent',0);
$query = $this->db->get();
return $query->result();
}
public function subcat_mod($id) {
$this->db->select('*');
$this->db->from('tbl_category');
 $this->db->where('Parent',$id);
$query = $this->db->get();
return $query->result();
}
public function location_mod() {
$this->db->select('*');
$this->db->from('tbl_location');
 $this->db->where('flag',0);
$query = $this->db->get();
return $query->result();
}
public function area_mod($id) {
$this->db->select('*');
$this->db->from('tbl_location');
 $this->db->where('Parent',$id);
$query = $this->db->get();
return $query->result();
}
public function getCatById($ID){
        $this->db->where('ID',$ID);
        $query = $this->db->get('tbl_category');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
	 public function getLocById($ID){
        $this->db->where('ID',$ID);
        $query = $this->db->get('tbl_location');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
	 public function deletecat($id){
        $this->db->where('ID',$id);
        $this->db->update('tbl_category',array("Status" => '1'));
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
     }
	 public function deleteloc($id){
        $this->db->where('ID',$id);
        $this->db->update('tbl_location',array("Status" => '1'));
        $this->db->update('tbl_location',array("Status" => '1'));
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
     }
	
	public function getuserid($data1)
            {
            $condition = "Email =" . "'" . $data1['Email'] . "'";
$this->db->select('*');
$this->db->from('tbl_user');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
if ($query->num_rows() != 0) {
return $query->result();
       
} else {
return false;
}
			}
	 public function insertCSV($data1)
            {
            $condition = "Emp_ID =" . "'" . $data1['Emp_ID'] . "'";
$this->db->select('*');
$this->db->from('tbl_admin');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
if ($query->num_rows() == 0) {
$this->db->insert('tbl_admin', $data1);
if ($this->db->affected_rows() > 0) {
return $this->db->insert_id();
}
} else {
return false;
}
           
            }
		  
  public function importcontract($data) {
  $this->db->insert('tbl_contract', $data);
 if ($this->db->affected_rows() > 0) {
return true;
} else {
return false;
}
}
public function tme_mod($id) {
$this->db->select('*');
$this->db->from('tbl_contract');
 $this->db->where('Role',$id);
$query = $this->db->get();
return $query->result();
}
public function getsubCatById($ID){
        $this->db->where('Parent',$ID);
        $query = $this->db->get('tbl_category');
        if($query->num_rows() > 0){
           return $query->result();
        }else{
            return false;
        }
     }
	 public function getsubCityById($ID){
		 //$this->db->where_in('columnname',$data);
        $this->db->where('Parent',$ID);
        $query = $this->db->get('tbl_location');
        if($query->num_rows() > 0){
           return $query->result();
        }else{
            return false;
        }
     }
	  public function getsubzoneById($ID){
		  $query=$this->db->query("SELECT * FROM `tbl_location` WHERE Zone in ('ZONE1','ZONE2')");
		  /*
		 $this->db->where_in('Zone',$ID);
       // $this->db->where('Parent',$ID);
       $query = $this->db->get('tbl_location');
	   */
        if($query->num_rows() > 0){
           return $query->result();
        }else{
            return false;
        }
     }
	 public function getCityById($ID){
        $this->db->where('ID',$ID);
        $query = $this->db->get('tbl_location');
        if($query->num_rows() > 0){
           return $query->result();
        }else{
            return false;
        }
     }
	 public function getpackById($ID){
        $this->db->where('ID',$ID);
        $query = $this->db->get('tbl_package');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
	 public function savecontract($contract){
   $this->db->insert('tbl_contract', $contract); 
    if($this->db->affected_rows() > 0){
        return $this->db->insert_id();
  
    }else{
        return false;
    }
}
public function allroledata($id,$role) {
	$query="";

if($role==9)
{
	$query='where UserID='.$id;
	//$this->db->where('UserID',$id);
 
}
else if($role==5){
	$query='where flag in (0,2) and EmpID in (select Contract_ID from  tbl_assigned where status=0 and Emp_ID='.$id.')';
}
else{
	//$this->db->where('EmpID',$id);
	$query='where EmpID='.$id;
}
$query=$this->db->query("SELECT * FROM `tbl_contract` $query");
return $query->result();
}
 public function getContractById($ID){
        $this->db->where('ContractID',$ID);
        $query = $this->db->get('tbl_contract');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
	 public function datareportmod($name='',$to='',$from='') {
		 $Role=$this->session->userdata('Role');
		 $query='';
		  if ($Role==3)
    { 
$empid=$this->session->userdata('Emp_ID');
$query="where (ContractID='".$name."' or CompanyName='".$name."') or (date(Created) BETWEEN '$from' AND '$to') and EmpID=".$empid ;
		}
		
 $query=$this->db->query("SELECT * FROM `tbl_contract` $query");
return $query->result();
}
public function contractreportmod($name='') {
		 $Role=$this->session->userdata('Role');
		 $query='';
		  if ($Role==3)
    { 
$empid=$this->session->userdata('Emp_ID');
$query="where (ContractID='".$name."' or ContractID in (SELECT ContractID FROM tbl_contract WHERE CompanyName LIKE '%$name%')) and Emp_ID=".$empid ;
		}
		//echo "SELECT * FROM `tbl_payment` $query";
 $query=$this->db->query("SELECT * FROM `tbl_payment` $query");
return $query->result();
}
public function salereportmod($name='',$to='',$from='') {
		 $Role=$this->session->userdata('Role');
		 $empid=$this->session->userdata('Emp_ID');
		 $query="where  Emp_ID=".$empid;
		  
		  if ($Role==3 || $name!="" || $from!="")
    { 

$query="where (ContractID='".$name."' or ContractID in (SELECT ContractID FROM tbl_contract WHERE CompanyName LIKE '%$name%'))  or (date(Created) BETWEEN '$from' AND '$to') and Emp_ID=".$empid ;
		}
		//echo "SELECT * FROM `tbl_payment` $query";
 $query=$this->db->query("SELECT * FROM `tbl_payment` $query");
return $query->result();
}
public function getpackage($ID){
        $this->db->where('ID',$ID);
        $query = $this->db->get('tbl_package');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }
	  public function getbilling($ID){
        $this->db->where('ContractID',$ID);
		 $this->db->where('Payment_Status',0);
        $query = $this->db->get('tbl_payment');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
     }

public function geMeById($area) {
	
$query=$this->db->query("SELECT * FROM `tbl_admin` where Emp_ID IN (SELECT ME_id FROM `tbl_meallocation` WHERE Area=$area)");
return $query->result();
}
public function geMetimeById($me,$medate) {
	
$query=$this->db->query("SELECT Appo_time FROM `tbl_appointment_me` where MeId='".$me."' and Appo_date='".$medate."'");
return $query->result();
}

}















