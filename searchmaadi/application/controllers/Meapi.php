<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: application/json');
class User extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('adminmod');
		$this->load->model('usermod');
	}
		
public function login(){
	
$data = array(
'Emp_ID' => $this->input->get('empid'),
'Password' => md5(trim($this->input->get('password')))
);
$result = $this->adminmod->login($data);

	
if ($result) {

	$session_data = array(
//'username' => $result[0]->username,
'userid' => $result[0]->ID,
'Emp_ID' => $result[0]->Emp_ID,
'Name' => $result[0]->Name,
'Role' => $result[0]->Role,		
);
 echo json_encode(array("status"=>'Success',"Msg"=>$session_data));	
}
else {
//$data['msg'] = 'Invalid Username or Password';
echo json_encode(array("status"=>'Failed',"Msg"=>'Invalid Username or Password'));	

}
}

	public function save_contract(){
	 	
		$contract = array();
		if (isset($_POST['to_web'])) {
			$status=1;
		}
		if (isset($_POST['to_quality'])) {
			$status=2;
		}
			if ($this->input->post('number2')!="") {
			$contract['Number2']     =	json_encode($this->input->post('number2'));
		}
		if ($this->input->post('landline2')!="") {
			$contract['SecondaryLandline']     =	json_encode($this->input->post('landline2'));
		}
		if ($this->input->post('email2')!="") {
			$contract['SecondaryEmail']     =	json_encode( $this->input->post('email2'));
		}
		//echo $this->input->post('id');
		$contract['ContractID']               =	trim($this->input->post('contractid'));
			if (!isset($_POST['id']) && $this->input->post('id') =="" && $this->input->post('contractid') ==""){
			$data1= array(
                                'Name' => trim($this->input->post('name')),
                                 'Emp_ID' => $this->input->post('email'),
                                 // 'Number' => $this->input->post('number'),
                                  'Password' => md5($this->input->post('number')),
                                  'Role' =>9                          
                                );
								
								
                       //UserID
                        $insertId = $this->adminmod->insertCSV($data1);	
						$contract['UserID']               =	$insertId ;
						$arrcat=$this->adminmod->getLocById($this->input->post('city'));
		   		 
			 $contract['ContractID']               =	"KAR".$arrcat->STDCode.date('dmY').rand(0,100000);
						
			}						
       
		// if($insertId!=false)
		 //{
			
        $contract['CompanyName']               =	trim($this->input->post('name'));
		 $contract['Name_slug']  = str_replace(' ', '_', $this->input->post('name'));
		$contract['Tagline']               =	trim($this->input->post('tagline'));
        $contract['Category']               =	trim($this->input->post('category'));
		$contract['SubCategory']               =json_encode($this->input->post('subcategory'));
		 $contract['City']               =	trim($this->input->post('city'));
		$contract['STDcode']               ='NA';
        $contract['Pincode']               =	trim($this->input->post('pincode'));
		$contract['AreaName']               =	trim($this->input->post('area'));
        $contract['Landmark']               =	'NA';
		$contract['ContactPerson']               =	trim($this->input->post('person'));
		 $contract['Designation']               =	trim($this->input->post('designation'));
        $contract['Email']               =	$this->input->post('email');
		$contract['Number']               =	trim($this->input->post('number'));
        $contract['Landline']               =	trim($this->input->post('landline'));
		$contract['Web']               =	trim($this->input->post('web'));
        $contract['Role']               =	$this->session->userdata('Role');
		$contract['PaymentType']               =	json_encode($this->input->post('payment'));
        $contract['Fax']               =	trim($this->input->post('fax'));
		$contract['Address']               =	trim($this->input->post('address'));
        $contract['Time']               =	json_encode($this->input->post('time'));
		$contract['Workingday']               =	json_encode($this->input->post('workday'));
        $contract['Facility']               =	json_encode($this->input->post('facility'));
		$contract['Language']               =	trim($this->input->post('language'));
        
		$contract['EmpID']               =	$this->session->userdata('Emp_ID');
        $contract['Tollfree']               =	trim($this->input->post('tollfree'));
		//$contract['Status']               =	$status;
		$contract['flag']               =	$status;
		//$this->db->insert('product', $data);  
		//var_dump($contract);
		if ( !isset($_POST['id']) && $this->input->post('id') ==""){
		 $savecon=$this->adminmod->savecontract($contract);  
		  if($savecon==true)
    {
		foreach ($this->input->post('subcategory') as $key ) {
			$cat=array();
			$cat['Cat']               =$key;
			$cat['ContractID']=$contract['ContractID'];
			$this->db->insert('tbl_client_cat', $cat);
		}
		foreach ($this->input->post('area') as $key ) {
			$area=array();
			$area['Area']               =$key;
			$area['ContractID']=$contract['ContractID'];
			$this->db->insert('tbl_client_location', $cat);
		}
			$ToEmail = 'info@searchmaadi.com'; 
    $EmailSubject = 'Account Creation as Company Owner'; 
    $mailheader = "From: From: SearchMaadi Team <info@searchmaadi.com> \r\n"; 
    $mailheader .= "Reply-To: no-reply \r\n"; 
    $mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
    $MESSAGE_BODY = '<table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color:#ffffff;" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
				<tbody>
					<tr>
						<td align="center" style="padding:5px;border:3px solid #ed2124;vertical-align:top; background-color:#7ba714;" valign="top"><img alt="" data-pin-nopin="true" height="100" src="'.base_url().'/dash/img/logo.png" width="193" /></td>
					</tr>
					<tr>
						<td style="padding:15px; vertical-align:top; background-color:#fee146; min-height:400px;" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td style="border:0px solid #253A40; background:#fff; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; line-height:18px;">
									<p style="font-size:14px;">Hi '.$contract['ContactPerson'] .',</p>

									<p><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;"><b>Welcome to&nbsp;</b></span><span style="color: rgb(238, 34, 36); font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 18px; background-color: rgb(255, 255, 255);"><b>THE SEARCHMAADI FAMILY.</b></span><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">.</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									&nbsp;</p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Your Account has been created as a Company owner. Please click on below link. Your Webpage and login credential is as follows:</span></span></p>
									&nbsp;
<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Your WebPage Link : '.base_url().'home/</span></span></p>
									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Admin Panel Link : '.base_url().'admin/login/'.$contract['Name_slug'].'</span></span></p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Email : '.$contract['Email'] .'</span></span></p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Password : '.$contract['Number'].'</span></span></p>
									<span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Please use these login credentials to review your Company details and Visitor summary on daily basis. We are just a call away to assist you however feel free to update your contact details or Any changes/updates in your Company  using this login in your admin panel. </span></span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									<br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									<span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">follow us now on <a href="https://m.facebook.com/nammasearchmaadi/?ref=bookmarks">Facebook</a>, <a href="https://twitter.com/nammsearchmaadi/status/941275649582362625">Twitter</a>,<a href="https://youtu.be/NZFfo125r44">Youtube</a> and <a href="https://www.instagram.com/p/Bcrs17XgYKo">Instagram</a></span><br />
									&nbsp;
									<p><span style="font-size:14px;"><b>Best regards,</b><br />
									<b>The SearchMaadi team</b><br />
									<b>Email:</b> support@searchmaadi.com<br />
									<b>Phone:</b> +91 9853328593</span></p>
									<span style="font-size:14px;"><a href=" http:="searchmaadi.com"> </a></span></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" height="7" style="background:#7ba714; " valign="middle"><span style="font-size:14px;"><a href="searchmaadi.com">&nbsp;</a></span></td>
					</tr>
					<tr>
						<td align="center" style="background:#ed2124;border-top:1px solid #ffffff;padding:10px 0; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#ffffff;" valign="middle"><span style="font-size:14px;">&copy; 2017. SEARCHMAADI. All Rights Reserved.</span></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
'; 
    
 
    $mail=mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure"); 
	if($mail)
    {
						  echo json_encode(array("status"=>'Success',"Msg"=>'Contract details has been Added successfully! and details set to mail'));	
    }
	}
	else{
		 echo json_encode(array("status"=>'Failed',"Msg"=>'Some error !Please try again'));	
	}
		}
    else{
		$id=$this->input->post('id');
		 $updatecon=$this->adminmod->updatecontarct($contract,$id);  
		 if($updatecon==true)
    {
   //echo json_encode(array("status"=>'Failed',"Msg"=>''));	
    echo json_encode(array("status"=>'Success',"Msg"=>'Contract details has been updated successfully!'));
    }
	else{
		 echo json_encode(array("status"=>'Failed',"Msg"=>'Some error !Please try again'));	
	}
	}
		 //}
		
	}
		function category() {
	$services = array();
     
         $categories = $this->db->get_where('tbl_category',array('Parent'=>0))->result_array();
                        foreach($categories as $row){
                         $services1 = array();
        
                         $subs = $this->db->get_where('tbl_category',array('Parent'=>$row['ID']))->result_array();
                             foreach ($subs as $row2)
{
	
$services1 [] = array('Sub_Category_ID' =>$row2['ID'],'Sub_CategoryName' =>$row2['Name']);

}

                        
$services[]  =array('Category_ID' =>$row['ID'],'CategoryName' =>$row['Name'],'Subcategory' =>$services1);
                        }
        echo json_encode(array("status"=>'Success',"Category"=>$services));	
    }
function location() {
	$location = array();
     
         $categories = $this->db->get_where('tbl_location',array('flag'=>0))->result_array();
                        foreach($categories as $row){
                         $area = array();
        
                         $subs = $this->db->get_where('tbl_location',array('Parent'=>$row['ID']))->result_array();
                             foreach ($subs as $row2)
{
	
$area [] = array('Area_ID' =>$row2['ID'],'AreaName' =>$row2['Name'],'Pincode' =>$row2['Zip']);

}

                        
$location[]  =array('City_ID' =>$row['ID'],'CityName' =>$row['Name'],'Area' =>$area);
                        }
        echo json_encode(array("status"=>'Success',"Location"=>$location));	
    }
function facilities() {
	$services = array();
     
         $categories = $this->db->get_where('tbl_category',array('Parent'=>0))->result_array();
                        foreach($categories as $row){
                         $facilities = array();
        
                         $subs = $this->db->get_where('tbl_facilities',array('Category'=>$row['ID']))->result_array();
                             foreach ($subs as $row2)
{
	
$facilities [] = array('FacilitiesId' =>$row2['ID'],'FacilitiesName' =>$row2['Name']);

}

                        
$services[]  =array('Category_ID' =>$row['ID'],'CategoryName' =>$row['Name'],'Facilities' =>$facilities);
                        }
        echo json_encode(array("status"=>'Success',"Category"=>$services));	
    }
	
}
