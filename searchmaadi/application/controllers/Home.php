<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model('frontmod');
		$this->load->library('session');
		$this->session->userdata("logged_user");
		$this->session->userdata("set_user");
		$this->load->library('user_agent');
		$this->load->library('cart');

	}
	

	public function index()
	{
		$this->load->view('index');
	}


	          }
