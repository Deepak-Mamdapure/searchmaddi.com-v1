<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('adminmod');
		$this->load->model('reportmod');
		$this->load->model('iromod');
		$this->load->library('session');
		$this->session->userdata("logged_admin");
		$this->session->userdata("agent_city");
		//$this->session->userdata("set_user");
		$this->load->library('user_agent');
		$this->load->library('cart');	
	}
	

	public function index()
	{
			
		 if (!$this->session->userdata('Emp_ID'))
    { 

 $this->load->view('admin/login');
		}else{
			
		$Role=$this->session->userdata('Role');
		
		//sending to iro module
		
		$data['LoginTime']=$this->session->userdata('logintime');
		$data['LoginDate']=$this->session->userdata('logindate');
		$data['Role']=$this->session->userdata('Role');
		$data['EmpID']=$this->session->userdata('Emp_ID');
		$data['agentloginresult'] = $this->iromod->confirmlogin($data);
		
		//print_r($data['agentloginresult']);exit;
		if($data['agentloginresult'] == false){
			$arr = array(
			'Emp_ID' => $data['EmpID'],
			'Role' => $data['Role'],
			'LoginTime' => $data['LoginTime'],
			'Date' => $data['LoginDate'],
			'Status' => '0',
			);
			
		$this->iromod->addlogintime($arr,$id = '');
		if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Successfully Logged In');
	                      //window.location='".base_url()."admin/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      //window.location='".base_url()."admin/';
	                      </script>";
					}
			
		}else{
			//query
		}
		
		/*
	 if ($Role==9){
							 $data['mess'] ="Company";
 $user=$this->session->userdata('userid');							 
						 }
						 */
		$data['totalvisitor'] = $this->adminmod->dashmod($Role);
		$data['visitor'] = $this->adminmod->dashvisitior($Role);
	$this->load->view('admin/home',$data);
		}
		//$this->load->view('admin/login');
	}
	/*
public function login()
	{
		//$this->load->view('admin/home');
		$this->load->view('admin/login');
	}
*/
	//login
	
public function savedisposition(){
		$data=array();
		$data['ContractID']  =	"KAR".$this->input->post('stdcode').date('dmY').rand(0,100000);
		$data['Category'] = $this->input->post('category');
						$data['CompanyName'] = $this->input->post('name');
						$data['Tagline'] = $this->input->post('tagline');
						$data['ContactPerson'] = $this->input->post('person');
						$data['Designation'] = $this->input->post('designation');
						$data['Address'] = $this->input->post('address');
						$data['City'] = $this->input->post('city');
						$data['AreaName'] = $this->input->post('area');
						$data['Email'] = $this->input->post('email');
						$data['Pincode'] = $this->input->post('pincode');
						$data['Number'] = $this->input->post('number');
						$data['Disposition'] = $this->input->post('disposition');
						$data['DisDate'] = $this->input->post('date');
						$data['DisTime'] = $this->input->post('time');
						$data['DisFeedback'] = $this->input->post('feedback');
						$data['Landmark'] = $this->input->post('landmark');
						$data['STDcode'] = $this->input->post('stdcode');
						$data['Landline'] = $this->input->post('landline');
						$data['Web'] = $this->input->post('website');
						$data['Fax'] = $this->input->post('fax');
						$data['SubCategory'] = $this->input->post('subctg');
						$data['Time'] = $this->input->post('starttime');
						
						$data['EndTime'] = $this->input->post('endtime');
						$data['Language'] = $this->input->post('language');
						$data['Tollfree'] = $this->input->post('tollfree');
						$data['AboutCompany'] = $this->input->post('aboutcompany');
						
						$this->db->insert('tbl_contract',$data);
						if ($this->db->affected_rows() > 0) {
							$me=array();
							$me['ContractID']  =	"KAR".$this->input->post('stdcode').date('dmY').rand(0,100000);
							$me['MeId'] = $this->input->post('mename');
		$me['Appo_date'] = $this->input->post('medate');
		$me['Appo_time'] = $this->input->post('metime');
	    $me['Feedback'] = $this->input->post('mefeedback');
		$this->db->insert(' tbl_appointment_me',$me);
								  echo "Disposition added";
								  
							  }else{
								 echo "Some error !Please try again";
							}
						
	}
public function login(){

$data = array(
'Emp_ID' => $this->input->post('empid'),
'Password' => md5(trim($this->input->post('password')))
);



$result = $this->adminmod->login($data);

$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
	
if ($result) {

	$session_data = array(
//'username' => $result[0]->username,
'userid' => $result[0]->ID,
'Emp_ID' => $result[0]->Emp_ID,
'Name' => $result[0]->Name,
'Role' => $result[0]->Role,	
'logintime' => $date->format( 'H:m:s'),	
'logindate' => $date->format( 'y-m-d' ) 	
	
);

// Add user data in session
$this->session->set_userdata($session_data);

//$url= basename($this->agent->referrer());
redirect(base_url().'admin');
}
else {
$data['msg'] = 'Invalid Username or Password';
$this->load->view('admin/login', $data);
}
}


//logout
public function logout()
    {
        $this->session->sess_destroy();
       $timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
		$data['id']= $this->input->post('id');
		if($data['id'] == ''){
			
			$data['Role']=$this->session->userdata('Role');
			$data['EmpID']=$this->session->userdata('Emp_ID');
			$data['agentloginresult'] = $this->iromod->confirmlogin($data);
			
		if($data['agentloginresult'] != false){
			$arr = array(
			'LogoutTime' => $date->format( 'H:m:s' ),
			'Status' => '1'
			);
			$this->iromod->addlogintime($arr,$data['agentloginresult']->ID);
			if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Successfully Logged Out');
	                     </script>";
						  redirect(base_url().'admin/login');
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      </script>";
					}	
					redirect(base_url().'admin/login');
		}
		}else{
			
			$arr = array(
			'LogoutTime' => $date->format( 'H:m:s' ),
			'Status' => '1'
			);
			$this->iromod->addlogintime($arr,$data['id']);
			if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Successfully Logged Out');
	                     </script>";
						  //redirect(base_url().'admin/login');
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      </script>";
					}
		}
    }
	
	
	/* Category start*/
	public function addcategory()
	{
		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	$this->load->view('admin/addcategory');
		//$this->load->view('admin/login');
	}
	public function storecategory()
	                  {
	                  		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}

	                  	
						if ($_FILES['smallimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['smallimage']['name'];
	                  		$file_size =$_FILES['smallimage']['size'];
	                  		$file_tmp =$_FILES['smallimage']['tmp_name'];
	                  		$file_type=$_FILES['smallimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/cat/small/".uniqid();

	                  	 $smallimagelink=$path.".".end($file_name);
	  //$file_ext=strtolower(end(explode('.',$_FILES['userfile']['name'])));
	   //$expensions= array("jpeg","jpg","png","psd");
      /*
      if(in_array($file_ext,$expensions)=== false ){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
	  */
      
      if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }

     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	   $smallimagelink=$this->input->post('smallimageupdate');               		
	                 		
	}
	if ($_FILES['bigimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['bigimage']['name'];
	                  		$file_size =$_FILES['bigimage']['size'];
	                  		$file_tmp =$_FILES['bigimage']['tmp_name'];
	                  		$file_type=$_FILES['bigimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/cat/big/".uniqid();

	                  	 $bigimagelink=$path.".".end($file_name);
						 
						 if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }
	 
     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	              		
	   $bigimagelink=$this->input->post('bigimageupdate');               		
	}

     $data = array(
     	'Name' => $this->input->post('name'),
     	'Kannada' => $this->input->post('kannada'),
		'Description' => $this->input->post('description'),
     	'BigImage' => $smallimagelink,
     	'SmallImage' => $bigimagelink,
		'Type' => $this->input->post('subcattype')
		

     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_category', $data);  
		   if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Category added successfuly');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addcategory/';
	                      </script>";
					}
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_category', $data);    
}
					  
					 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Category Updated successfuly');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addcategory/';
	                      </script>";
					}						 
		//$this->load->view('admin/addbrand',$data);


	                  }
 public function storesubcategory()
	                  {
						 	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
     $data = array(
     	'Name' => $this->input->post('name'),
     	'Kannada' => $this->input->post('kannada'),
     	'Parent' => $this->input->post('cat'),
     	'Type' => $this->input->post('subcattype')

     	);
		
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_category', $data);  
		  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Sub Category added successfuly');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addcategory/';
	                      </script>";
					}
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_category', $data);    
}
					  
					 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Sub Category Updated successfuly');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('No changes made');
	                      window.location='".base_url()."admin/addcategory/';
	                      </script>";
					}						 
		//$this->load->view('admin/addbrand',$data);


	                  }
 public function categoryinfo()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	                	 $data['cat'] = $this->adminmod->cat_mod();
		$this->load->view('admin/categoryinfo',$data);

	                }
public function subcategoryinfo($id)
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	                	 $data['cat'] = $this->adminmod->subcat_mod($id);
		$this->load->view('admin/subcategoryinfo',$data);

	                }
					
					public function editcat($id)
	 {
	 	$data['cat'] = $this->adminmod->getCatById($id);
	 	$this->load->view('admin/addcategory',$data);
		
	 }
	 	public function editsubcat($id)
	 {
	 	$data['subcat'] = $this->adminmod->getCatById($id);
	 	$this->load->view('admin/addcategory',$data);
		
	 }
	 public function deletecat($id){
      	$result = $this->adminmod->deletecat($id);
      if($result==true)
		{
      	//redirect(base_url().'admin/categoryinfo');
		echo "<script language='javascript'>window.alert('Category Deleted successfuly');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
		}
		else
		{
			echo "<script language='javascript'>window.alert('Some error! please try again');
	                      window.location='".base_url()."admin/categoryinfo/';
	                      </script>";
		}
      	}
					/* Category End*/
						
	/* Location start*/
	public function addlocation()
	{
		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	$this->load->view('admin/addlocation');
		//$this->load->view('admin/login');
	}
	public function storelocation()
	                  {
	                  		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}

	                  	
				if ($_FILES['smallimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['smallimage']['name'];
	                  		$file_size =$_FILES['smallimage']['size'];
	                  		$file_tmp =$_FILES['smallimage']['tmp_name'];
	                  		$file_type=$_FILES['smallimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/location/small".uniqid();

	                  	 $smallimagelink=$path.".".end($file_name);
	  //$file_ext=strtolower(end(explode('.',$_FILES['userfile']['name'])));
	   //$expensions= array("jpeg","jpg","png","psd");
      /*
      if(in_array($file_ext,$expensions)=== false ){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
	  */
      
      if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }

     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	   $smallimagelink=$this->input->post('smallimageupdate');               		
	                 		
	}
	if ($_FILES['bigimage']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['bigimage']['name'];
	                  		$file_size =$_FILES['bigimage']['size'];
	                  		$file_tmp =$_FILES['bigimage']['tmp_name'];
	                  		$file_type=$_FILES['bigimage']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/location/big".uniqid();

	                  	 $bigimagelink=$path.".".end($file_name);
						 
						 if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }
	 
     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	              		
	   $bigimagelink=$this->input->post('bigimageupdate');               		
	}


     $data = array(
     	'Name' => $this->input->post('name'),
     	'Kannada' => $this->input->post('kannada'),
     	'STDCode' => $this->input->post('stdcode'),
     	'Description' => $this->input->post('description'),
     	'BigImage' => $smallimagelink,
     	'SmallImage' => $bigimagelink,

     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_location', $data); 
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Location added successfuly');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addlocation/';
	                      </script>";
					}			  
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_location', $data);    
 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Location Updated successfuly');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('No change done');
	                      window.location='".base_url()."admin/addlocation/';
	                      </script>";
					}	
}
					
	                  }
 public function storearea()
	                  {
	                  		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
     $data = array(
     	'Name' => $this->input->post('name'),
     	'Kannada' => $this->input->post('kannada'),
		'Zip' => $this->input->post('zip'),
     	'Parent' => $this->input->post('location')

     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_location', $data);  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Area added successfuly');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addlocation/';
	                      </script>";
					}
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_location', $data);    
 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Area Updated successfuly');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/addlocation/';
	                      </script>";
					}
}
					  
					 						 
		//$this->load->view('admin/addbrand',$data);


	                  }
 public function locationinfo()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	                	 $data['location'] = $this->adminmod->location_mod();
		$this->load->view('admin/locationinfo',$data);

	                }
public function areainfo($id)
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	                	 $data['cat'] = $this->adminmod->area_mod($id);
		$this->load->view('admin/areainfo',$data);

	                }
					public function editlocation($id)
	 {
	 	$data['location'] = $this->adminmod->getLocById($id);
	 	$this->load->view('admin/addlocation',$data);
	 }
	 	public function editarea($id)
	 {
	 	$data['area'] = $this->adminmod->getLocById($id);
		//print_r($data);exit;
	 	$this->load->view('admin/addlocation',$data);
	 }
	  public function deletelocation($id){
      	$result = $this->adminmod->deleteloc($id);
		if($result==true)
		{
      	//redirect(base_url().'admin/categoryinfo');
		echo "<script language='javascript'>window.alert('Loction Deleted successfuly');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
		}
		else
		{
			echo "<script language='javascript'>window.alert('Some error! please try again');
	                      window.location='".base_url()."admin/locationinfo/';
	                      </script>";
		}
      	}
					/* Location End*/
					public function uploadcontract()
	{
		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	$this->load->view('admin/uploadcontract');
		//$this->load->view('admin/login');
	}
				

public function contractimport()
        {
			
   if($_FILES["file"]["size"] > 0)
                  {
					  $role=$this->input->post('role');
                  $filename=$_FILES["file"]["tmp_name"];
                    $file = fopen($filename, "r");
                    $count = 0;
                     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                     {
                        $count++;          

					  // add this line
//$emapData= array_map("utf8_encode", $emapData);
//var_dump($emapData);
    if($count>1){
		
   //echo  utf8_encode($emapData[6]);
   /*
                         $data1= array(
                                'Name' => strtolower($emapData[2]),
                                 'Email' => strtolower($emapData[3]),
                                  'Number' => strtolower($emapData[4]),
                                  'Password' => md5($emapData[4]),
                                  'Type' =>2                           
                                );
								*/
								$data1= array(
                                'Name' => strtolower($emapData[2]),
                                 'Emp_ID' => strtolower($emapData[3]),
                                 // 'Number' => $this->input->post('number'),
                                  'Password' => md5($emapData[4]),
                                  'Role' =>9                          
                                );
								
								
								//echo date('dmY');
								
                       //UserID
                        $insertId = $this->adminmod->insertCSV($data1);
					
                        if($insertId==false)
                        {
                        $userid= $this->adminmod->getuserid($data1);
                        //var_dump($userid);
                          $insertId=$userid[0]->ID;
                        }
						$contractid="KAR".$emapData[22].date('dmY').rand(0,100000);
						$zip=$emapData[24];
  $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&amp;sensor=false";
  $result_string = file_get_contents($url);
  $result = json_decode($result_string, true);
  $result1[]=$result['results'][0];
  $result2[]=$result1[0]['geometry'];
  $result3[]=$result2[0]['location'];
$getVal=$result3[0];

$contract['Lat']               =	$getVal['lat'];
        $contract['Lang']               =	$getVal['lng'];
                            $data = array(
                                'ContractID' => $contractid,
                               'CompanyName' => $emapData[0],
                                'Tagline' => $emapData[1],
								'ContactPerson' => $emapData[2],
								'Designation' => $emapData[3],
								 'Email' => $emapData[4],
								 'SecondaryEmail' => $emapData[5],
								'SecondaryEmail2' => $emapData[6],
								'SecondaryEmail3' => $emapData[7],
								'SecondaryEmail4' => $emapData[8],
								 'Number' => $emapData[9],
								 'Number2' => $emapData[10],
								'Number2' => $emapData[11],
								'Number3' => $emapData[12],
								'Number4' => $emapData[13],
								 'Landline' => $emapData[14],
								 'SecondaryLandline' => $emapData[15],
								 'SecondaryLandline2' => $emapData[16],
                                'SecondaryLandline3' => $emapData[17],
                                'SecondaryLandline4' => $emapData[18],	
                                'Category' => $emapData[19],
                                'SubCategory' => $emapData[20],
                                'City' => $emapData[21],
                                'STDcode' => $emapData[22],
								'AreaName' => $emapData[23],
                                'Pincode' => $emapData[24],
								'Address' => $emapData[25],
                                'Landmark' => $emapData[26],
                                'Web' => $emapData[27],
                                 'PaymentType' => $emapData[28],
                                'Workingday' => $emapData[29],
                                'StartTime' => $emapData[30],
                                'EndTime' => $emapData[31],
								  'Facility' => $emapData[32],
                                 'Language' => $emapData[33],
                                'Tollfree' => $emapData[34],
                                'Fax' => $emapData[35],
                                 'Role' => $role,
								'UserID' => $insertId,
								'Lat'     =>	$getVal['lat'],
							    'Lang'    =>	$getVal['lng']
                                
                                );
                               // var_dump($data);
                                 $result= $this->adminmod->importcontract($data);
								
		 foreach (explode(', ', $emapData[20]) as $key ) {
			$cat=array();
			$cat['Cat']  =str_replace(' ', '-', $key);
			$cat['CatTag']=$key;
			$cat['ContractID']=$contractid;
			$this->db->insert('tbl_client_cat', $cat);
		}
		$area=array();
			$area['Area']               =str_replace(' ', '_', $emapData[23]);
			$area['AreaTag']               = $emapData[23];
			$area['ContractID']=$contract['ContractID'];
			$this->db->insert('tbl_client_location', $area);  
							
                                 
                     }
					  
                     }
					
                    fclose($file);
				
                  if ($result == TRUE) {
		echo "<script language='javascript'>window.alert('Contract Details Added Successfully!');
	window.location='".base_url()."/admin/uploadcontract';
	</script>";
		}
		else
		{
		echo "<script language='javascript'>window.alert('Some Error!Try again');
	window.location='".base_url()."/admin/uploadcontract';
	</script>";
		}
                  }
                  else
                  {
                  echo "<script language='javascript'>window.alert('Invalid File:Please Upload CSV File!');
	window.location='".base_url()."/admin/uploadcontract';
	</script>";

	
                  }
                
                  
            }
			public function tmeinfo($id)
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	                	 $data['tme'] = $this->adminmod->tme_mod($id);
						 if($id==3)
						 {
							$data['mess'] ="TME"; 
						 }
						else if ($id==4){
							 $data['mess'] ="ME"; 
						 }
						else if ($id==5){
							 $data['mess'] ="Quality"; 
						 }
						else if ($id==6){
							 $data['mess'] ="TECH"; 
						 }
						else if ($id==7){
							 $data['mess'] ="Direct"; 
						 }
						  else{
							 $data['mess'] ="All";
						 }
		$this->load->view('admin/tmeinfo',$data);

	                }
				public function newbusiness()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
		/*
		$Role=$this->session->userdata('Role');
	
						 if($Role==3)
						 {
							$data['mess'] ="TME"; 
							$user=$this->session->userdata('Emp_ID');
						 }
						else if ($Role==4){
							 $data['mess'] ="ME"; 
							 $user=$this->session->userdata('Emp_ID');
						 }
						else if ($Role==5){
							 $data['mess'] ="Quality"; 
							 $user=$this->session->userdata('Emp_ID');
						 }
						else if ($Role==6){
							 $data['mess'] ="TECH"; 
							 $user=$this->session->userdata('Emp_ID');
						 }
						else if ($Role==7){
							 $data['mess'] ="Direct"; 
							 $user=$this->session->userdata('Emp_ID');
						 }
						 else if ($Role==9){
							 $data['mess'] ="Company";
 $user=$this->session->userdata('userid');							 
						 }
						  else{
							 $data['mess'] ="All";
							 $user=$this->session->userdata('Emp_ID');
						 }
						 $data['tme'] = $this->adminmod->allroledata($user,$Role);
		$this->load->view('admin/newbusiness',$data);
		*/
		
			$flagid = $this->input->post('contract');
				 $data['flagid'] = $flagid;
			//echo $flagid;
	   		$data['contractlist'] = $this->reportmod->newcontractlist($flagid);
			//var_dump($data['contractlist']);
		$this->load->view('admin/newbusiness',$data);

	                }
					public function irototme()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
	
			//$flagid = $this->input->post('contract');
				// $data['flagid'] = $flagid;
			//echo $flagid;
	   		$data['contractlist'] = $this->reportmod->irototme();
			//var_dump($data['contractlist']);
		$this->load->view('admin/irototme',$data);

	                }
    public function allocated($cityid='')
	 {
		           	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		} 
		 if($this->session->userdata('agent_city') == false  && isset($_POST['city']) ) 
	{
		// $this->session->unset_userdata('agent_city');
  $city=$this->input->post('city');
$this->session->set_userdata('agent_city',$city);
  }
  else if($this->session->userdata('agent_city') == true && isset($_POST['city']))
  {
	  $this->session->unset_userdata('agent_city');
  $city=$this->input->post('city');
$this->session->set_userdata('agent_city',$city);
  }
  else{
	   $this->session->unset_userdata('agent_city');
		$city=38;
$this->session->set_userdata('agent_city',$city);
  }
		
 

		 // $arrcat=$this->adminmod->getLocById($city);
		   
		  //$data['contractid']="KAR".$arrcat->STDCode.date('dmY').rand(0,100000);
	 	//$data['location'] = $this->adminmod->getLocById($id);
	 	$this->load->view('admin/allocated');
		
	 }
	 public function addcontract()
	 {
		            	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
		if($this->session->userdata('agent_city') == true) {
   $city = $this->session->userdata('agent_city');
   }
		 // $arrcat=$this->adminmod->getLocById($city);
		   		//  $data['contractid']="KAR".$arrcat->STDCode.date('dmY').rand(0,100000);
	 	//$data['cityname'] = $arrcat->Name;
		//$data['stdcode'] = $arrcat->STDCode;
	 	$this->load->view('admin/addcontract');
			 }
			 /* start area,pincode and category */
	 public function ajax_call_pin(){
		if (isset($_POST) && isset($_POST['area'])) {
                   $area= $_POST['area'];
                       // echo "alert('error')";
               
          $arrcat=$this->adminmod->getLocById($area);
             // var_dump($arrcat);
                //$arrstates[$states->state] = $states->state;
               echo $arrcat->Zip ;
			   //'<input type="text" name="pincode" id="txt9" value="'.$arrcat->Zip.'"  class="form-control area">';
               // echo '<option value="'.$area->Zip.'">'.$cat->name.'</option>';
              }
	}
	 public function ajax_call_me(){
		
		if (isset($_POST) && isset($_POST['area'])) {
                  $area= $_POST['area'];
                       // echo "alert('error')";
               
          $arrcat=$this->adminmod->geMeById($area);
		 // echo '<option value="">Select ME </option>';
		  echo '<option value=""> Select area</option>';
              foreach ($arrcat as $emp) {
                //$arrstates[$states->state] = $states->state;
                echo '<option value="'.$emp->Emp_ID.'">'.$emp->Name.'('.$emp->Emp_ID.')</option>';
            }
            
             }
			
	}
	 public function ajax_call_metime(){
		
		if (isset($_POST) && isset($_POST['me'])) {
                   $me= $_POST['me'];
				   $medate= $_POST['medate'];
                       // echo "alert('error')";
              //echo "SELECT Appo_time FROM `tbl_appointment_me` where MeId='".$me."' and Appo_date='".$medate."'";
			  
          $arrme=$this->adminmod->geMetimeById($me,$medate);
		// var_dump($arrme);
		 // echo '<option value="">Select ME </option>';
            
                   foreach ($arrme as $emp) {
                //$arrstates[$states->state] = $states->state;
               $a[]=$emp->Appo_time;
            }
			//echo '<option value="'.$emp->Emp_ID.'">'.$emp->Name.'('.$emp->Emp_ID.')</option>';
				echo '<option value="00" ' ;if(in_array("00", $a)){ echo "disabled";} echo '>00</option>
			<option value="01:00"'; if(in_array("01:00", $a)){ echo "disabled";} echo '>01:00</option>
			';
			echo '<option value="02:00"'; if(in_array("02:00", $a)){ echo "disabled";} echo ' >02:00</option>
			<option value="03:00"'; if(in_array("03:00", $a)){ echo "disabled";} echo '>03:00</option>
			<option value="04:00"'; if(in_array("04:00", $a)){ echo "disabled";} echo '>04:00</option>
			<option value="05:00"'; if(in_array("05:00", $a)){ echo "disabled";} echo '>05:00</option>
			<option value="06:00"'; if(in_array("06:00", $a)){ echo "disabled";} echo '>06:00</option>
			<option value="07:00"'; if(in_array("07:00", $a)){ echo "disabled";} echo '>07:00</option>
			<option value="08:00"'; if(in_array("08:00", $a)){ echo "disabled";} echo '>08:00</option>
			<option value="09:00"'; if(in_array("09:00", $a)){ echo "disabled";} echo '>09:00</option>
			<option value="10:00"'; if(in_array("10:00", $a)){ echo "disabled";} echo '>10:00</option>
			<option value="11:00"' ;if(in_array("11:00", $a)){ echo "disabled";} echo '>11:00</option>
			<option value="12:00"'; if(in_array("12:00", $a)){ echo "disabled";} echo '>12:00</option>
			<option value="13:00"'; if(in_array("13:00", $a)){ echo "disabled";} echo '>13:00</option>
			<option value="14:00"'; if(in_array("14:00", $a)){ echo "disabled";} echo '>14:00</option>
			<option value="15:0"'; if(in_array("15:00", $a)){ echo "disabled";} echo '>15:00</option>
			<option value="16:00"'; if(in_array("16:00", $a)){ echo "disabled";} echo '>16:00</option>
			<option value="17:00"'; if(in_array("17:00", $a)){ echo "disabled";} echo '>17:00</option>
			<option value="18:00"'; if(in_array("18:00", $a)){ echo "disabled";} echo '>18:00</option>
			<option value="19:00"'; if(in_array("19:00", $a)){ echo "disabled";} echo '>19:00</option>
			<option value="20:00"'; if(in_array("20:00", $a)){ echo "disabled";} echo '>20:00</option>
			<option value="21:00"'; if(in_array("21:00", $a)){ echo "disabled";} echo '>21:00</option>
			<option value="22:00"'; if(in_array("22:00", $a)){ echo "disabled";} echo '>22:00</option>
			<option value="23:00"'; if(in_array("23:00", $a)){ echo "disabled";} echo '>23:00</option>';
			 
			
	 }
           
       
			
	}
	 public function ajax_call_sub(){
		if (isset($_POST) && isset($_POST['cat'])) {
                   $cat= $_POST['cat'];
                       // echo "alert('error')";
               
                 
          $arrcat=$this->adminmod->getsubCatById($cat);
             // var_dump($arrcat);
                //$arrstates[$states->state] = $states->state;
               //echo $arrcat->Zip ;
			   //'<input type="text" name="pincode" id="txt9" value="'.$arrcat->Zip.'"  class="form-control area">';
			   
                 foreach ($arrcat as $subcat) {
                //$arrstates[$states->state] = $states->state;
               // echo '<option value="'.$subcat->ID.'">'.$subcat->Name.'</option>';
			   echo '<label class="form-checkbox form-normal form-primary form-text "><input type="checkbox"  name="subcategory1" value="'.$subcat->Name.'" >'.$subcat->Name.'</label>';
          /* echo '<label><input type="checkbox" value="football" name="subcategory" id="check"> Football</label>
        <label><input type="checkbox" value="baseball" name="subcategory" id="check"> Baseball</label>
        <label><input type="checkbox" value="cricket" name="subcategory" id="check"> Cricket</label>
        <label><input type="checkbox" value="boxing" name="subcategory" id="check"> Boxing</label>
        <label><input type="checkbox" value="racing" name="subcategory"> Racing</label>
        <label><input type="checkbox" value="swimming" name="subcategory"> Swimming</label>';
		*/
				 }
            
             }
	}
	public function ajax_call_city(){
		if (isset($_POST) && isset($_POST['city'])) {
                   $city= $_POST['city'];
                       // echo "alert('error')";
               
          $arrcity=$this->adminmod->getsubCityById($city);
             // var_dump($arrcat);
                //$arrstates[$states->state] = $states->state;
               //echo $arrcat->Zip ;
			   //'<input type="text" name="pincode" id="txt9" value="'.$arrcat->Zip.'"  class="form-control area">';
			    echo '<option value=""> Select area</option>';
                 foreach ($arrcity as $city) {
                //$arrstates[$states->state] = $states->state;
               
                echo '<option value="'.$city->ID.'">'.$city->Name.'</option>';
            }
           
            
             }
	}
	public function ajax_call_std(){
		if (isset($_POST) && isset($_POST['city'])) {
                   $city= $_POST['city'];
                       // echo "alert('error')";
             
          $arrcity=$this->adminmod->getCityById($city);
             // var_dump($arrcat);
                //$arrstates[$states->state] = $states->state;
               //echo $arrcat->Zip ;
			   //'<input type="text" name="pincode" id="txt9" value="'.$arrcat->Zip.'"  class="form-control area">';
			  
                 foreach ($arrcity as $city) {
                //$arrstates[$states->state] = $states->state;
              echo $city->STDCode;
            }
             }
	}
	/* end area,pincode and category std */
	/* start  package details std */
	public function ajax_call_package(){
		if (isset($_POST) && isset($_POST['pack'])) {
                   $pack= $_POST['pack'];
                       // echo "alert('error')";
               
          $pack=$this->adminmod->getpackById($pack);
            echo $pack->Description;
            }
           
	}
	public function ajax_call_packprice(){
		if (isset($_POST) && isset($_POST['city'])) {
                   $pack= $_POST['pack'];
				    $city= $_POST['city'];
        $pack=$this->adminmod->getpackById($pack);
             
			if($city==410)
			{
				echo $pack->Price1;
			}
			else{
				echo $pack->Price2;
			}
             }
	}
	public function ajax_call_area(){
		if (isset($_POST) && isset($_POST['city'])) {
                    $city= $_POST['city'];
					 $city ="'" . str_replace(",", "','", $city) . "'";;
                       // echo "alert('error')";
               
          $arrcity=$this->adminmod->getsubzoneById($city);
             // var_dump($arrcat);
                //$arrstates[$states->state] = $states->state;
               //echo $arrcat->Zip ;
			   //'<input type="text" name="pincode" id="txt9" value="'.$arrcat->Zip.'"  class="form-control area">';
		
			    echo '<option value=""> Select Area</option>';
                 foreach ($arrcity as $city) {
                //$arrstates[$states->state] = $states->state;
               
                echo '<option value="'.$city->ID.'">'.$city->Name.'</option>';
            }
           
        
			//var_dump($arrcity);
             }
	}
	/* end package details */
	public function save_contract(){
	 	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
		$contract = array();
		if (isset($_POST['to_web'])) {
			$status=1;
		}
		if (isset($_POST['to_quality'])) {
			$status=2;
		}
		
		if (count($this->input->post('number2'))!=0) {
			
			if (count($this->input->post('number2'))>0) {
			 $contract['Number2']     =	$this->input->post('number2')[0];
			}
			if (count($this->input->post('number2'))>1) {
			 $contract['Number3']     =	$this->input->post('number2')[1];
		}
		if (count($this->input->post('number2'))>2) {
			$contract['Number4']     =$this->input->post('number2')[2];
		}
		if (count($this->input->post('number2'))>3) {
			$contract['Number5']     =	$this->input->post('number2')[3];
		}
		}
		
		if (count($this->input->post('landline2')[0])!=0) {	
			if (count($this->input->post('landline2'))>0) {
			$contract['SecondaryLandline']     =	$this->input->post('landline2')[0];
			}
			if (count($this->input->post('landline2'))>1) {
			$contract['SecondaryLandline2']     =	$this->input->post('landline2')[1];
		}
		if (count($this->input->post('landline2'))>2) {
			$contract['SecondaryLandline3']     =$this->input->post('landline2')[2];
		}
		if (count($this->input->post('landline2'))>3) {
			$contract['SecondaryLandline4']     =	$this->input->post('landline2')[3];
		}
					
		}
		if (count($this->input->post('email2')[0])!=0) {	
			if (count($this->input->post('email2'))>0) {
			$contract['SecondaryEmail']     =	$this->input->post('email2')[0];
			}
			if (count($this->input->post('email2'))>1) {
			$contract['SecondaryEmail2']     =	$this->input->post('email2')[1];
		}
		if (count($this->input->post('email2'))>2) {
			$contract['SecondaryEmail3']     =$this->input->post('email2')[2];
		}
		if (count($this->input->post('email2'))>3) {
			$contract['SecondaryEmail4']     =	$this->input->post('email2')[3];
		}
					
		}
		
		
		//echo $this->input->post('id');
		$contract['ContractID']               =	trim($this->input->post('contractid'));
			if (!isset($_POST['id']) && $this->input->post('id') =="" && $this->input->post('contractid') ==""){
			$data1= array(
                                'Name' => trim($this->input->post('name')),
                                 'Emp_ID' => $this->input->post('email'),
                                 // 'Number' => $this->input->post('number'),
                                  'Password' => md5($this->input->post('number')),
                                  'Role' =>9                          
                                );
								
								
                       //UserID
                        $insertId = $this->adminmod->insertCSV($data1);	
						$contract['UserID']               =	$insertId ;
						$arrcat=$this->adminmod->getLocById($this->input->post('city'));
		   		 
			 $contract['ContractID']               =	"KAR".$arrcat->STDCode.date('dmY').rand(0,100000);
						
			}						
       
		// if($insertId!=false)
		 //{
			
        $contract['CompanyName']               =	trim($this->input->post('name'));
		 $contract['Name_slug']  = str_replace(' ', '-', $this->input->post('name'));
		$contract['Tagline']               =	trim($this->input->post('tagline'));
        $contract['Category']               =	trim($this->input->post('category'));
		$contract['SubCategory']               =json_encode(explode(', ', $this->input->post('subcategory1')));
		        $contract['City']               =	trim($this->input->post('city'));
		$contract['STDcode']               =trim($this->input->post('stdcode'));
        $contract['Pincode']               =	trim($this->input->post('pincode'));
		$contract['AreaName']               =	trim($this->input->post('area'));
        $contract['Landmark']               =	trim($this->input->post('landmark'));
		$contract['ContactPerson']               =	trim($this->input->post('person'));
		 $contract['Designation']               =	trim($this->input->post('designation'));
        $contract['Email']               =	$this->input->post('email');
		$contract['Number']               =	trim($this->input->post('number'));
        $contract['Landline']               =	trim($this->input->post('landline'));
		$contract['Web']               =	trim($this->input->post('web'));
        $contract['Role']               =	$this->session->userdata('Role');
		$contract['PaymentType']               =	json_encode($this->input->post('payment'));
        $contract['Fax']               =	trim($this->input->post('fax'));
		$contract['Address']               =	trim($this->input->post('address'));
        $contract['Time']               =	$this->input->post('starttime');
		 $contract['EndTime']               =	$this->input->post('endtime');
		$contract['Workingday']               =	json_encode($this->input->post('workday'));
        $contract['Facility']               =	json_encode($this->input->post('facility'));
		$contract['Language']               =	json_encode($this->input->post('language'));
		$contract['EmpID']               =	$this->session->userdata('Emp_ID');
        $contract['Tollfree']               =	trim($this->input->post('tollfree'));
		$contract['AboutCompany']               =	trim($this->input->post('aboutcompany'));
		//$contract['Status']               =	$status;
		$contract['flag']               =	$status;
		$zip=$contract['Pincode'];
  $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&amp;sensor=false";
  $result_string = file_get_contents($url);
  $result = json_decode($result_string, true);
  $result1[]=$result['results'][0];
  $result2[]=$result1[0]['geometry'];
  $result3[]=$result2[0]['location'];
$getVal=$result3[0];

$contract['lat']               =	'NA';
        $contract['Lang']               =	'NA';
		if($getVal['lat']!="")
		{
			$contract['Lang']               =	$getVal['lng'];
		}
		if($getVal['lng']!="")
		{
			$contract['Lang']               =	$getVal['lng'];
		}
		//$this->db->insert('product', $data);  
		//var_dump($contract);
		//var_dump(explode(', ', $this->input->post('subcategory1')));
		if ( !isset($_POST['id']) && $this->input->post('id') ==""){
			
		$savecon=$this->adminmod->savecontract($contract);  
		 foreach (explode(', ', $this->input->post('subcategory1')) as $key ) {
			$cat=array();
			$cat['Cat']  =str_replace(' ', '-', $key);
			$cat['CatTag']=$key;
			$cat['ContractID']=$contract['ContractID'];
			$this->db->insert('tbl_client_cat', $cat);
		}
		$area=array();
		$category= $this->db->get_where('tbl_location' , array('ID' => $contract['AreaName']))->row_array(); 

			$area['Area']    =str_replace(' ', '_',  $category['Name']);
			$area['AreaTag']   =  $category['Name'];
			$area['ContractID']=$contract['ContractID'];
			$this->db->insert('tbl_client_location', $area);
			//var_dump($contract);
			/*
			$ToEmail = 'info@searchmaadi.com'; 
    $EmailSubject = 'Account Creation as Company Owner'; 
    $mailheader = "From: From: SearchMaadi Team <info@searchmaadi.com> \r\n"; 
    $mailheader .= "Reply-To: no-reply \r\n"; 
    $mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
    $MESSAGE_BODY = '<table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color:#ffffff;" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
				<tbody>
					<tr>
						<td align="center" style="padding:5px;border:3px solid #ed2124;vertical-align:top; background-color:#7ba714;" valign="top"><img alt="" data-pin-nopin="true" height="100" src="'.base_url().'/dash/img/logo.png" width="193" /></td>
					</tr>
					<tr>
						<td style="padding:15px; vertical-align:top; background-color:#fee146; min-height:400px;" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td style="border:0px solid #253A40; background:#fff; padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; line-height:18px;">
									<p style="font-size:14px;">Hi '.$contract['ContactPerson'] .',</p>

									<p><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;"><b>Welcome to&nbsp;</b></span><span style="color: rgb(238, 34, 36); font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 18px; background-color: rgb(255, 255, 255);"><b>THE SEARCHMAADI FAMILY.</b></span><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">.</span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									&nbsp;</p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Your Account has been created as a Company owner. Please click on below link. Your Webpage and login credential is as follows:</span></span></p>
									&nbsp;
<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Your WebPage Link : '.base_url().'home/</span></span></p>
									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Admin Panel Link : '.base_url().'admin/login/'.$contract['Name_slug'].'</span></span></p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Email : '.$contract['Email'] .'</span></span></p>

									<p><span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Password : '.$contract['Number'].'</span></span></p>
									<span style="font-size:14px;"><span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">Please use these login credentials to review your Company details and Visitor summary on daily basis. We are just a call away to assist you however feel free to update your contact details or Any changes/updates in your Company  using this login in your admin panel. </span></span><br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									<br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;" />
									<span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal;">follow us now on <a href="https://m.facebook.com/nammasearchmaadi/?ref=bookmarks">Facebook</a>, <a href="https://twitter.com/nammsearchmaadi/status/941275649582362625">Twitter</a>,<a href="https://youtu.be/NZFfo125r44">Youtube</a> and <a href="https://www.instagram.com/p/Bcrs17XgYKo">Instagram</a></span><br />
									&nbsp;
									<p><span style="font-size:14px;"><b>Best regards,</b><br />
									<b>The SearchMaadi team</b><br />
									<b>Email:</b> support@searchmaadi.com<br />
									<b>Phone:</b> +91 9853328593</span></p>
									<span style="font-size:14px;"><a href=" http:="searchmaadi.com"> </a></span></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" height="7" style="background:#7ba714; " valign="middle"><span style="font-size:14px;"><a href="searchmaadi.com">&nbsp;</a></span></td>
					</tr>
					<tr>
						<td align="center" style="background:#ed2124;border-top:1px solid #ffffff;padding:10px 0; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#ffffff;" valign="middle"><span style="font-size:14px;">&copy; 2017. SEARCHMAADI. All Rights Reserved.</span></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
'; 
    
 
    $mail=mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure"); 
	if($mail)
    {
		/*
    echo "<script language='javascript'>window.alert('your request has been sent successfully! Will get back');
	                      window.location='".base_url()."admin/addcontract';
	                      </script>";
						  */
						
				
						  if ($this->db->affected_rows() > 0) {
						  redirect('/admin/makesale/'.$contract['ContractID'], 'refresh');
						  
    }

	
	
		}
    else{
		$id=$this->input->post('id');
		 $updatecon=$this->adminmod->updatecontarct($contract,$id);  
		 if($updatecon==true)
    {
    echo "<script language='javascript'>window.alert('Contract details has been updated successfully!');
	                      window.location='".base_url()."admin/newbusiness';
	                      </script>";
    }
	}
		 //}
		
	}
	
	public function editcontract($id)
	 {
	 	$data['contractinfo'] = $this->adminmod->getContractById($id);
		//var_dump($data['area']);
	 	$this->load->view('admin/addcontract',$data);
	 }
	 public function makesale($id)
	 {
	 	$data['contractinfo'] = $this->adminmod->getContractById($id);
		//var_dump($data['area']);
	 	$this->load->view('admin/makesale',$data);
	 }
	  public function savepackage()
	 {
		echo  var_dump($this->input->post('area'));
	 	//$data['contractinfo'] = $this->adminmod->getContractById($id);
		//var_dump($data['area']);
	 	//$this->load->view('admin/makesale',$data);
	 }
	 public function datareport()
	 {
		$name="";
		 if($this->input->post('name')!="")
		 {
		$name= $this->input->post('name');
		 }
		 $to="";
		 if($this->input->post('to')!="")
		 {
		$to= $this->input->post('to');
		 }
		 $from="";
		 if($this->input->post('from')!="")
		 {
		$from= $this->input->post('from');
		 }
		 if($name!="" || $to!="" || $from!="")
		 {
		 $data['tme']=$this->adminmod->datareportmod($name,$to,$from);
	 	$this->load->view('admin/datareport',$data);
		
		
		 }else{
			 $this->load->view('admin/datareport');
		 }
	//var_dump($data['tme']);
	 	
	 }
	 public function contractreport()
	 {
		
		 if($this->input->post('name')!="")
		 {
		$name= $this->input->post('name');
		
		$data['tme']=$this->adminmod->contractreportmod($name);
	 	$this->load->view('admin/contractreport',$data);
		 }else{
			 $this->load->view('admin/contractreport');
		 }
	//var_dump($data['tme']);
	 	
	 }
	  public function salereport()
	 {
		$name="";
		 if($this->input->post('name')!="")
		 {
		$name= $this->input->post('name');
		 }
		 $to="";
		 if($this->input->post('to')!="")
		 {
		$to= $this->input->post('to');
		 }
		 $from="";
		 if($this->input->post('from')!="")
		 {
		$from= $this->input->post('from');
		 }
		 $data['tme']=$this->adminmod->salereportmod($name,$to,$from);
	 	$this->load->view('admin/salereport',$data);
	//var_dump($data['tme']);
	 	
	 }
	  public function bouncereport()
	 {
		$name="";
		 if($this->input->post('name')!="")
		 {
		$name= $this->input->post('name');
		 }
		 $to="";
		 if($this->input->post('to')!="")
		 {
		$to= $this->input->post('to');
		 }
		 $from="";
		 if($this->input->post('from')!="")
		 {
		$from= $this->input->post('from');
		 }
	 	
		$data['tme']=$this->adminmod->salereportmod($name,$to,$from);
		 $this->load->view('admin/bouncereport',$data);
		 }
	
	 public function companyweb()
	 {
		         $this->load->view('admin/company');   
	          }
			  
			  public function create_pdf() {
    //============================================================+
    // File name   : example_001.php
    //
    // Description : Example 001 for TCPDF class
    //               Default Header and Footer
    //
    // Author: Muhammad Saqlain Arif
    //
    // (c) Copyright:
    //               Muhammad Saqlain Arif
    //               PHP Latest Tutorials
    //               http://www.phplatesttutorials.com/
    //               saqlain.sial@gmail.com
    //============================================================+
 
   $this->load->library("Pdf");
  
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 003');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'BI', 12);

// add a page
$pdf->AddPage();
ob_start();
    // we can have any view part here like HTML, PHP etc
   $txt = '<table style="width: 471.95pt; margin-left: 4.65pt; border-collapse: collapse;" width="629">
<tbody>
<tr style="height: 21.75pt;">
<td style="width: 471.95pt; padding: 0in 5.4pt 0in 5.4pt; height: 21.75pt;" colspan="7" width="629">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><img src="http://localhost/searchmaadi/dash/img/logo.png" alt="" width="233" height="94" /></p>
</td>
</tr>
<tr style="height: 21.75pt;">
<td style="width: 196.8pt; border: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 21.75pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">INV NO: 01</span></strong></p>
</td>
<td style="width: 63.0pt; border-top: solid windowtext 1.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 21.75pt;" width="84">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="font-size: 16.0pt; color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 48.0pt; border-top: solid windowtext 1.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 21.75pt;" width="64">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="font-size: 16.0pt; color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 164.15pt; border-top: solid windowtext 1.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 21.75pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: right; line-height: normal;"><strong><span style="color: black;">INV DATE: 10/01/2017</span></strong></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">M/s. SEARCHMAADI.COM</span></strong></p>
</td>
<td style="width: 275.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><u><span style="font-size: 12.0pt; color: black;">BILLING ADDRESS:</span></u></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Unit of :TELCON ADVERTISEMENTS Pvt. Ltd.</span></p>
</td>
<td style="width: 275.15pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">M/s. OYO MOBILES</span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Lady Curzon Road</span></p>
</td>
<td style="width: 275.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Cnt Prs: Mohammed Zakir Hussain</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Bangalore - 560 001</span></p>
</td>
<td style="width: 275.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">No:20 RT Nagar</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Karnataka</span></p>
</td>
<td style="width: 231.45pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="4" width="309">
<p style="margin-bottom: .0001pt; line-height: normal;"><u><span style="color: black;">oyomobiles@gmail.com</span></u></p>
</td>
<td style="width: 43.7pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="58">&nbsp;</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 111.0pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="148">
<p style="margin-bottom: .0001pt; line-height: normal;"><u><span style="color: black;">www.oyomobiles.com</span></u></p>
</td>
<td style="width: 120.45pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="161">&nbsp;</td>
<td style="width: 43.7pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="58">&nbsp;</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">&nbsp;</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 111.0pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="148">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Ph:9876543210</span></p>
</td>
<td style="width: 120.45pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="161">&nbsp;</td>
<td style="width: 43.7pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="58">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
<td style="width: 111.0pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="148">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Bangalore - 560 029</span></p>
</td>
<td style="width: 120.45pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="161">&nbsp;</td>
<td style="width: 43.7pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="58">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">CIN:</span></strong></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">U74994KA2017PTC107305</span></strong></p>
</td>
<td style="width: 275.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Karnataka </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">GSTIN: </span></strong></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">29AAGCT4736K1ZT</span></strong></p>
</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">&nbsp;</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">PAN:</span></strong></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">AAGCT4736K</span></strong></p>
</td>
<td style="width: 275.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="5" width="367">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">GSTIN: GST1234</span></strong></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">Sl.No</span></strong></p>
</td>
<td style="width: 218.1pt; border: solid windowtext 1.0pt; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">Description</span></strong></p>
</td>
<td style="width: 48.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="64">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">SAC</span></strong></p>
</td>
<td style="width: 164.15pt; border-top: solid windowtext 1.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">&nbsp;Amount (Rs)</span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 164.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">1</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">Swagath Package</span></strong></p>
</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; text-align: right; line-height: normal;"><span style="font-family: Times New Roman,serif; color: #333333;">998361</span></p>
</td>
<td style="width: 164.15pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">2000.00</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">web listing</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 164.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">App Listing</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 164.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Phone Enquiries</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 164.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 164.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="219">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">Taxable Value</span></strong></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;&nbsp;&nbsp;&nbsp; 2,000.00 </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">ADD: CGST @ 9%</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 180.00 </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">ADD: SGST @ 9%</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 180.00 </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">ADD: IGST @ 9%</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Total GST</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 360.00 </span></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 16.5pt;">
<td style="width: 41.7pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 16.5pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 218.1pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 16.5pt;" colspan="2" width="291">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 16.5pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 16.5pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">TOTAL VALUE</span></strong></p>
</td>
<td style="width: 65.05pt; border: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 16.5pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="font-size: 12.0pt; color: black;">&nbsp; 2,360.00 </span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">Place Opted For:</span></strong></p>
</td>
<td style="width: 63.0pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">Keyword:</span></strong></p>
</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">&nbsp;</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Zonal No: 4 </span></p>
</td>
<td style="width: 210.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="280">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Mobile Dealer, Mobile seller ,</span></p>
</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">560020;560034</span></p>
</td>
<td style="width: 210.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="280">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Mobile Repair and service</span></p>
</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 147.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="196">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 147.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="196">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 63.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="84">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 147.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="196">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><strong><span style="color: black;">Amount Chargeable (In Words):</span></strong></p>
</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 48.0pt; border: none; border-top: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; border: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 65.05pt; border-top: solid windowtext 1.0pt; border-left: none; border-bottom: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 259.8pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="3" width="346">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">Two Thousand Three Hundred and Sixty Rupees Only </span></p>
</td>
<td style="width: 48.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">&nbsp;</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 212.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="4" width="283">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">2360</span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">&nbsp;</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">&nbsp;</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">&nbsp;</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">&nbsp;</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">&nbsp;</span></strong></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 41.7pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="207">&nbsp;</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 212.15pt; border: none; border-right: solid black 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="4" width="283">
<p style="margin-bottom: .0001pt; text-align: center; line-height: normal;"><strong><span style="color: black;">&nbsp;for SEARCHMAADI.COM</span></strong></p>
</td>
</tr>
<tr style="height: 15.75pt;">
<td style="width: 41.7pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="56">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 155.1pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="207">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 63.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="84">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 48.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="64">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 99.1pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" width="132">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
<td style="width: 65.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.75pt;" colspan="2" width="87">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="color: black;">&nbsp;</span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 471.95pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="7" width="629">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 8.0pt; color: black;">Advertisement Services are under section 194C of Income Tax Act, wherein maximum TDS rate is mentioned as 2%. </span></p>
</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 406.9pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="5" width="543">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 8.0pt; color: black;">All TDS deductions, form no. 16A should be sent at tds@searchmaadi.com or mailed to our address</span></p>
</td>
<td style="width: 65.05pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">&nbsp;</td>
</tr>
<tr style="height: 15.0pt;">
<td style="width: 196.8pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="262">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 8.0pt; color: black;">TDS should not be deducted on GST</span></p>
</td>
<td style="width: 63.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="84">&nbsp;</td>
<td style="width: 48.0pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="64">&nbsp;</td>
<td style="width: 99.1pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" width="132">&nbsp;</td>
<td style="width: 65.05pt; padding: 0in 5.4pt 0in 5.4pt; height: 15.0pt;" colspan="2" width="87">&nbsp;</td>
</tr>
<tr style="height: 28.5pt;">
<td style="width: 471.95pt; padding: 0in 5.4pt 0in 5.4pt; height: 28.5pt;" colspan="7" width="629">
<p style="margin-bottom: .0001pt; line-height: normal;"><span style="font-size: 8.0pt; color: black;">Terms and Conditions are as per the "Terms of Use for Vendors/Service Providers" available at<br /> &nbsp;https.searchmaadi.com/Terms of use</span></p>
</td>
</tr>
<tr>
<td style="border: none;" width="56">&nbsp;</td>
<td style="border: none;" width="207">&nbsp;</td>
<td style="border: none;" width="86">&nbsp;</td>
<td style="border: none;" width="66">&nbsp;</td>
<td style="border: none;" width="132">&nbsp;</td>
<td style="border: none;" width="28">&nbsp;</td>
<td style="border: none;" width="58">&nbsp;</td>
</tr>
</tbody>
</table>
';
ob_end_clean();
$pdf->writeHTML($txt,true, false, false, false, '');
// set some text to print


// print a block of text using Write()
//$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

////Close and output PDF document
//$pdf->Output('example_003.pdf', 'I');
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
   $pdf->Output($_SERVER['DOCUMENT_ROOT'].'/searchmaadi/dash/'.uniqid().'.pdf', 'F');  
	//$pdf->Output("F",'dash/invoice/example_001.pdf'); 	
 
    }
public function processpayment($ID)
	 {
		 if($this->input->post('package')!="")
		 {
		 $payment = array();
		 //http://localhost/searchmaadi/admin/makesale/KAR82881201201815546
		        // $city=$this->input->post('city');
				//$area=$this->input->post('area') ;
				//$package=$this->input->post('package');
				 $package=$this->input->post('package');
				$packageprice=$this->adminmod->getpackage($package);
				 $city=$this->input->post('city');
				
				 if($package!=3)
				 {
					 if($city==410)
					 {
					$packageprice->Price1;
					 }
					 else{
						$price=  $packageprice->Price2;
					 }
		// $payment['city']  =  $city;
		 //$payment['area']  =  $this->input->post('area');
				 }
				 else{
		$division =  $this->input->post('division');
		$state  =  $this->input->post('state');
		 if(count($division)!=0)
		 {
		$price=$packageprice->Price1 * count($payment['division']);
		 }
		 if($state)
		 {
			$price= $packageprice->Price2;
		 }
				 }
		$gst=($price / 100) * 18;
		 $payment['ContractID'] =$ID;
		 $payment['Package']  =  $package;
		  $payment['Emp_ID']  =  $this->session->userdata('Emp_ID');
		  //$payment['Emp_ID']  =  $package;
		  $payment['Price']  =  $price;
		   $payment['GST']  =  $gst;
		   $payment['TotalPrice']  =  $price + $gst;
		    $this->db->where('ContractID',$ID);
			 $this->db->where('Payment_Status',0);
        $this->db->delete('tbl_payment');
		 $this->db->insert('tbl_payment',$payment);
		  
		 }
		  $data['billing']=$this->adminmod->getbilling($ID);
			 
		 $this->load->view('admin/paymentprocess',$data);
		
	          }
  
}
