<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('adminmod');
		$this->load->model('reportmod');
		$this->load->library('session');
		$this->session->userdata("logged_admin");
		$this->session->userdata("agent_city");
		//$this->session->userdata("set_user");
		$this->load->library('user_agent');
		$this->load->library('cart');	
	}
	public function onlinedatalist(){
		$data['onlinelist'] = $this->reportmod->onlinedatalist();
		$this->load->view('admin/onlinedatalist',$data);

	}
	public function onlinedata(){
		$data['categorylist'] = $this->adminmod->cat_mod();
		$this->load->view('admin/onlinedata',$data);
	}
	public function updateonlinedata($id){
		$data['categorylist'] = $this->adminmod->cat_mod();
		$data['onlinedata'] = $this->reportmod->singleonlinedata($id);
		//echo "<pre>";print_r($data);exit;
		$this->load->view('admin/onlinedata',$data);
	}
	
	
	
	public function saveonlinedata(){
		
		 $data = array(
     	'ContractID' => "Con".date('dmY').rand(0,100000),
     	'CompanyName' => $this->input->post('companyname'),
     	'ContactPerson' => $this->input->post('contactperson'),
     	'PhoneNumber' => $this->input->post('phonenumber'),
     	'Email' => $this->input->post('email'),
     	'Address' => $this->input->post('address'),
     	'City' => $this->input->post('city'),
     	'PinCode' => $this->input->post('pincode'),
     	'Area' => $this->input->post('area'),
     	'CategoryId' => $this->input->post('category')
     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_online', $data); 
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Online Data added successfuly');
	                      window.location='".base_url()."report/onlinedatalist/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/onlinedatalist/';
	                      </script>";
					}			  
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_online', $data);    
 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Online Data Updated successfuly');
	                      window.location='".base_url()."report/onlinedatalist/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('No change done');
	                      window.location='".base_url()."report/onlinedatalist/';
	                      </script>";
					}	
	}
	}
	public function dataonlinedata(){
		//$data=$this->reportmod->dataonline();
		if($data==true)
		{
    echo "<script language='javascript'>window.alert('Onlinedata has been sent successfully! ');
	                      window.location='".base_url()."report/onlinedatalist';
	                      </script>";
		}
		
	}
	public function contractlist($id = ''){
		
		  if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
						if($id==3)
						 {
							$data['mess'] ="TME"; 
						 }
						else if ($id==4){
							 $data['mess'] ="ME"; 
						 }
						else if ($id==5){
							 $data['mess'] ="Quality"; 
						 }
						else if ($id==6){
							 $data['mess'] ="Database"; 
						 }
						else if ($id==7){
							 $data['mess'] ="IRO"; 
						 }
						  else{
							 $data['mess'] ="All";
						 }
			if($this->input->post('contract')== ""){
				 $flagid = 1;
			}else{
				 $flagid = $this->input->post('contract');
				 $data['flagid'] = $flagid;
		    }
	    $data['id'] = $id;
		$data['contractlist'] = $this->reportmod->contractlist($flagid,$id);
		$this->load->view('admin/contractlist',$data);
	}
	public function newcontractlist(){
		
		  if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
			if($this->input->post('contract')== ""){
				 $flagid = 1;
			}else{
				 $flagid = $this->input->post('contract');
				 $data['flagid'] = $flagid;
		    }
	   		$data['contractlist'] = $this->reportmod->newcontractlist($flagid,$id);
		$this->load->view('admin/newbusiness',$data);
	}
	public function addeventtype($id=''){
		$data['eventlists']= $this->reportmod->eventtypelist();
		if($id == ''){
			$this->load->view('admin/addeventtype',$data);
		}else{
		$data['eventtype']= $this->reportmod->getContractById($id);
		$this->load->view('admin/addeventtype',$data);
		}
	}
	public function saveeventtype(){
		if($this->input->post('id') ==""){
			$data = $this->reportmod->saveeventtype();
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Event Type has been saved successfully! ');
							  window.location='".base_url()."report/addeventtype/';
							  </script>";
			}
		}elseif($this->input->post('id') !=""){
			$id=$this->input->post('id');
		$data= $this->reportmod->saveeventtype($id); 
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Event Type has been Updated successfully! ');
							  window.location='".base_url()."report/addeventtype/';
							  </script>";
			}	
		}
		
	}
	public function addsourcetype($id=''){
		$data['sourcelists']= $this->reportmod->sourcetypelist();
		if($id == ''){
			$this->load->view('admin/addsourcetype',$data);
		}else{
		$data['sourcetype']= $this->reportmod->getContractById($id);
		$this->load->view('admin/addsourcetype',$data);
		}
	}
	public function savesourcetype(){
		if($this->input->post('id') ==""){
			$data = $this->reportmod->savesourcetype();
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Source Type has been saved successfully! ');
							  window.location='".base_url()."report/addsourcetype/';
							  </script>";
			}
		}elseif($this->input->post('id') !=""){
			$id=$this->input->post('id');
		$data= $this->reportmod->savesourcetype($id); 
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Source Type has been Updated successfully! ');
							  window.location='".base_url()."report/addsourcetype/';
							  </script>";
			}	
		}
		
	}
	public function events($contractid=''){
		
		if($contractid != ''){
			$data['contractinfo']= $this->reportmod->getContractById($contractid);
			$this->load->view('admin/events',$data);
		}
		else{
		//var_dump($data['contractinfo']);
		$this->load->view('admin/events');
		}
	}
	public function editevent($eventid){
		$data['eventinfo']= $this->reportmod->getContractById($eventid);
		$this->load->view('admin/events',$data);
	}
	
	public function saveevents(){
		
		if ( !isset($_POST['id']) && $this->input->post('id') ==""){
		$contractid=$this->input->post('contractid');
		$data = $this->reportmod->events($contractid);
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Events has been added successfully! ');
							  window.location='".base_url()."report/events/$contractid';
							  </script>";
			}else
			{
			echo "<script language='javascript'>window.alert('No changes made! ');
							  window.location='".base_url()."report/eventlists/';
							  </script>";
			}	
		
		}
		else{
			
		$id=$this->input->post('id');
		$data= $this->reportmod->updateevents($id); 
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Events has been Updated successfully! ');
							  window.location='".base_url()."report/eventlists/';
							  </script>";
			}else
			{
			echo "<script language='javascript'>window.alert('No changes made! ');
							  window.location='".base_url()."report/eventlists/';
							  </script>";
			}		
		
		}
	}
	
	public function eventlists(){

		$data['eventlist']= $this->reportmod->eventlist();
		$this->load->view('admin/eventlist',$data);
	}
	
	public function offers($contractid){
		$data['contractinfo']= $this->reportmod->getContractById($contractid);
		$this->load->view('admin/offers',$data);
	}
	public function editoffers($offerid){
		$data['offerinfo']= $this->reportmod->getContractById($offerid);
		$this->load->view('admin/offers',$data);
	}
	public function saveoffers(){
		if ( !isset($_POST['id']) && $this->input->post('id') ==""){
		$contractid=$this->input->post('contractid');
		$data= $this->reportmod->offers($contractid);
			if($data==true)
			{
			echo "<script language='javascript'>window.alert('Events has been Added successfully! ');
							  window.location='".base_url()."report/offers/$contractid';
							  </script>";
			}else{
				echo "<script language='javascript'>window.alert('No Changes Made');
							  window.location='".base_url()."report/offerlists/';
							  </script>";
			}	
		
		}
		else{
		$id=$this->input->post('id');
		$data= $this->reportmod->updateoffer($id);  
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Events has been Updated successfully! ');
							  window.location='".base_url()."report/offerlists/';
							  </script>";
			}else{
				echo "<script language='javascript'>window.alert('No Changes Made');
							  window.location='".base_url()."report/offerlists/';
							  </script>";
			}				
		
		}
		
	}
	public function addfacilities(){
		$data['categorylist'] = $this->adminmod->cat_mod();
		$this->load->view('admin/addfacilities',$data);
	}
	public function facilitiesinfo(){
		$data['facilities'] = $this->reportmod->facilitieslist();
		$this->load->view('admin/facilitiesinfo',$data);
	}
	public function savefacilities(){
		
		$data=$this->reportmod->facilitiesdata();
		
		if($data==1)
		{
    echo "<script language='javascript'>window.alert('Facilities Saved successfully! ');
	                      window.location='".base_url()."report/facilitiesinfo';
	                      </script>";
		}elseif($data==2){ echo "<script language='javascript'>window.alert('Facilities Updated successfully! ');
	                      window.location='".base_url()."report/facilitiesinfo';
	                      </script>";
		}else{
			echo "<script language='javascript'>window.alert('Enter Something ');
	                      window.location='".base_url()."report/facilitiesinfo';
	                      </script>";
			
		}
	}
	public function editfacilities($id=""){
		$data['categorylist'] = $this->adminmod->cat_mod();
		$data['facilities'] = $this->reportmod->getContractById($id);
		//var_dump($data['facilities']);
		$this->load->view('admin/addfacilities',$data);
	
		
	}
	public function offerlists(){
		$data['offerlist']= $this->reportmod->offerlist();
		$this->load->view('admin/offerlist',$data);
	}
	public function deletecontract($contractid){
		$data= $this->reportmod->deletelist($contractid);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Contract has been Deleted successfully! ');
							  window.location='".base_url()."report/contractlist/';
							  </script>";
			}
		
	}
	public function deleteoffer($offerid){
		$data= $this->reportmod->deletelist($offerid);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Offers has been Deleted successfully! ');
							  window.location='".base_url()."report/offerlists/';
							  </script>";
			}
		
	}
	public function deleteevent($eventid){
		$data= $this->reportmod->deletelist($eventid);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Events has been Deleted successfully! ');
							  window.location='".base_url()."report/eventlists/';
							  </script>";
			}
		
	}
	public function deletefacilities($id){
		$data= $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Facilities has been Deleted successfully! ');
							  window.location='".base_url()."report/facilitiesinfo/';
							  </script>";
			}
		
	}
	public function deleteonlinedata($id){
		$data= $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Online Data has been Deleted successfully! ');
							  window.location='".base_url()."report/onlinedatalist/';
							  </script>";
			}
	
	}public function deleteeventtype($id){
		$data= $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Event Type has been Deleted successfully! ');
							  window.location='".base_url()."report/addeventtype/';
							  </script>";
			}
	
	}public function deletesourcetype($id){
		$data= $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Source Type has been Deleted successfully! ');
							  window.location='".base_url()."report/addsourcetype/';
							  </script>";
			}
	
	}
	public function deletemovies($contid,$id){
		$data= $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('Movie has been Deleted successfully! ');
							  window.location='".base_url()."report/movieslists/$contid';
							  </script>";
			}
	
	}
	//////////////////////////////////////////////Start of Famous Personalities///////////////////////////////////////
	public function famouspersons(){
		$data['famouspersons'] = $this->reportmod->famouspersons();
		$this->load->view('admin/famouspersonality',$data);

	}
	public function addfamouspersons(){
		$this->load->view('admin/addfamouspersons');

	}
	public function updatepersondata($id){
		$data['famouspersons'] = $this->reportmod->getContractById($id);
		$this->load->view('admin/addfamouspersons',$data);

	}
	public function savefamouspersons(){
				if ($_FILES['image']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['image']['name'];
	                  		$file_size =$_FILES['image']['size'];
	                  		$file_tmp =$_FILES['image']['tmp_name'];
	                  		$file_type=$_FILES['image']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/famouspersons/".uniqid();

	                  	 $image =$path.".".end($file_name);
						 
						 if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }
	 
     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	              		
	   $image=$this->input->post('imageupdate');               		
	}

     $data = array(
     	'PersonName' => $this->input->post('name'),
     	'Specialization' => $this->input->post('specialization'),
		'Image' => $image,
		'Dateofbirth' => $this->input->post('dob'),
		'Dateofdeath' => $this->input->post('dod'),
		'Contribution' => $this->input->post('contribution'),
		'Place' => $this->input->post('place')
		

     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_famouspersonality', $data);  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('This Personality has been saved successfully! ');
							   window.location='".base_url()."report/famouspersons/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/addfamouspersons/';
	                      </script>";
					}
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_famouspersonality', $data);    
}
					  
					 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Updated successfully! ');
							   window.location='".base_url()."report/famouspersons/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('No Changes made');
	                      window.location='".base_url()."report/famouspersons/';
	                      </script>";
					}						 
	}
	
	public function deletefamouspersons($id){
		$data  = $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('This Personality has been Deleted successfully! ');
							  window.location='".base_url()."report/famouspersons/';
							  </script>";
			}

	}
	//////////////////////////////////////////////End of Famous Personalities///////////////////////////////////////
	//////////////////////////////////////////////Starting of Packages///////////////////////////////////////
	public function test(){
		$this->load->view('test');
	}
	public function packagelist(){
		$data['packages'] = $this->reportmod->packagelist();
		//echo "<pre>";print_r($data);exit;
		$this->load->view('admin/packagelist',$data);
	}
	
	public function addpackages(){
		$this->load->view('admin/addpackages');

	}
	public function updatepackages($id){
		$data['packages'] = $this->reportmod->getContractById($id);
		$this->load->view('admin/addpackages',$data);

	}
	public function savepackages(){
		
     $data = array(
     	'Name' => $this->input->post('name'),
     	'Description' => $this->input->post('description'),
		'Price1' => $this->input->post('bangaloreprice'),
		'Price2' => $this->input->post('smartcity'),
		
     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_package', $data);  
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_package', $data);    
}
					  
					 if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('This Package has been saved successfully! ');
							   window.location='".base_url()."report/packagelist/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/addpackages/';
	                      </script>";
					}						 
	}
	
	public function deletepackages($id){
		$data  = $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('This Package has been Deleted successfully! ');
							  window.location='".base_url()."report/packagelist/';
							  </script>";
			}

	}
	
	
	public function addcitywisecategory(){
		$data['categorylist'] = $this->adminmod->cat_mod();
		$this->load->view('admin/addcitywisecategory',$data);

	}
	public function savecitywisecatg(){
		$catg = implode(",",$this->input->post('category'));
		
		$data = array(
     	'Categories' => $catg,
     	'CityName' => $this->input->post('city')
		
     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_city_catg', $data);  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Added successfully! ');
							   window.location='".base_url()."report/addcitywisecategory/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/addcitywisecategory/';
	                      </script>";
					}	
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_city_catg', $data);  
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Updated successfully! ');
							   window.location='".base_url()."report/addcitywisecategory/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/addcitywisecategory/';
	                      </script>";
					}	 
}
					  
	

	}
	public function movies($contractid){
		$data['contractinfo']= $this->reportmod->getContractById($contractid);
		$this->load->view('admin/movies',$data);

	}
	public function updatemovies($contractid,$id){
		$data['contractinfo']= $this->reportmod->getContractById($contractid);
		$data['movieinfo']= $this->reportmod->moviedetails($contractid,$id);
		//print_r($data['movieinfo']);exit;
		$this->load->view('admin/movies',$data);

	}
	
	public function savemovies(){
		//echo "<pre>"; print_r($this->input->post());
		
		
		if ($_FILES['image']['size'] != 0 ){
							$errors= array();
							$file_name = $_FILES['image']['name'];
	                  		$file_size =$_FILES['image']['size'];
	                  		$file_tmp =$_FILES['image']['tmp_name'];
	                  		$file_type=$_FILES['image']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/movies/".uniqid();

	                  	 $image =$path.".".end($file_name);
						 
						 if($file_size > 2097152256425645){
         $errors[]='File size must be exactly 2 MB';
     }
	 
     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		
 	}
	else
	{
	              		
	   $image=$this->input->post('imageupdate');               		
	}

		$language = implode(",",$this->input->post('language'));
		$contractId = $this->input->post('contractId');
		
		$data = array(
     	'ContractId' => $contractId,
     	'MovieName' => $this->input->post('moviename'),
     	'Language' => $language,
     	'ShowTime' => $this->input->post('movietime'),
     	'Image' => $image
		
     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_movie', $data);  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Added successfully! ');
							   window.location='".base_url()."report/movies/$contractId';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/movies/$contractId';
	                      </script>";
					}	
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_movie', $data);  
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Updated successfully! ');
							   window.location='".base_url()."report/movies/$contractId';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/movies';
	                      </script>";
					}	 
}

	}
	
	public function movieslists($contractid){
		$data['movieslist']= $this->reportmod->movieslist($contractid);
		$data['contractid'] = $contractid;
		$this->load->view('admin/movielist',$data);

	}
	
	public function meallocationlist(){
		$data['meallocationlist'] = $this->reportmod->meallocationlist();
		$this->load->view('admin/meallocationlist',$data);

	}
	public function allocateme(){
		$data['melist'] = $this->reportmod->melist();
		//echo "<pre>"; print_r($this->session->userdata('Emp_ID'));exit;
		$this->load->view('admin/allocateme',$data);

	}
	public function editallocation($id){
		$data['melist'] = $this->reportmod->melist();
		$data['allocations'] = $this->reportmod->meallocatedlist($id)[0];
		//echo "<pre>"; print_r($data);exit;
		$this->load->view('admin/allocateme',$data);

	}
	public function saveallocatedme(){
		$data = array(
     	'Dasabase_id' => $this->session->userdata('Emp_ID'),
     	'ME_id' => $this->input->post('meid'),
		'City' => $this->input->post('city'),
		'Area' => $this->input->post('area')
		
     	);
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_meallocation', $data);  
		  if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('ME Allocated successfully! ');
							   window.location='".base_url()."report/meallocationlist/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/meallocationlist/';
	                      </script>";
					}	
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_meallocation', $data);  
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('ME Allocation Updated successfully! ');
							   window.location='".base_url()."report/meallocationlist/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/meallocationlist/';
	                      </script>";
					}	 
}
					  
					 

	}
	public function deleteallocation($id){
		$data  = $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('This List has been Deleted successfully! ');
							  window.location='".base_url()."report/meallocationlist/';
							  </script>";
			}

	}
	public function locations(){
		//$data['melist'] = $this->reportmod->melist();
		//echo "<pre>"; print_r($this->session->userdata('Emp_ID'));exit;
		$this->load->view('admin/tmeinfo');

	}
	
	public function irodatanotfound(){
		$data['datanotfound'] = $this->reportmod->irosearchdata();
		$this->load->view('admin/datanotfoundlist',$data);
	}
	public function deletenotfound($id){
		$data  = $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('This List has been Deleted successfully! ');
							  window.location='".base_url()."report/irodatanotfound/';
							  </script>";
			}

	}
	
}