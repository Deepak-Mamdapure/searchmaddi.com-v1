<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iro extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('adminmod');
		$this->load->model('iromod');
		$this->load->model('reportmod');
		$this->load->library('session');
		$this->session->userdata("logged_admin");
		$this->session->userdata("agent_city");
			
	}
	public function index(){
		$data['customerlist'] = $this->iromod->customerlist();
		$data['catg'] = $this->adminmod->cat_mod();
		$data['tmenames'] = $this->iromod->tmenames();
		$this->load->view('admin/agent',$data);
	}
	public function searchresult(){
		$data['prevdata'] = $this->input->post();
		$data['customerlist'] = $this->iromod->customerlist();
		$data['contractlist'] = $this->iromod->contractlist();
		$data['offers'] = $this->iromod->offerlist();
		//echo "<pre>";print_r($data['contractlist']);exit;
		//$this->load->view('admin/transfertotme',$data);
		//echo "<pre>";print_r($data['customerlist']);exit;
		$this->load->view('admin/searchresult',$data);
	}
	public function datanotfound(){
		$this->load->view('admin/datanotfound');
	}
	public function updatedata($id){
		$data['datalist'] = $this->iromod->searchresultdata($id);
		$this->load->view('admin/datanotfound',$data);
	}
	public function irodatanotfound(){
		$data['datanotfound'] = $this->reportmod->irosearchdata();
		$this->load->view('admin/datanotfoundlist',$data);
	}
	public function storedata(){
		$data['CompanyName']=$this->input->post('companyname');
		$data['CategoryID'] = $this->input->post('categoryname');
		$data['CategoryNotFound'] = $this->input->post('catgnotfound');
		$data['EmployeeId'] = $this->input->post('empid');
		$data['CityID'] = $this->input->post('city');
		$data['AreaID'] = $this->input->post('area');
		  
 if($this->input->post('id')==""){
		$this->db->insert('tbl_datanotfound',$data);
		
		if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Successfully Sent');
	                      window.location='".base_url()."iro/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."iro/datanotfound';
	                      </script>";
					}
  } else{
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_datanotfound', $data);  
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Updated successfully! ');
							   window.location='".base_url()."iro/';
							  </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."report/datanotfound';
	                      </script>";
					}	 
}
						
	}
	
	public function senddata(){
		$data['EmpID']=$this->session->userdata('Emp_ID');
		$data['userid'] = $this->input->post('customerdata');
		$data['contractids'] = $this->input->post('contid');
		$data['previouddata'] = $this->input->post('previouddata');
		$data['num'] = $this->input->post('Number');
		
		$this->iromod->senddatatouser($data);
		
		if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Successfully Sent');
	                      window.location='".base_url()."iro/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."iro/';
	                      </script>";
					}
		
	}
	public function deletenotfound($id){
		$data  = $this->reportmod->deletelist($id);
		if($data==true)
			{
			echo "<script language='javascript'>window.alert('This List has been Deleted successfully! ');
							  window.location='".base_url()."iro/datanotfound/';
							  </script>";
			}

	}
	public function updatebreak(){
		$timezone = new DateTimeZone("Asia/Kolkata" );
		$date = new DateTime();
		$data['LoginDate']=$this->session->userdata('logindate');
		$data['Role']=$this->session->userdata('Role');
		$data['EmpID']=$this->session->userdata('Emp_ID');
		$data['agentloginresult'] = $this->iromod->confirmlogin($data);
		$breakin = $data['agentloginresult']->BreakIn ; 
		$totbreak = $data['agentloginresult']->TotalBreak ;
		$interval = $date->format( 'H:m:s');
		
		$start_t = new DateTime($breakin);
		
		if($this->input->post('breakin')){
			$arr = array(
			'BreakIn' => $date->format( 'H:m:s'),
			
			);
			
		}elseif($this->input->post('breakout')){
			$current_t = new DateTime($interval);
			$difference = $start_t ->diff($current_t );
			$return_time = $difference ->format('%H:%I:%S');
	
				$times = array(
					$totbreak,
					$return_time,
				);

				$seconds = 0;

				foreach ( $times as $time )
				{
					list( $g, $i, $s ) = explode( ':', $time );
					$seconds += $g * 3600;
					$seconds += $i * 60;
					$seconds += $s;
				}

				$hours    = floor( $seconds / 3600 );
				$seconds -= $hours * 3600;
				$minutes  = floor( $seconds / 60 );
				$seconds -= $minutes * 60;

				$totaltime="{$hours}:{$minutes}:{$seconds}";  

			$arr = array(
			'BreakOut' => $date->format( 'H:m:s'),
			'TotalBreak' =>$totaltime
			);
			
		}
		$this->iromod->addlogintime($arr,$data);
			if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Break Approved');
	                      window.location='".base_url()."admin/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin/';
	                      </script>";
					}
		
	}
	
	
	
	public function savecustdata(){
		
		$data = array();
			/*	if($this->input->post('Number') != ''){
				$query = $this->db->query("Select `Number` from `tbl_user` where `Status`='0' And `Number`= '".$this->input->post('Number')."'");
					if($query->num_rows() > 0){
						echo "Phone Number Already Exist";
					}
					else{*/
						$data['Name'] = $this->input->post('Name');
						$data['Email'] = $this->input->post('Email');
						$data['Number'] = $this->input->post('Number');
						$data['LandlineNumber'] = $this->input->post('LandlineNumber');
						$data['BusinessProfile'] = $this->input->post('BusinessProfile');
						$this->db->insert('tbl_user',$data);
						if ($this->db->affected_rows() > 0) {
								  echo "Customer added successfuly";
								  
							  }else{
								 echo "Some error !Please try again";
							}
					//}
				//}
						
			
	}
	public function savebusinessdata(){
		
		$data = array();
			/*	if($this->input->post('Number') != ''){
				$query = $this->db->query("Select `Number` from `tbl_user` where `Status`='0' And `Number`= '".$this->input->post('Number')."'");
					if($query->num_rows() > 0){
						echo "Phone Number Already Exist";
					}
					else{*/
						$data['CustomerID'] = $this->input->post('custid');
						$data['CompanyName'] = $this->input->post('companyname');
						$data['CategoryName'] = $this->input->post('categoryname');
						$data['DecisionMaker'] = $this->input->post('decmakername');
						$data['Email'] = $this->input->post('emailaddress');
						$data['Mobile'] = $this->input->post('mobilenumber');
						$data['Landline'] = $this->input->post('landlinenumber');
						$data['Address'] = $this->input->post('address');
						$data['ClientResponse'] = $this->input->post('clientresponse');
						//$data['TMEID'] = $this->input->post('tmename');
						$data['DifferedReason'] = $this->input->post('differed');
						
						$this->db->insert('tbl_customerbusiness',$data);
						if ($this->db->affected_rows() > 0) {
							 echo "Customer's Business added successfuly";
	                      //header('Location: '.base_url()."iro/");
								 
								  
							  }else{
								  echo "Some error !Please try again";
	                      //header('Location: '.base_url()."iro/");
	                      
								
							}
				
	}
	public function catglist(){
			
			$catlist = $this->iromod->catglist();
			//echo $catlist;exit;
			echo '<ul id="country-list">';

			foreach($catlist as $catg) {

			echo '<li onClick="selectCountry('.$catg->Cat.');">'.$catg->Cat.'</li>';
			} 
			echo '</ul>';
		}
		
}