<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('adminmod');
		$this->load->model('usermod');
		$this->load->library('session');
		$this->session->userdata("logged_admin");
		$this->session->userdata("agent_city");
		//$this->session->userdata("set_user");
		$this->load->library('user_agent');
		$this->load->library('cart');	
	}
	public function adduser(){
		 if (!$this->session->userdata('Emp_ID')){ 
			redirect(base_url().'admin/login', 'refresh');
		}
		$this->load->view('admin/adduser');
	}
	public function updateuser($id){
		 if (!$this->session->userdata('Emp_ID')){ 
			redirect(base_url().'admin/login', 'refresh');
		}
		$data['cat'] = $this->usermod->singleuser($id);
		$this->load->view('admin/adduser',$data);
	}
	public function updateprofile(){
		 if (!$this->session->userdata('Emp_ID')){ 
			redirect(base_url().'admin/login', 'refresh');
		}
		$data['cat'] = $this->usermod->singleuser($this->session->userdata('Emp_ID'));
		$this->load->view('admin/adduser',$data);
	}
	public function deleteuser($id){
		$data = array('Status' => '1');
		$this->db->where('ID',$id);
		$this->db->update('tbl_admin',$data); 
		if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('User added successfuly');
	                      window.location='".base_url()."user/userdetail/';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."user/userdetail/';
	                      </script>";
					}		  
										
							
	}
	public function storeuser(){
	                  		 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
$admin = array();
	                  	
							if ($_FILES['image']['size'] != 0 ){
$errors= array();
	                $file_name = $_FILES['image']['name'];
	                  		$file_size =$_FILES['image']['size'];
	                  		$file_tmp =$_FILES['image']['tmp_name'];
	                  		$file_type=$_FILES['image']['type'];
	                  		$file_name = explode(".",$file_name);

	                  		$path="dash/img/imp/".uniqid();

	                  	 $imagelink=$path.".".end($file_name);
	  //$file_ext=strtolower(end(explode('.',$_FILES['userfile']['name'])));	
	   //$expensions= array("jpeg","jpg","png","psd");
      /*
      if(in_array($file_ext,$expensions)=== false ){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
	  */
      
     /* if($file_size > 2097152256425645){
         $errors[]='File size must be excately 2 MB';
     }*/

     if(empty($errors)==true){
     	move_uploaded_file($file_tmp,$path.".".end($file_name));



     }
     else{
     	print_r($errors);
     }
 		$admin['Image']  = $imagelink;
 	}
	else
	{
	   $imagelink=$this->input->post('imageupdate');               		
	}

		$admin['Name']               =	trim($this->input->post('name'));
        $admin['Emp_ID']               =	trim($this->input->post('empid'));
		 
		$admin['Email']               =	trim($this->input->post('email'));
        $admin['Password']               =	md5(trim($this->input->post('password')));
		$admin['Phone']               =	trim($this->input->post('phone'));
		$admin['Address']               =	trim($this->input->post('address'));
		$admin['Position']               =	trim($this->input->post('position'));
     
   
 if($this->input->post('id')=="")
	                  	{
		  $this->db->insert('tbl_admin', $admin); 
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('User added successfuly');
	                      window.location='".base_url()."admin';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin';
	                      </script>";
					}		  
						}					
							
 else
 {
 $this->db->where('ID',$this->input->post('id'));
 $this->db->update('tbl_admin', $admin);   
if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('User Updated successfuly');
	                      window.location='".base_url()."admin';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin';
	                      </script>";
					} 
}
					  
					 						 
		//$this->load->view('admin/addbrand',$data);


	                  }
					  public function userdetail()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}

						 $data['user'] = $this->usermod->userdetailmod();
		$this->load->view('admin/userdetail',$data);

	                }
					 public function assigntoql()
	                {
	                	 if (!$this->session->userdata('Emp_ID'))
    { 
redirect(base_url().'admin/login', 'refresh');
		}
$tme         =	$this->input->post('tme');
$quality  =	$this->input->post('quality');
$data1= array(
                                  'status' =>1                          
                                );
$this->db->where('Emp_ID',$quality);
 $this->db->update('tbl_assigned', $data1);  
 foreach ($tme  as $tmename)
                        { 
						$data1= array(
                                  'flag' =>2                          
                                );
$this->db->where('EmpID',$tmename);
$this->db->where('flag',0);
 $this->db->update('tbl_contract', $data1);  
						$admin=array();
						$admin['Contract_ID']  =$tmename;
        $admin['Emp_ID']               =	$quality ;
		$this->db->insert('tbl_assigned', $admin);  
						}
						  echo "<script language='javascript'>window.alert('Data assigned to Quality');
	                      window.location='".base_url()."admin/tmeinfo/3';
	                      </script>";

	                }
					
					public function changepassword(){
		 if (!$this->session->userdata('Emp_ID')){ 
			redirect(base_url().'admin/login', 'refresh');
		}
		$this->load->view('admin/changepassword');
	}
	public function savenewpassword(){
		 if (!$this->session->userdata('Emp_ID')){ 
			redirect(base_url().'admin/login', 'refresh');
		}
		$admin['Password']  =	md5(trim($this->input->post('newpassword')));
		$this->db->where('Emp_ID',$this->session->userdata('Emp_ID'));
		$query = $this->db->get('tbl_admin');
		$result = $query->result()[0];
		if($result->Password  == md5(trim($this->input->post('oldpassword')))){
			 $this->db->where('Emp_ID',$this->session->userdata('Emp_ID'));
			 $this->db->update('tbl_admin', $admin);
			
			if ($this->db->affected_rows() > 0) {
						  echo "<script language='javascript'>window.alert('Password Updated successfuly');
	                      window.location='".base_url()."admin';
	                      </script>";
						  //$data['msg'] = "success";
	                  }else{
	                  	 echo "<script language='javascript'>window.alert('Some error !Please try again');
	                      window.location='".base_url()."admin';
	                      </script>";
					}		  
						
		}
		
	}
					
					
}
